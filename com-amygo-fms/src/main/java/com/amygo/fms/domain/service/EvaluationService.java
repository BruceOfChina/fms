package com.amygo.fms.domain.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.fms.interfaces.facade.dto.EvaluationReplyDTO;
import com.amygo.persis.domain.Evaluation;
import com.amygo.persis.repository.EvaluationJpaRepository;

@Service
public class EvaluationService {
	@Autowired
	private EvaluationJpaRepository evaluationJpaRepository;
	
	public void saveEvaluation(Evaluation evaluation) {
		evaluationJpaRepository.save(evaluation);
	}
	
	public List<Evaluation> list() {
		return evaluationJpaRepository.findAll();
	}
	
	public void replyEvaluation(EvaluationReplyDTO dto) {
		Evaluation evaluation = evaluationJpaRepository.findOne(dto.getId());
		evaluation.setReplyContent(dto.getReplyContent());
		evaluation.setReplyTime(new Date());
		evaluation.setIsReplied(Evaluation.REPLIED);
		evaluationJpaRepository.save(evaluation);
	}

}
