package com.amygo.fms.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.amygo.fms.domain.modle.CarStatusRecord;
import com.amygo.fms.domain.modle.Orders;
import com.amygo.fms.domain.repository.CarStatusRecordRepository;
import com.amygo.persis.domain.Order;
import com.amygo.persis.repository.OrderJpaRepository;

@Service
@CacheConfig(cacheNames = "orders")
public class OrdersService {
	@Autowired
	private OrderJpaRepository ordersRepository;
	
	@Autowired
	protected CarStatusRecordRepository carStatusRecordRepository;
	
	
//    @Cacheable(key = "'orders'")
    public List<Order> list(){
        return ordersRepository.findAll();
    }
    
    @Cacheable(key = "#orderCode")//key是请求参数，如果查询的是同一个订单，第二次从缓存取
    public List<CarStatusRecord> track(String orderCode){
        return carStatusRecordRepository.list(orderCode);
    }

}
