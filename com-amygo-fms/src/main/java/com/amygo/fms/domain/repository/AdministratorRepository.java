package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.fms.domain.modle.Administrator;


/**
 * @author Bruce
 *
 */
public interface AdministratorRepository {

    void add(Administrator user);

    void update(Administrator user);

    void updateRoles(String uid, List<String> rids);

    Administrator get(String id);

    boolean contains(String name);

    List<Administrator> list();

    boolean hasResourcePermission(String userId,String resourceCode);

    void remove(String id);

    void switchStatus(String id,boolean disabled);

    Administrator findByUserName(String username);

    //判断用户名称是否重复
    List<Administrator> getUserByUname(String username);

    //判断email是否重复
    List<Administrator> getUserByEmail(String email);

}
