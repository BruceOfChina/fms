package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.fms.domain.vo.CarVo;
import com.amygo.persis.domain.Car;

/**
 * @author Bruce
 *
 */
public interface CarRepository {

    void add(Car car);

    void update(Car car);

    void updateMenus(String rid, List<String> mids);

    void updateResources(String rid, List<String> resources);

    boolean contains(String carName);

    Car get(String id);

    List<CarVo> list();

    void remove(String id);

    void removeCarMenuByMenuId(String menuId);

    void removeCarResourceByResourceId(String resourceId);

    void switchStatus(String id,boolean disabled);

    List<Car> getCars();


}
