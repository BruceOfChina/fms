package com.amygo.fms.domain.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.repository.CarStatusJpaRepository;


/**
 * @author Bruce
 *
 */
@Service
@CacheConfig(cacheNames = "carStatus")
public class CarStatusService {

    @Autowired
    protected CarStatusJpaRepository carStatusJpaRepository;

    @Caching(
            evict = @CacheEvict(key = "'list'"),
            put = @CachePut(key = "#carStatus.guid")
    )
    public CarStatus create(CarStatus carStatus) {
        Assert.hasText(carStatus.getVinCode(),"CarStatus vinCode is empty");
        carStatus.setGuid(UUID.randomUUID().toString());
        carStatusJpaRepository.save(carStatus);
        return carStatus;
    }

    @Caching(
            evict = @CacheEvict(key = "'list'"),
            put = @CachePut(key = "#newCarStatus.id")
    )
    public CarStatus modify(CarStatus newCarStatus) {
        Assert.hasText(newCarStatus.getVinCode(),"CarStatus name is empty");
        CarStatus carStatus = carStatusJpaRepository.findOne(newCarStatus.getId());
        carStatus.setRemainElectricity(newCarStatus.getRemainElectricity());
        carStatus.setEnduranceMileage(newCarStatus.getEnduranceMileage());
        carStatusJpaRepository.save(carStatus);
        return newCarStatus;
    }

    @Cacheable
    public CarStatus get(Long id){
        return carStatusJpaRepository.findOne(id);
    }

    @Cacheable(key = "'list'")
    public List<CarStatus> list(){
        return carStatusJpaRepository.findAll();
    }

    @Caching(
            evict = {@CacheEvict(key = "'list'"), @CacheEvict(key = "#id")}
    )
    public void delete(Long id){
        carStatusJpaRepository.delete(id);
    }

    @Caching(
            evict = {@CacheEvict(key = "'list'"), @CacheEvict(key = "#id")}
    )
    public void switchStatus(String id,boolean disable){
//        carStatusJpaRepository.switchStatus(id,disable);
    }
}
