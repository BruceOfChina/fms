package com.amygo.fms.domain.vo;

import java.util.Date;

public class CarVo {
	private Long id;
	
	private String vinCode;
	
	private String guid;
	
	private String color;
	private String brand;
	private String vehicleModel;
	private String plateNumber;
	private Integer year;
	private Integer status;
	private Integer statusDetail;
	private Date create_time;
	private String tboxId;
	private String ipcId;
	private String rootKey;
	private String initIv;
	private String orderCode;
	
	private Long carId;
	private Double lat;
	private Double lon;
	private Double remainElectricity;
	private Long enduranceMileage;
	private Integer lockStatus;
	private Integer doorStatus;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVinCode() {
		return vinCode;
	}
	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getPlateNumber() {
		return plateNumber;
	}
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatusDetail() {
		return statusDetail;
	}
	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getTboxId() {
		return tboxId;
	}
	public void setTboxId(String tboxId) {
		this.tboxId = tboxId;
	}
	public String getIpcId() {
		return ipcId;
	}
	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}
	public String getRootKey() {
		return rootKey;
	}
	public void setRootKey(String rootKey) {
		this.rootKey = rootKey;
	}
	public String getInitIv() {
		return initIv;
	}
	public void setInitIv(String initIv) {
		this.initIv = initIv;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public Double getRemainElectricity() {
		return remainElectricity;
	}
	public void setRemainElectricity(Double remainElectricity) {
		this.remainElectricity = remainElectricity;
	}
	public Long getEnduranceMileage() {
		return enduranceMileage;
	}
	public void setEnduranceMileage(Long enduranceMileage) {
		this.enduranceMileage = enduranceMileage;
	}
	public Integer getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
	}
	public Integer getDoorStatus() {
		return doorStatus;
	}
	public void setDoorStatus(Integer doorStatus) {
		this.doorStatus = doorStatus;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
}
