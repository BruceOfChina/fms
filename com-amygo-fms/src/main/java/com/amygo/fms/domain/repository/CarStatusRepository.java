package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.persis.domain.CarStatus;

/**
 * @author Bruce
 *
 */
public interface CarStatusRepository {

    void add(CarStatus carStatus);

    void update(CarStatus carStatus);

    void updateMenus(String rid, List<String> mids);

    void updateResources(String rid, List<String> resources);

    boolean contains(String carStatusName);

    CarStatus get(String id);

    List<CarStatus> list();

    void remove(String id);

    void removeCarStatusMenuByMenuId(String menuId);

    void removeCarStatusResourceByResourceId(String resourceId);

    void switchStatus(String id,boolean disabled);

    List<CarStatus> getCarStatuss(String userId);


}
