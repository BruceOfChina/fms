package com.amygo.fms.domain.modle;

public class CarStatusRecord {
	private Long id;
	
	private Long carId;
	
	private String vinCode;
	
	private String guid;
	
	private Double lat;
	/**
	 * 经度
	 */
	private Double lon;
	/**
	 * 高德纬度
	 */
	private Double gcjLat;
	/**
	 * 高德经度
	 */
	private Double gcjLon;
	
	private Long lastPowerOnTime;//上次上电时间
	
	private String powerModel;//电源模式
	
	private String gear;//档位
	
	private String faultModel;//故障模式
	
	private Double mileage;//里程
	
	private Double speed;//车辆行驶速度
	
	private boolean highVoltage;//是否高压
	
	private boolean charge;//是否充电
	
	private String direction;//朝向
	
	private Double gpsPhysicalAddr;//GPS的地址
	
	private Double remainElectricity;//剩余电量
	
	private Double onlineRate;//在线率
	
	private Double workingTimeOnlineRate;//工作时间在线率？
	
	private Double speedRate;//有车速率
	
	private Integer faultAmount;//故障数量
	
	private Integer chargingTime;//充电时间
	
	private Long travelTime;//行驶时间
	
	private Long totalOnlineTime;//总在线时间
	
	private Long totalChargingTime;//总充电时间
	
	private Long serverTime;
	
	private Long deviceTime;
	
	private String orderCode;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Long getLastPowerOnTime() {
		return lastPowerOnTime;
	}

	public void setLastPowerOnTime(Long lastPowerOnTime) {
		this.lastPowerOnTime = lastPowerOnTime;
	}

	public String getPowerModel() {
		return powerModel;
	}

	public void setPowerModel(String powerModel) {
		this.powerModel = powerModel;
	}

	public String getGear() {
		return gear;
	}

	public void setGear(String gear) {
		this.gear = gear;
	}

	public String getFaultModel() {
		return faultModel;
	}

	public void setFaultModel(String faultModel) {
		this.faultModel = faultModel;
	}

	public Double getMileage() {
		return mileage;
	}

	public void setMileage(Double mileage) {
		this.mileage = mileage;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public boolean isHighVoltage() {
		return highVoltage;
	}

	public void setHighVoltage(boolean highVoltage) {
		this.highVoltage = highVoltage;
	}

	public boolean isCharge() {
		return charge;
	}

	public void setCharge(boolean charge) {
		this.charge = charge;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Double getGpsPhysicalAddr() {
		return gpsPhysicalAddr;
	}

	public void setGpsPhysicalAddr(Double gpsPhysicalAddr) {
		this.gpsPhysicalAddr = gpsPhysicalAddr;
	}

	public Double getRemainElectricity() {
		return remainElectricity;
	}

	public void setRemainElectricity(Double remainElectricity) {
		this.remainElectricity = remainElectricity;
	}

	public Double getOnlineRate() {
		return onlineRate;
	}

	public void setOnlineRate(Double onlineRate) {
		this.onlineRate = onlineRate;
	}

	public Double getWorkingTimeOnlineRate() {
		return workingTimeOnlineRate;
	}

	public void setWorkingTimeOnlineRate(Double workingTimeOnlineRate) {
		this.workingTimeOnlineRate = workingTimeOnlineRate;
	}

	public Double getSpeedRate() {
		return speedRate;
	}

	public void setSpeedRate(Double speedRate) {
		this.speedRate = speedRate;
	}

	public Integer getFaultAmount() {
		return faultAmount;
	}

	public void setFaultAmount(Integer faultAmount) {
		this.faultAmount = faultAmount;
	}

	public Integer getChargingTime() {
		return chargingTime;
	}

	public void setChargingTime(Integer chargingTime) {
		this.chargingTime = chargingTime;
	}

	public Long getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(Long travelTime) {
		this.travelTime = travelTime;
	}

	public Long getTotalOnlineTime() {
		return totalOnlineTime;
	}

	public void setTotalOnlineTime(Long totalOnlineTime) {
		this.totalOnlineTime = totalOnlineTime;
	}

	public Long getTotalChargingTime() {
		return totalChargingTime;
	}

	public void setTotalChargingTime(Long totalChargingTime) {
		this.totalChargingTime = totalChargingTime;
	}

	public Double getGcjLat() {
		return gcjLat;
	}

	public void setGcjLat(Double gcjLat) {
		this.gcjLat = gcjLat;
	}

	public Double getGcjLon() {
		return gcjLon;
	}

	public void setGcjLon(Double gcjLon) {
		this.gcjLon = gcjLon;
	}

	public Long getServerTime() {
		return serverTime;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	public Long getDeviceTime() {
		return deviceTime;
	}

	public void setDeviceTime(Long deviceTime) {
		this.deviceTime = deviceTime;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
}
