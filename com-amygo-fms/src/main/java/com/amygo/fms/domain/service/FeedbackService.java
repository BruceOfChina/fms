package com.amygo.fms.domain.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.fms.interfaces.facade.dto.FeedbackReplyDTO;
import com.amygo.persis.domain.Feedback;
import com.amygo.persis.repository.FeedbackJpaRepository;

@Service
public class FeedbackService {
	@Autowired
	private FeedbackJpaRepository feedbackJpaRepository;
	
	public void saveFeedback(Feedback feedback) {
		feedbackJpaRepository.save(feedback);
	}
	
	public List<Feedback> list() {
		return feedbackJpaRepository.findAll();
	}
	
	public void replyFeedback(FeedbackReplyDTO dto) {
		Feedback feedback = feedbackJpaRepository.findOne(dto.getId());
		feedback.setReplyContent(dto.getReplyContent());
		feedback.setReplyTime(new Date());
		feedback.setIsReplied(Feedback.REPLIED);
		feedbackJpaRepository.save(feedback);
	}

}
