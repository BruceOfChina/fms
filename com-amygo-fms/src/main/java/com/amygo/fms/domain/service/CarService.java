package com.amygo.fms.domain.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.amygo.common.enums.CarStatusEnum;
import com.amygo.common.util.TextUtils;
import com.amygo.fms.domain.repository.ResourceRepository;
import com.amygo.fms.domain.vo.CarVo;
import com.amygo.persis.domain.Car;
import com.amygo.persis.repository.CarJpaRepository;


/**
 * @author Bruce
 *
 */
@Service
@CacheConfig(cacheNames = "car")
public class CarService {
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected CarJpaRepository carJpaRepository;

    @Autowired
    protected ResourceRepository resourceRepository;

    @Caching(
            evict = @CacheEvict(key = "'list'"),
            put = @CachePut(key = "#car.guid")
    )
    public Car create(Car car) {
        Assert.hasText(car.getVinCode(),"Car vin code is empty");
        car.setGuid(UUID.randomUUID().toString());
        car.setCreate_time(new Date());
        car.setInitIv(TextUtils.getRandomString(16));
        car.setRootKey(TextUtils.getRandomString(16));
        car.setStatus(CarStatusEnum.CAR_TO_USE.getStatus());
        carJpaRepository.save(car);
        return car;
    }

    @Caching(
            evict = @CacheEvict(key = "'list'"),
            put = @CachePut(key = "#newCar.id")
    )
    public Car modify(Car newCar) {
        Assert.hasText(newCar.getVinCode(),"Car name is empty");
        Car car = carJpaRepository.findOne(newCar.getId());
        car.setColor(newCar.getColor());
        car.setPlateNumber(newCar.getPlateNumber());
        car.setYear(newCar.getYear());
        car.setIpcId(newCar.getIpcId());
        car.setVinCode(newCar.getVinCode());
        car.setPurchaseDate(newCar.getPurchaseDate());
        car.setVehicleModel(newCar.getVehicleModel());
        car.setBrand(newCar.getBrand());
        carJpaRepository.save(car);
        return car;
    }

    @Cacheable
    public Car get(Long id){
        return carJpaRepository.findOne(id);
    }

//    @Cacheable(key = "'list'")
    public List<CarVo> list(){
    	String sql = "select c.id,c.guid,c.vin_code,c.ipc_id,c.brand,c.vehicle_model,c.color,c.create_time,c.status,cs.remain_electricity,cs.endurance_mileage from car c join car_status cs on c.id=cs.id";
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper<CarVo>(CarVo.class));
    }
    
    @Cacheable(key = "'cars'")
    public List<Car> getCars(){
        return carJpaRepository.findAll();
    }

    @Caching(
            evict = {@CacheEvict(key = "'list'"), @CacheEvict(key = "#id")}
    )
    public void delete(Long id){
        carJpaRepository.delete(id);
    }

    @Caching(
            evict = {@CacheEvict(key = "'list'"), @CacheEvict(key = "#id")}
    )
    public void switchStatus(String id,boolean disable){
//        carJpaRepository.switchStatus(id,disable);
    }

}
