package com.amygo.fms.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.amygo.fms.domain.vo.CarVo;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.SystemVariable;
import com.amygo.persis.repository.SystemVariableCategoryJpaRepository;
import com.amygo.persis.repository.SystemVariableJpaRepository;


/**
 * @author Bruce
 *
 */
@Service
@CacheConfig(cacheNames = "systemVariable")
public class SystemVariableService {
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected SystemVariableJpaRepository systemVariableJpaRepository;

    @Autowired
    protected SystemVariableCategoryJpaRepository systemVariableCategoryJpaRepository;
    
    public SystemVariable modify(SystemVariable newsystemVariable) {
        Assert.hasText(newsystemVariable.getVariableValue(),"Car name is empty");
        SystemVariable systemVariable = systemVariableJpaRepository.findOne(newsystemVariable.getId());
        systemVariable.setVariableValue(newsystemVariable.getVariableValue());
        systemVariableJpaRepository.save(systemVariable);
        return systemVariable;
    }
    
    public SystemVariable add(SystemVariable systemVariable) {
        systemVariableJpaRepository.save(systemVariable);
        return systemVariable;
    }
    
    @Cacheable(key = "'list'")
    public List<CarVo> list(){
    	String sql = "select c.id,c.guid,c.vin_code,c.color,c.create_time,c.status,cs.remain_electricity,cs.endurance_mileage from car c join car_status cs on c.id=cs.id";
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper<CarVo>(CarVo.class));
    }
    
//    @Cacheable(key = "'list'")
    public List<SystemVariable> queryOperationTime(){
        return systemVariableJpaRepository.findByVariableCategory("OPERATION_TIME");
    }
    
    public List<SystemVariable> queryOperationRegions(){
        return systemVariableJpaRepository.findByVariableCategory("OPERATION_REGION");
    }
    
    public SystemVariable getSystemVariable(String variableCategory,String variableKey){
        return systemVariableJpaRepository.findByVariableCategoryAndVariableKey(variableCategory,variableKey).orElse(null);
    }
    
//    @Cacheable
    public SystemVariable get(Long id){
        return systemVariableJpaRepository.findOne(id);
    }

    @Caching(
            evict = {@CacheEvict(key = "'list'"), @CacheEvict(key = "#id")}
    )
    public void delete(Long id){
    	systemVariableJpaRepository.delete(id);
    }

}
