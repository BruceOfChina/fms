package com.amygo.fms.domain.vo;

public class OperationTimeVo {
	
	private String startTime;
	
	private String endTime;
	
	private Long id;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public OperationTimeVo(Long id,String startTime, String endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.id = id;
	}

	
}
