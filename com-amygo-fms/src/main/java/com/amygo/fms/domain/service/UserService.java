package com.amygo.fms.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.User;
import com.amygo.persis.repository.UserJpaRepository;


/**
 * @author Bruce
 *
 */
@Service
@CacheConfig(cacheNames = "car")
public class UserService {
	
    @Autowired
    private UserJpaRepository userJpaRepository;
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    

    @Cacheable(key = "'list'")
    public List<User> list(){
    	return userJpaRepository.findAll();
    }
    

}
