package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.fms.domain.modle.CarStatusRecord;

public interface CarStatusRecordRepository {
	
	public List<CarStatusRecord> list(String orderCode);
	
}
