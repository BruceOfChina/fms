package com.amygo.fms.domain.modle;


/**
 * @author Bruce
 *
 */
public class SelectResource {

    private Long rid;//resource id

    private String label;

    private boolean checked;

    public SelectResource() {
    }

    public SelectResource(Long rid, String label, boolean checked) {
        this.rid = rid;
        this.label = label;
        this.checked = checked;
    }

    public String getLabel() {
        return label;
    }

    public Long getRid() {
        return rid;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
