package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.fms.domain.modle.Resource;


/**
 * @author Bruce
 *
 */
public interface ResourceRepository {

    void add(Resource resource);

    void update(Resource resource);

    Resource get(String code);

    List<Resource> list();

    void remove(String code);

    void switchStatus(String code,boolean disabled);

    List<Resource> listByRole(String roleId);

    List<Resource> getEnableResources();



}
