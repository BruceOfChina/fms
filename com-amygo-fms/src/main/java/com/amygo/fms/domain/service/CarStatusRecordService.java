package com.amygo.fms.domain.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.amygo.common.util.DateUtils;
import com.amygo.fms.interfaces.facade.dto.CarStatusRecordRequestDTO;
import com.amygo.fms.domain.modle.CarStatusRecord;
@Service
public class CarStatusRecordService {
	 @Autowired
	 protected JdbcTemplate jdbcTemplate;
	 
    public List<CarStatusRecord> track(CarStatusRecordRequestDTO dto) {
    	StringBuilder sb = new StringBuilder("select lat,lon from car_status_record where 1=1 ");
    	if(StringUtils.isNotBlank(dto.getVinCode())) {
    		sb.append(" and vin_code ='"+ dto.getVinCode()+"' ");
    	}
    	if(StringUtils.isNotBlank(dto.getStartTime())) {
    		sb.append(" and server_time >= "+ DateUtils.getTimeByStr(dto.getStartTime(), DateUtils.commonDateFormat));
    	}
    	if(StringUtils.isNotBlank(dto.getEndTime())) {
    		sb.append(" and server_time <= "+ DateUtils.getTimeByStr(dto.getEndTime(), DateUtils.commonDateFormat));
    	}
    	return jdbcTemplate.query(sb.toString(),new BeanPropertyRowMapper<CarStatusRecord>(CarStatusRecord.class));
    }

}
