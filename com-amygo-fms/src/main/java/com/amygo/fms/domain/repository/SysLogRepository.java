package com.amygo.fms.domain.repository;

import java.util.List;

import com.amygo.fms.domain.modle.SysLog;


/**
 * @author Bruce
 *
 */
public interface SysLogRepository {

    void add(SysLog sysLog);

    List<SysLog> list();

    void clear();
}
