package com.amygo.fms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.amygo.common.UtilConfig;

import de.codecentric.boot.admin.config.EnableAdminServer;

/**
 * @author Bruce
 *
 */
@EnableDiscoveryClient
@Import({ UtilConfig.class})
@ComponentScan("com.amygo")
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
//@EnableAdminServer
@EnableFeignClients
public class FmsApplication {

    public static void main(String[] arg){
        SpringApplication.run(FmsApplication.class);
    }
}

