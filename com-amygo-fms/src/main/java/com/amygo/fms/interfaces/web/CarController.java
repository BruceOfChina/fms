package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.fms.domain.service.CarService;
import com.amygo.fms.domain.service.CarStatusService;
import com.amygo.fms.interfaces.facade.commondobject.CarUpdateCommond;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;



@Controller
@RequestMapping("/car")
public class CarController {

    @Autowired
    protected CarService carService;
    @Autowired
    protected CarStatusService carStatusService;
    @Value("${vcm.service.url:}")
    private String vcmServiceUrl;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String create(Car car,CarStatus carStatus) {
        carService.create(car);
        carStatus.setId(car.getId());
        carStatusService.create(carStatus);
        return "redirect:/car";
    }


    @RequestMapping(value = "/{id}/modify", method = RequestMethod.POST)
    public String modify(@PathVariable("id") Long id, Car car,CarStatus carStatus) {
    	carService.modify(car);
    	carStatusService.modify(carStatus);
        return "redirect:/car";
    }


    @RequestMapping(value = "/{id}/status", method = RequestMethod.PUT)
    @ResponseBody
    public void switchStatus(@PathVariable("id") String id, @RequestParam("disable") boolean disable) {
        carService.switchStatus(id,disable);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Long id) {
         carService.delete(id);
         carStatusService.delete(id);
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String toform(@RequestParam(value = "id", required = false) Long id, @RequestParam(value = "parent", required = false) boolean parent, Model model) {
        String url = null;
        if (id!=null && !parent) {
            model.addAttribute("car", carService.get(id));
            model.addAttribute("carStatus", carStatusService.get(id));
            url = "/car/" + id + "/modify";
        } else {
            url = "/car/add";
            if (id!=null) {
                model.addAttribute("parentPath", id);
            }
        }
        model.addAttribute("api", url);
        return "car/form";
    }
    @RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
    public String view(@PathVariable(value = "id") Long id, Model model) {
    	Car car = carService.get(id);
    	CarStatus carStatus = carStatusService.get(id);
    	model.addAttribute("car", car);
    	model.addAttribute("carStatus", carStatus);
        return "car/view";
    }
    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", carService.list());
        model.addAttribute("vcmServiceUrl",vcmServiceUrl+"/realtimeMonitor.html");
        return "car/list";
    }
    
    @RequestMapping(value = "/carMonitor",method = RequestMethod.GET)
    public String getCars(Model model) {
        model.addAttribute("list", carService.getCars());
        return "car/monitor";
    }


}
