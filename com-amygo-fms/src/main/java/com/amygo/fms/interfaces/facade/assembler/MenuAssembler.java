package com.amygo.fms.interfaces.facade.assembler;

import com.amygo.fms.domain.modle.Menu;
import com.amygo.fms.infrastructure.BeanUtil;
import com.amygo.fms.interfaces.facade.commondobject.MenuCreateCommand;
import com.amygo.fms.interfaces.facade.commondobject.MenuUpdateCommond;


/**
 * @author Bruce
 *
 */
public class MenuAssembler {

    public static Menu updateCommendToDomain(String id, MenuUpdateCommond updateCommond) {
        Menu menu=new Menu();
      BeanUtil.copeProperties(updateCommond,menu);
        menu.setId(id);
        return menu;
    }

    public static Menu createCommendToDomain(MenuCreateCommand creteCommand){
        Menu menu=new Menu();
        BeanUtil.copeProperties(creteCommand,menu);
        return menu;
    }
}
