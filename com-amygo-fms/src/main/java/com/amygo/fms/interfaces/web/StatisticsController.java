package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.fms.domain.service.CarService;
import com.amygo.fms.domain.service.CarStatusService;



@Controller
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    protected CarService carService;
    @Autowired
    protected CarStatusService carStatusService;


    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", carService.list());
        return "car/list";
    }
    



}
