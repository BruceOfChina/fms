package com.amygo.fms.interfaces.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.common.BaseResult;
import com.amygo.common.util.PathPlanCommandDTO;

@FeignClient(name="com-amygo-vehicle")
public interface VehicleFeignClient {
	
	@RequestMapping(value="/api/sendPathPlanCommand",method = RequestMethod.POST)
	public BaseResult sendPathPlan(@RequestBody PathPlanCommandDTO dto);
	
	@RequestMapping(value="/api/sendDestinationPlanCommand",method = RequestMethod.POST)
	public BaseResult sendDestinationPlanCommand(@RequestBody DestinationPlanCommandDTO dto);
	
	@RequestMapping(value="/api/sendVehicleStartStopCommand",method = RequestMethod.POST)
	public BaseResult sendVehicleStartStopCommand(@RequestBody VehicleStartStopCommandDTO dto);
	
	@RequestMapping(value="/api/sendVehicleSteeringWheelCommand",method = RequestMethod.POST)
	public BaseResult sendVehicleSteeringWheelCommand(@RequestBody VehicleSteeringWheelCommandDTO dto);
	
	@RequestMapping(value="/api/sendVehicleAccelerationDecelerationCommand",method = RequestMethod.POST)
	public BaseResult sendVehicleAccelerationDecelerationCommand(@RequestBody VehicleAccelerationDecelerationCommandDTO dto);
	
	@RequestMapping(value="/api/sendRemoteSetCommand",method = RequestMethod.POST)
	public BaseResult sendRemoteSetCommand(@RequestBody VehicleRemoteSetCommandDTO dto);
	
}
