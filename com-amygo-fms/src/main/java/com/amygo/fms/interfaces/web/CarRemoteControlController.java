package com.amygo.fms.interfaces.web;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.common.BaseResult;
import com.amygo.common.util.CommonUtils;
import com.amygo.common.util.HttpUtil;
import com.amygo.fms.domain.service.CarService;
import com.amygo.fms.domain.service.CarStatusService;
import com.amygo.fms.interfaces.feign.VehicleAccelerationDecelerationCommandDTO;
import com.amygo.fms.interfaces.feign.VehicleFeignClient;
import com.amygo.fms.interfaces.feign.VehicleRemoteSetCommandDTO;
import com.amygo.fms.interfaces.feign.VehicleStartStopCommandDTO;
import com.amygo.fms.interfaces.feign.VehicleSteeringWheelCommandDTO;
import com.amygo.persis.domain.Car;



@Controller
@RequestMapping("/carRemoteControl")
public class CarRemoteControlController {

    @Autowired
    protected CarService carService;
    @Autowired
    protected CarStatusService carStatusService;
    
    @Autowired
	private VehicleFeignClient vehicleFeignClient;
    
    @Autowired
    private CommonUtils commonUtils;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    @RequestMapping(value = "/{id}/init",method = RequestMethod.GET)
    public String list(@PathVariable(value = "id") Long id, Model model) {
    	Car car = carService.get(id);
    	model.addAttribute("ipcId", car.getIpcId());
        return "carRemoteControl/remoteControl";
    }
    
	@RequestMapping(value = "/sendStartStopCommand", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult sendStartStopCommand(Model model,
			@RequestParam(value = "ipcId", required = true) String ipcId,
			@RequestParam(value = "commandType", required = true) Short commandType) {
		VehicleStartStopCommandDTO dto = new VehicleStartStopCommandDTO(ipcId,commandType);
		BaseResult br = vehicleFeignClient.sendVehicleStartStopCommand(dto);
		System.out.println("call vcm result"+commonUtils.toJson(br));
		return br;
	}
	
	@RequestMapping(value = "/sendRemoteSetCommand", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult sendRemoteSetCommand(Model model,
			@RequestParam(value = "ipcId", required = true) String ipcId,
			@RequestParam(value = "paramType", required = true) Short paramType,
			@RequestParam(value = "paramValue", required = true) Short paramValue) {
		VehicleRemoteSetCommandDTO dto = new VehicleRemoteSetCommandDTO(ipcId,paramType,paramValue);
		BaseResult br = vehicleFeignClient.sendRemoteSetCommand(dto);
		System.out.println("call vcm result"+commonUtils.toJson(br));
		return br;
	}
	
	@RequestMapping(value = "/sendVehicleAccelerationDecelerationCommand", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult sendVehicleAccelerationDecelerationCommand(Model model,
			@RequestParam(value = "ipcId", required = true) String ipcId,
			@RequestParam(value = "commandValue", required = true) Integer commandValue) {
		short accelerationOrDeceleration = commandValue>=0? (short)1:(short)0;
		VehicleAccelerationDecelerationCommandDTO dto = new VehicleAccelerationDecelerationCommandDTO(ipcId,accelerationOrDeceleration,new Integer(Math.abs(commandValue)).shortValue());
		BaseResult br = vehicleFeignClient.sendVehicleAccelerationDecelerationCommand(dto);
		System.out.println("call vcm result"+commonUtils.toJson(br));
		return br;
	}
	
	@RequestMapping(value = "/sendVehicleSteeringWheelCommand", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult sendVehicleSteeringWheelCommand(Model model,
			@RequestParam(value = "ipcId", required = true) String ipcId,
			@RequestParam(value = "angelValue", required = true) Integer angelValue) {
		short controlDirection = angelValue>=0?(short)1:(short)0;
		VehicleSteeringWheelCommandDTO dto = new VehicleSteeringWheelCommandDTO(ipcId,controlDirection,Math.abs(angelValue));
		BaseResult br = vehicleFeignClient.sendVehicleSteeringWheelCommand(dto);
		System.out.println("call vcm result"+commonUtils.toJson(br));
		return br;
	}
    

}
