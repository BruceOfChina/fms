package com.amygo.fms.interfaces.feign;

public class VehicleStartStopCommandDTO {
	private String ipcId;
	
	private Short commandType;

	public String getIpcId() {
		return ipcId;
	}

	public Short getCommandType() {
		return commandType;
	}

	public void setCommandType(Short commandType) {
		this.commandType = commandType;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public VehicleStartStopCommandDTO(String ipcId, Short commandType) {
		super();
		this.ipcId = ipcId;
		this.commandType = commandType;
	}
}
