package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.amygo.fms.domain.service.CarStatusService;

@Controller
@RequestMapping("/carStatus")
public class CarStatusController {

	@Autowired
	protected CarStatusService carStatusService;


	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String toform(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "parent", required = false) boolean parent, Model model) {
		String url = null;
		if (id!=null && !parent) {
			model.addAttribute("car", carStatusService.get(id));
			url = "/car/" + id + "/modify";
		} else {
			url = "/car/add";
			if (id!=null) {
				model.addAttribute("parentPath", id);
			}
		}
		model.addAttribute("api", url);
		return "car/form";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("list", carStatusService.list());
		return "carStatus/locaVisualization";
	}
}
