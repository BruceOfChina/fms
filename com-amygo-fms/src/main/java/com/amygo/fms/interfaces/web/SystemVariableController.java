package com.amygo.fms.interfaces.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.common.BaseResult;
import com.amygo.common.entity.Coordinate;
import com.amygo.fms.domain.service.SystemVariableService;
import com.amygo.fms.domain.vo.OperationTimeVo;
import com.amygo.persis.domain.SystemVariable;

@Controller
@RequestMapping("/systemVariable")
public class SystemVariableController {

	@Autowired
	protected SystemVariableService systemVariableService;

	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public String create(SystemVariable systemVariable, HttpServletRequest request) {
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		systemVariable.setVariableCategory("OPERATION_TIME");
		systemVariable.setVariableKey("OPERATION_TIME");
		systemVariable.setVariableName("运营时间");
		systemVariable.setVariableValue(startTime + "-" + endTime);
		systemVariable.setCreateTime(new Date());
		systemVariableService.add(systemVariable);
		return "redirect:/systemVariable/operationTimeList";
	}

	@RequestMapping(value = "/{id}/modify", method = RequestMethod.POST)
	public String modify(@PathVariable("id") Long id, SystemVariable systemVariable, HttpServletRequest request) {
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		systemVariable.setVariableValue(startTime + "-" + endTime);
		systemVariableService.modify(systemVariable);
		return "redirect:/systemVariable/operationTimeList";
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@PathVariable("id") Long id) {
		systemVariableService.delete(id);
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String toform(@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "parent", required = false) boolean parent, Model model) {
		String url = null;
		if (id != null && !parent) {
			SystemVariable systemVariable = systemVariableService.get(id);
			String[] operationTimeScope = systemVariable.getVariableValue().split("-");
			model.addAttribute("startTime", operationTimeScope[0]);
			model.addAttribute("endTime", operationTimeScope[1]);
			url = "/systemVariable/" + id + "/modify";
		} else {
			url = "/systemVariable/add";
			if (id != null) {
				model.addAttribute("parentPath", id);
			}
		}
		model.addAttribute("api", url);
		return "systemVariable/operationTime";
	}

	@RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
	public String list(@PathVariable(value = "id") Long id, Model model) {
		SystemVariable systemVariable = systemVariableService.get(id);
		if (systemVariable != null && StringUtils.isNotBlank(systemVariable.getVariableValue())) {
			String[] latAndLon = StringUtils.split(systemVariable.getVariableValue(), ",");
			List<Coordinate> list = new ArrayList<Coordinate>();
			for (int i = 0; i < latAndLon.length; i++) {
				String[] latLon = StringUtils.split(latAndLon[i], "_");
				Coordinate coordinate = new Coordinate(Double.parseDouble(latLon[0]), Double.parseDouble(latLon[1]));
				list.add(coordinate);
			}
			model.addAttribute("list", list);
		}
		return "systemVariable/viewOperationRegion";
	}

	@RequestMapping(value = "/{id}/updateOperationRegionUI", method = RequestMethod.GET)
	public String updateOperationRegionUI(@PathVariable(value = "id") Long id, Model model) {
		SystemVariable systemVariable = systemVariableService.get(id);
		if (systemVariable != null && StringUtils.isNotBlank(systemVariable.getVariableValue())) {
			String[] latAndLon = StringUtils.split(systemVariable.getVariableValue(), ",");
			List<Coordinate> list = new ArrayList<Coordinate>();
			for (int i = 0; i < latAndLon.length; i++) {
				String[] latLon = StringUtils.split(latAndLon[i], "_");
				Coordinate coordinate = new Coordinate(Double.parseDouble(latLon[0]), Double.parseDouble(latLon[1]));
				list.add(coordinate);
			}
			model.addAttribute("list", list);
		}
		model.addAttribute("id",systemVariable.getId());
		return "systemVariable/updateOperationRegion";
	}

	@RequestMapping(value = "/addOperationRegionUI", method = RequestMethod.GET)
	public String addOperationRegionUI(Model model) {
		return "systemVariable/addOperationRegion";
	}

	@RequestMapping(value = "/addOperationRegion", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult addOperationRegion(Model model,
			@RequestParam(value = "pointsLatLon", required = true) String pointsLatLon,
			@RequestParam(value = "operationRegionName", required = true) String operationRegionName) {
		System.out.println(pointsLatLon);
		String[] lonAndLats = StringUtils.split(pointsLatLon, ",");
		StringBuffer sb = new StringBuffer();
		sb.append(lonAndLats[lonAndLats.length-1]);
		for (int i = lonAndLats.length-2; i >=0; i--) {
			if (i % 2 == 1) {
				sb.append(",").append(lonAndLats[i]);
			} else if (i % 2 == 0) {
				sb.append("_").append(lonAndLats[i]);
			}
		}
		Date date = new Date();
		SystemVariable systemVariable = new SystemVariable(operationRegionName, "OPERATION_REGION", sb.toString(),
				"OPERATION_REGION", date, date);
		systemVariableService.add(systemVariable);
		return BaseResult.DEFAULT_SUCCESS;
	}
	@RequestMapping(value = "/updateOperationRegion", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult updateOperationRegion(Model model,
			@RequestParam(value = "pointsLatLon", required = true) String pointsLatLon,
			@RequestParam(value = "id", required = true) Long id) {
		System.out.println(pointsLatLon);
		String[] lonAndLats = StringUtils.split(pointsLatLon, ",");
		StringBuffer sb = new StringBuffer();
		sb.append(lonAndLats[lonAndLats.length-1]);
		for (int i = lonAndLats.length-2; i >=0; i--) {
			if (i % 2 == 1) {
				sb.append(",").append(lonAndLats[i]);
			} else if (i % 2 == 0) {
				sb.append("_").append(lonAndLats[i]);
			}
		}
		SystemVariable systemVariable = systemVariableService.get(id);
		systemVariable.setVariableValue(sb.toString());
		systemVariableService.add(systemVariable);
		return BaseResult.DEFAULT_SUCCESS;
	}

	@RequestMapping(value = "/operationTimeList", method = RequestMethod.GET)
	public String operationTimeList(Model model) {
		List<SystemVariable> sysVarlist = systemVariableService.queryOperationTime();
		List<OperationTimeVo> list = new ArrayList<OperationTimeVo>();
		for(SystemVariable systemVariable : sysVarlist) {
			String[] operationTimeScope = systemVariable.getVariableValue().split("-");
			list.add(new OperationTimeVo(systemVariable.getId(), operationTimeScope[0], operationTimeScope[1]));
		}
		model.addAttribute("list", list);
		return "systemVariable/operationTimeList";
	}

	@RequestMapping(value = "/operationRegionList", method = RequestMethod.GET)
	public String operationRegionList(Model model) {
		List<SystemVariable> list = systemVariableService.queryOperationRegions();
		model.addAttribute("list", list);
		return "systemVariable/operationRegionList";
	}
	
	@RequestMapping(value = "/electronicFence", method = RequestMethod.GET)
	public String electronicFence(Model model) {
		SystemVariable systemVariable = systemVariableService.getSystemVariable("ELECTRONIC_FENCE","ELECTRONIC_FENCE");
		if (systemVariable != null && StringUtils.isNotBlank(systemVariable.getVariableValue())) {
			String[] latAndLon = StringUtils.split(systemVariable.getVariableValue(), ",");
			List<Coordinate> list = new ArrayList<Coordinate>();
			for (int i = 0; i < latAndLon.length; i++) {
				String[] latLon = StringUtils.split(latAndLon[i], "_");
				Coordinate coordinate = new Coordinate(Double.parseDouble(latLon[0]), Double.parseDouble(latLon[1]));
				list.add(coordinate);
			}
			model.addAttribute("list", list);
			model.addAttribute("id", systemVariable.getId());
		}
		return "systemVariable/electronicFence";
	}
	
	@RequestMapping(value = "/updateElectronicFence", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BaseResult updateElectronicFence(Model model,
			@RequestParam(value = "pointsLatLon", required = true) String pointsLatLon,
			@RequestParam(value = "id", required = true) Long id) {
		System.out.println(pointsLatLon);
		String[] lonAndLats = StringUtils.split(pointsLatLon, ",");
		StringBuffer sb = new StringBuffer();
		sb.append(lonAndLats[lonAndLats.length-1]);
		for (int i = lonAndLats.length-2; i >=0; i--) {
			if (i % 2 == 1) {
				sb.append(",").append(lonAndLats[i]);
			} else if (i % 2 == 0) {
				sb.append("_").append(lonAndLats[i]);
			}
		}
		SystemVariable systemVariable = systemVariableService.get(id);
		systemVariable.setVariableValue(sb.toString());
		systemVariableService.add(systemVariable);
		return BaseResult.DEFAULT_SUCCESS;
	}
}
