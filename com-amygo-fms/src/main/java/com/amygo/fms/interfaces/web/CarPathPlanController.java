package com.amygo.fms.interfaces.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.common.util.CommonUtils;
import com.amygo.fms.domain.service.CarService;
import com.amygo.fms.domain.service.CarStatusService;
import com.amygo.persis.domain.Car;



@Controller
@RequestMapping("/carPathPlan")
public class CarPathPlanController {

    @Autowired
    protected CarService carService;
    @Autowired
    protected CarStatusService carStatusService;
    @Autowired
    private CommonUtils commonUtils;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    @RequestMapping(value = "/{id}/init",method = RequestMethod.GET)
    public String list(@PathVariable(value = "id") Long id, Model model) {
    	Car car = carService.get(id);
    	model.addAttribute("ipcId", car.getIpcId());
        return "carPathPlan/pathPlan";
    }
    
	@RequestMapping(value = "/sendGlobalPathCommand", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String sendStartStopCommand(Model model,
			@RequestParam(value = "ipcId", required = true) String ipcId,
			@RequestParam(value = "pathLatLon", required = true) String pathLatLon) {
		return "0";
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("ipcId", ipcId);
//		map.put("pathLatLon", pathLatLon);
//		String result = HttpUtil.clientPostJson(vcmServiceUrl+gloabalPathPlanCommandPath, commonUtils.toJson(map), "utf-8");
//		BaseResult br = new BaseResult();
//		br.setData(result);
//		System.out.println("call vcm result"+result);
//		return result;
	}
}
