package com.amygo.fms.interfaces.facade.dto;

public class FeedbackReplyDTO {
	
	private Long id;
	
	private String replyContent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
}
