package com.amygo.fms.interfaces.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.fms.domain.modle.Administrator;
import com.amygo.fms.domain.service.AdministratorService;
import com.amygo.fms.domain.service.MenuService;
import com.amygo.fms.interfaces.facade.assembler.AdministratorAssembler;
import com.amygo.fms.interfaces.facade.commondobject.AdministratorCommond;
import com.amygo.fms.interfaces.facade.commondobject.ProfileCommand;
import com.amygo.fms.security.SecurityUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;


/**
 * @author Bruce
 *
 */
@Controller
@RequestMapping("/administrator")
public class AdministratorController {

    @Autowired
    protected AdministratorService administratorService;

    @Autowired
    protected MenuService menuService;

    public static final Logger logger = LoggerFactory.getLogger(AdministratorController.class);

    @RequestMapping(method = RequestMethod.POST,value = "/add")
    public String create( AdministratorCommond administrator){
    	administratorService.create(AdministratorAssembler.commondToDomain(administrator));
        return "redirect:/administrator";
    }

    @RequestMapping(value = "/{id}/modify", method = RequestMethod.POST)
    public String modify(@PathVariable("id") String id,  AdministratorCommond administrator) {
    	administratorService.modify(AdministratorAssembler.commondToDomain(id, administrator));
        return "redirect:/administrator";
    }

    @RequestMapping(value = "/{id}/status",method = RequestMethod.PUT)
    @ResponseBody
    public void switchStatus(@PathVariable("id") String id,@RequestParam("disable") boolean disable){
        administratorService.switchStatus(id,disable);
    }
    @RequestMapping(value = "/{id}/delete",method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id")String id){
         administratorService.delete(id);
    }

    @RequestMapping(value = "/form",method = RequestMethod.GET)
    public String form(@RequestParam(value = "id",required = false)String id, Model model){
        String api="/administrator/add";
        if(StringUtils.isNotBlank(id)){
            model.addAttribute("acount",AdministratorAssembler.domainToDto(administratorService.get(id)));
            api="/administrator/"+id+"/modify";
        }
        model.addAttribute("api",api);
        return  "administrator/form";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("list",AdministratorAssembler.domainToDto(administratorService.list()));
        return "administrator/list";
    }

    @RequestMapping(value = "/{id}/grant-role",method = RequestMethod.POST)
    public String grantRole(@PathVariable("id") String id,  String[] rid) {
        if(rid==null){
            rid=new String[0];
        }
        administratorService.grantRole(id, Lists.newArrayList(rid));
        return "redirect:/administrator";
    }


    @RequestMapping(value = "/{id}/select-role", method = RequestMethod.GET)
    public String selectRole(@PathVariable("id") String id,Model model) {
        model.addAttribute("list",administratorService.selectRoles(id));
        model.addAttribute("api","/administrator/"+id+"/grant-role");
        return "administrator/grant-role";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String myinfo() {
        return "administrator/profile";
    }

    @RequestMapping(value = "/modify-profile", method = RequestMethod.POST)
    public String modifyProfile( ProfileCommand myInfo) {
        this.administratorService.modify(AdministratorAssembler.profileToDomain(SecurityUtil.getUid(), myInfo));
        SecurityUtil.getUser().setEmail(myInfo.getEmail());
        return "administrator/profile";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request,HttpServletResponse response) {
        request.getSession().removeAttribute("administrator");
        return "login";
    }

    @RequestMapping(value = "/selectusername", method = RequestMethod.GET)
    @ResponseBody
    public String selectusername(String username) throws IOException {
        logger.debug("username:{}",username);
        boolean result = false;
        List<Administrator> listuser = administratorService.getUserByUname(username);
        if (listuser.size() < 1) {
            result = true;
        }
        Map<String, Boolean> map = new HashMap<>();
        map.put("valid", result);
        ObjectMapper mapper = new ObjectMapper();
        String resultString = "";
        try {
            resultString = mapper.writeValueAsString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    @RequestMapping(value = "/selectuseremail", method = RequestMethod.GET)
    @ResponseBody
    public String selectuseremail(String email) throws IOException {
        logger.debug("email:{}",email);
        boolean result = false;
        List<Administrator> listuser = administratorService.getUserByEmail(email);
        if (listuser.size() < 1) {
            result = true;
        }
        Map<String, Boolean> map = new HashMap<>();
        map.put("valid", result);
        ObjectMapper mapper = new ObjectMapper();
        String resultString = "";
        try {
            resultString = mapper.writeValueAsString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }



}
