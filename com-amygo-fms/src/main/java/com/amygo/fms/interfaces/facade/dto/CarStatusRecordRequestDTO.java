package com.amygo.fms.interfaces.facade.dto;

public class CarStatusRecordRequestDTO {
	
	private String startTime;
	
	private String endTime;
	
	private String vinCode;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public CarStatusRecordRequestDTO(String startTime, String endTime, String vinCode) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.vinCode = vinCode;
	}
	

}
