package com.amygo.fms.interfaces.facade.assembler;

import org.springframework.util.CollectionUtils;

import com.amygo.fms.domain.modle.Administrator;
import com.amygo.fms.infrastructure.BeanUtil;
import com.amygo.fms.interfaces.facade.commondobject.AdministratorCommond;
import com.amygo.fms.interfaces.facade.commondobject.ProfileCommand;
import com.amygo.fms.interfaces.facade.dto.AdministratorDto;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Bruce
 *
 */
public class AdministratorAssembler {

    public static Administrator commondToDomain(AdministratorCommond commond) {
        Administrator user=new Administrator();
        BeanUtil.copeProperties(commond,user);
        return user;
    }

    public static Administrator commondToDomain(String uid, AdministratorCommond commond) {
        Administrator user = new Administrator();
        BeanUtil.copeProperties(commond, user);
        user.setId(uid);
        return user;
    }

    public static Administrator profileToDomain(String uid, ProfileCommand commond) {
        Administrator user = new Administrator();
        BeanUtil.copeProperties(commond, user);
        user.setId(uid);
        return user;
    }


    public static AdministratorDto domainToDto(Administrator user){
        AdministratorDto dto=new AdministratorDto();
       BeanUtil.copeProperties(user,dto);
        return dto;
    }

    public static List<AdministratorDto> domainToDto(List<Administrator> user){
       if(CollectionUtils.isEmpty(user)){
           return null;
       }
        List<AdministratorDto> dtos=new ArrayList<>(user.size());
        user.stream().forEach(user1 -> {
            dtos.add(domainToDto(user1));
        });
        return dtos;
    }

}
