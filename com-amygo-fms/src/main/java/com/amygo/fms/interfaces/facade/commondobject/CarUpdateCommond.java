package com.amygo.fms.interfaces.facade.commondobject;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CarUpdateCommond{
    
	private Long id;
	
	private String vinCode;
	
	private String guid;
	
	private String color;
	
	private String plateNumber;
	private Integer year;
	private Integer status;
	private Integer statusDetail;
	private Date create_time;
	private String tboxId;
	private String ipcId;
	private String rootKey;
	private String initIv;
	private String orderCode;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTboxId() {
		return tboxId;
	}

	public void setTboxId(String tboxId) {
		this.tboxId = tboxId;
	}

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public String getRootKey() {
		return rootKey;
	}

	public void setRootKey(String rootKey) {
		this.rootKey = rootKey;
	}

	public String getInitIv() {
		return initIv;
	}

	public void setInitIv(String initIv) {
		this.initIv = initIv;
	}

	public Integer getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
}

