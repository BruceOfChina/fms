package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amygo.fms.domain.service.UserService;
import com.amygo.persis.domain.Car;



@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    protected UserService userService;

    @RequestMapping(value = "/{id}/status", method = RequestMethod.PUT)
    @ResponseBody
    public void switchStatus(@PathVariable("id") String id, @RequestParam("disable") boolean disable) {
//        userService.switchStatus(id,disable);
    }

    @RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
    public String view(@PathVariable(value = "id") Long id, Model model) {
//    	Car user = userService.get(id);
//    	model.addAttribute("user", user);
        return "user/view";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", userService.list());
        return "user/list";
    }
    



}
