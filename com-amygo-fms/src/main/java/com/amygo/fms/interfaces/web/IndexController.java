package com.amygo.fms.interfaces.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author Bruce
 *
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index() {
//        return "index";
    	return "redirect:/menu";
    }
}
