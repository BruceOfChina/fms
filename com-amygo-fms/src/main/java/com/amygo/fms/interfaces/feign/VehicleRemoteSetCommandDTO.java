package com.amygo.fms.interfaces.feign;

public class VehicleRemoteSetCommandDTO {
//	1：驾驶模式设置
//	2：EPB状态设置
//	3：目标档位设置
	private String ipcId;
	
	private Short paramType;
	
	private Short paramValue;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public Short getParamType() {
		return paramType;
	}

	public void setParamType(Short paramType) {
		this.paramType = paramType;
	}

	public Short getParamValue() {
		return paramValue;
	}

	public void setParamValue(Short paramValue) {
		this.paramValue = paramValue;
	}

	public VehicleRemoteSetCommandDTO(String ipcId, Short paramType, Short paramValue) {
		super();
		this.ipcId = ipcId;
		this.paramType = paramType;
		this.paramValue = paramValue;
	}
}
