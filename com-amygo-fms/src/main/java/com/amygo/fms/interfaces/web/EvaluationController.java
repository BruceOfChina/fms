package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.fms.domain.service.EvaluationService;
import com.amygo.fms.interfaces.facade.dto.EvaluationReplyDTO;
import com.amygo.persis.repository.EvaluationJpaRepository;

@Controller
@RequestMapping("/evaluation")
public class EvaluationController {
	@Autowired
	private EvaluationService evaluationService;
	@Autowired
	private EvaluationJpaRepository evaluationJpaRepository;
	
	@GetMapping
    public String list(Model model) {
        model.addAttribute("list", evaluationService.list());
        return "evaluation/list";
    }
	
	@RequestMapping(value = "/{id}/replyUI", method = RequestMethod.GET)
    public String replyUI(@PathVariable("id") Long id,Model model) {
        model.addAttribute("evaluation", evaluationJpaRepository.findOne(id));
        model.addAttribute("api", "/evaluation/reply");
        return "evaluation/replyUI";
    }
	
	@RequestMapping(value = "/reply", method = RequestMethod.POST)
    public String reply(EvaluationReplyDTO dto,Model model) {
		evaluationService.replyEvaluation(dto);
        return "redirect:/evaluation";
    }
	
	@RequestMapping(value = "/{id}/viewUI", method = RequestMethod.GET)
    public String viewUI(@PathVariable("id") Long id,Model model) {
        model.addAttribute("evaluation", evaluationJpaRepository.findOne(id));
        return "evaluation/view";
    }

}
