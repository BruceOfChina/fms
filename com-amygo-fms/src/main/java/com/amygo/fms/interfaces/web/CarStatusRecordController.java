package com.amygo.fms.interfaces.web;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.amygo.fms.domain.service.CarStatusRecordService;
import com.amygo.fms.interfaces.facade.dto.CarStatusRecordRequestDTO;
import com.amygo.fms.domain.modle.CarStatusRecord;

@Controller
@RequestMapping("/carStatusRecord")
public class CarStatusRecordController {

	@Autowired
	protected CarStatusRecordService carStatusRecordService;


	@RequestMapping(value = "/track", method = RequestMethod.GET)
    public String view(@RequestParam(name="vinCode",required=false)String vinCode,
    		@RequestParam(name="startTime",required=false)String startTime,
    		@RequestParam(name="endTime",required=false)String endTime,
    		Model model) {
		if(StringUtils.isNotBlank(vinCode)) {
			CarStatusRecordRequestDTO dto = new CarStatusRecordRequestDTO(startTime,endTime,vinCode);
			List<CarStatusRecord> list = carStatusRecordService.track(dto);
	    	model.addAttribute("list", list);
	    	model.addAttribute("dto", dto);
	    	if(!list.isEmpty()) {
	    		CarStatusRecord center = list.get(list.size()/2);
	        	model.addAttribute("centerLon",center.getLon());
	        	model.addAttribute("centerLat",center.getLat());
	    	}
		}
        return "carStatusRecord/track";
    }


}
