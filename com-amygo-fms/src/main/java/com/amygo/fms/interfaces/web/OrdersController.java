package com.amygo.fms.interfaces.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.fms.domain.modle.CarStatusRecord;
import com.amygo.fms.domain.service.OrdersService;



@Controller
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    protected OrdersService ordersService;

    @RequestMapping(value = "/{orderCode}/track", method = RequestMethod.GET)
    public String view(@PathVariable(value = "orderCode") String orderCode, Model model) {
    	List<CarStatusRecord> list = ordersService.track(orderCode);
    	model.addAttribute("list", list);
    	if(!list.isEmpty()) {
    		CarStatusRecord center = list.get(list.size()/2);
        	model.addAttribute("centerLon",center.getLon());
        	model.addAttribute("centerLat",center.getLat());
    	}
        return "carStatus/track";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", ordersService.list());
        return "orders/list";
    }
}
