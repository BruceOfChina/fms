package com.amygo.fms.interfaces.feign;

public class VehicleSteeringWheelCommandDTO {
	private String ipcId;
	
	private Short controlDirection;
	
	private Integer angelValue;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public Short getControlDirection() {
		return controlDirection;
	}

	public void setControlDirection(Short controlDirection) {
		this.controlDirection = controlDirection;
	}

	public Integer getAngelValue() {
		return angelValue;
	}

	public void setAngelValue(Integer angelValue) {
		this.angelValue = angelValue;
	}

	public VehicleSteeringWheelCommandDTO(String ipcId, Short controlDirection, Integer angelValue) {
		super();
		this.ipcId = ipcId;
		this.controlDirection = controlDirection;
		this.angelValue = angelValue;
	}
}
