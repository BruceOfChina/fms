package com.amygo.fms.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.fms.domain.service.FeedbackService;
import com.amygo.fms.interfaces.facade.dto.FeedbackReplyDTO;
import com.amygo.persis.repository.FeedbackJpaRepository;

@Controller
@RequestMapping("/feedback")
public class FeedbackController {
	@Autowired
	private FeedbackService feedbackService;
	@Autowired
	private FeedbackJpaRepository feedbackJpaRepository;
	
	@GetMapping
    public String list(Model model) {
        model.addAttribute("list", feedbackService.list());
        return "feedback/list";
    }
	
	@RequestMapping(value = "/{id}/replyUI", method = RequestMethod.GET)
    public String replyUI(@PathVariable("id") Long id,Model model) {
        model.addAttribute("feedback", feedbackJpaRepository.findOne(id));
        model.addAttribute("api", "/feedback/reply");
        return "feedback/replyUI";
    }
	
	@RequestMapping(value = "/reply", method = RequestMethod.POST)
    public String reply(FeedbackReplyDTO dto,Model model) {
		feedbackService.replyFeedback(dto);
        return "redirect:/feedback";
    }
	
	@RequestMapping(value = "/{id}/viewUI", method = RequestMethod.GET)
    public String viewUI(@PathVariable("id") Long id,Model model) {
        model.addAttribute("feedback", feedbackJpaRepository.findOne(id));
        return "feedback/view";
    }

}
