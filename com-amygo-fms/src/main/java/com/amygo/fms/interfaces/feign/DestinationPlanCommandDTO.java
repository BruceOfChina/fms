package com.amygo.fms.interfaces.feign;

public class DestinationPlanCommandDTO {
	private String ipcId;
	
	private Short destinationType;

	private Long destinationLon;

	private Long destinationLat;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public Short getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(Short destinationType) {
		this.destinationType = destinationType;
	}

	public Long getDestinationLon() {
		return destinationLon;
	}

	public void setDestinationLon(Long destinationLon) {
		this.destinationLon = destinationLon;
	}

	public Long getDestinationLat() {
		return destinationLat;
	}

	public void setDestinationLat(Long destinationLat) {
		this.destinationLat = destinationLat;
	}
}
