package com.amygo.fms.infrastructure.persistence.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.amygo.fms.domain.modle.Administrator;
import com.amygo.fms.domain.repository.AdministratorRepository;


/**
 * @author Bruce
 *
 */
@Repository
public class AdministratorRepositoryJdbc implements AdministratorRepository {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Override
    public void add(Administrator administrator) {
        jdbcTemplate.update("INSERT administrator (id,username,password,email,disabled,createTime,salt) VALUES (?,?,?,?,?,?,?)",administrator.getId(),administrator.getUsername(),administrator.getPassword(),administrator.getEmail(),administrator.isDisabled()?1:0,new Date(),administrator.getSalt());
    }

    @Override
    public void update(Administrator administrator) {
        jdbcTemplate.update("UPDATE administrator SET username=?,email=?,password=? WHERE id=?",administrator.getUsername(),administrator.getEmail(),administrator.getPassword(),administrator.getId());
    }

    @Override
    public void updateRoles(String uid, List<String> rids) {
        jdbcTemplate.update("DELETE FROM user_role WHERE uid=?", uid);
        if (!CollectionUtils.isEmpty(rids)) {
            jdbcTemplate.batchUpdate("INSERT user_role (uid,role_id) VALUES (?,?)", new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setString(1, uid);
                    ps.setString(2, rids.get(i));
                }

                @Override
                public int getBatchSize() {
                    return rids.size();
                }
            });
        }
    }

    @Override
    public Administrator get(String id) {
        return jdbcTemplate.queryForObject("select * from administrator where id=?", BeanPropertyRowMapper.newInstance(Administrator.class),id);
    }

    @Override
    public boolean contains(String name) {
        return jdbcTemplate.query("select count(username) from administrator where username=?", rs -> rs.getInt(1)>0,name);
    }

    @Override
    public List<Administrator> list() {
        return jdbcTemplate.query("select * from administrator where username <> 'root'", BeanPropertyRowMapper.newInstance(Administrator.class));
    }


    @Override
    public boolean hasResourcePermission(String uid,String resourceCode) {
        return jdbcTemplate.query("select count(*) from user_role ur join role_resource rr on ur.role_id=rr.role_id where ur.uid=? and rr.resource_id=?",rs -> rs.getInt(0)>0,uid,resourceCode);
    }



    @Override
    public void remove(String id) {
        Administrator administrator=get(id);
        if(administrator.isRoot()){
            return;
        }
        jdbcTemplate.update("DELETE FROM administrator WHERE id=?",id);
        jdbcTemplate.update("DELETE FROM user_role WHERE uid=?",id);
    }

    public void switchStatus(String id,boolean disabled){
        jdbcTemplate.update("update administrator SET disabled=? WHERE id=?",disabled?1:0,id);
    }


    @Override
    public Administrator findByUserName(String username) {
        try {
            return jdbcTemplate.queryForObject("select * from administrator where username=? ", BeanPropertyRowMapper.newInstance(Administrator.class), username);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
    }

    @Override
    public List<Administrator> getUserByUname(String username) {
        return jdbcTemplate.query("select * from administrator where username =?",BeanPropertyRowMapper.newInstance(Administrator.class),username);
    }

    @Override
    public List<Administrator> getUserByEmail(String email) {
        return jdbcTemplate.query("select * from administrator  where email =?",BeanPropertyRowMapper.newInstance(Administrator.class),email);
    }

}
