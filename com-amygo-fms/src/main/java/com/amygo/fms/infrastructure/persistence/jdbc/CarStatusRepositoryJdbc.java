package com.amygo.fms.infrastructure.persistence.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.amygo.fms.domain.repository.CarStatusRepository;
import com.amygo.persis.domain.CarStatus;
@Repository
public class CarStatusRepositoryJdbc implements CarStatusRepository{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void add(CarStatus carStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(CarStatus carStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMenus(String rid, List<String> mids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateResources(String rid, List<String> resources) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(String carStatusName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public CarStatus get(String id) {
		return jdbcTemplate.queryForObject("select * from car_status where id=?",createMapper(),id);
	}

	@Override
	public List<CarStatus> list() {
		String sql = "select * from car_status";
		return jdbcTemplate.query(sql,createMapper());
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCarStatusMenuByMenuId(String menuId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCarStatusResourceByResourceId(String resourceId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchStatus(String id, boolean disabled) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<CarStatus> getCarStatuss(String userId) {
		String sql = "select * from car_status";
		return jdbcTemplate.query(sql,createMapper());
	}
	
	 private RowMapper<CarStatus> createMapper() {
	       return BeanPropertyRowMapper.newInstance(CarStatus.class);
	   }


}
