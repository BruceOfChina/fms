package com.amygo.fms.infrastructure.persistence.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.amygo.fms.domain.repository.CarRepository;
import com.amygo.fms.domain.vo.CarVo;
import com.amygo.persis.domain.Car;
@Repository
public class CarRepositoryJdbc implements CarRepository{
	@Autowired
	protected JdbcTemplate jdbcTemplate;

	@Override
	public void add(Car car) {
	}

	@Override
	public void update(Car car) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMenus(String rid, List<String> mids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateResources(String rid, List<String> resources) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(String carName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Car get(String id) {
		return jdbcTemplate.queryForObject("select * from car where id=?",createMapper(),id);
	}

	@Override
	public List<CarVo> list() {
		String sql = "select c.id,c.guid,c.vin_code,c.color,c.create_time,c.status,cs.remain_electricity,cs.endurance_mileage from car c join car_status cs on c.id=cs.car_id";
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper<CarVo>(CarVo.class));
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCarMenuByMenuId(String menuId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCarResourceByResourceId(String resourceId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchStatus(String id, boolean disabled) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Car> getCars() {
		String sql = "select * from car";
		return jdbcTemplate.query(sql,createMapper());
	}
	
	 private RowMapper<Car> createMapper() {
       return BeanPropertyRowMapper.newInstance(Car.class);
   }

}
