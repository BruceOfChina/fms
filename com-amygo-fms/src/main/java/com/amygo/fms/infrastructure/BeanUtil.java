package com.amygo.fms.infrastructure;


import org.springframework.beans.BeanUtils;


/**
 * @author Bruce
 *
 */
public class BeanUtil {

    public static void copeProperties(Object from,Object dest){
        try {
            BeanUtils.copyProperties(from, dest);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
