package com.amygo.fms.infrastructure.persistence.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.amygo.fms.domain.modle.CarStatusRecord;
import com.amygo.fms.domain.repository.CarStatusRecordRepository;
@Repository
public class CarStatusRecordRepositoryJdbc implements CarStatusRecordRepository{
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	
	@Override
	public List<CarStatusRecord> list(String orderCode) {
		String sql = "select * from car_status_record where order_code='"+orderCode+"'";
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper<CarStatusRecord>(CarStatusRecord.class));
	}
}
