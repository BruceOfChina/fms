

// Charts.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - Designbudy.com -



 $(document).ready(function() {

	// MORRIS LINE CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	var day_data = [
      {
        year: "12-03",
        value: 2.12
      }, {
        year: "12-04",
        value: 2.09
      }, {
        year: "12-05",
        value: 1.98
      }, {
        year: "12-06",
        value: 1.93
      }, {
        year: "12-07",
        value: 2.02
      },      {
        year: "12-08",
        value: 1.87
      }, {
        year: "12-09",
        value: 1.78
      }];
	Morris.Line({
		element: 'demo-morris-line',
		data: day_data,
		xkey: 'year',
		ykeys: ['value'],
		labels: ['value'],
		gridEnabled: true,
		lineColors: ['#01c0c8'],
		lineWidth: 3,
		parseTime: false,
		resize:true,
		hideHover: 'auto'
	});
	
		var day_data_total = [
      {
        year: "12-03",
        value: 668.33
      }, {
        year: "12-04",
        value: 670.42
      }, {
        year: "12-05",
        value: 672.4
      }, {
        year: "12-06",
        value: 674.33
      }, {
        year: "12-07",
        value: 676.35
      },      {
        year: "12-08",
        value: 678.22
      }, {
        year: "12-09",
        value: 680
      }];
	Morris.Line({
		element: 'demo-morris-line-total',
		data: day_data_total,
		xkey: 'year',
		ykeys: ['value'],
		labels: ['value'],
		gridEnabled: true,
		lineColors: ['#01c0c8'],
		lineWidth: 3,
		parseTime: false,
		resize:true,
		hideHover: 'auto'
	});

	// MORRIS AREA CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	var day_data = [
      {
        year: "2018-12-03",
        value: 83
      }, {
        year: "2018-12-04",
        value: 80
      }, {
        year: "2018-12-05",
        value: 78
      }, {
        year: "2018-12-06",
        value: 79
      }, {
        year: "2018-12-07",
        value: 79
      },      {
        year: "2018-12-08",
        value: 70
      }, {
        year: "2018-12-09",
        value: 72
      }];
	Morris.Area({
		element: 'demo-morris-area',
		data: day_data,
		xkey: 'year',
		ykeys: ['value'],
		labels: ['value'],
		gridEnabled: true,
		lineColors: ['#66d9de'],
		lineWidth: 2,
		pointSize: 5,
		parseTime: true,
		resize:true,
		fillOpacity: 0.5,
		hideHover: 'auto'
	});

	var day_data = [
      {
        year: "2018-12-03",
        value: 80
      }, {
        year: "2018-12-04",
        value: 80
      }, {
        year: "2018-12-05",
        value: 80
      }, {
        year: "2018-12-06",
        value: 80
      }, {
        year: "2018-12-07",
        value: 80
      },      {
        year: "2018-12-08",
        value: 80
      }, {
        year: "2018-12-09",
        value: 80
      }];
	Morris.Area({
		element: 'demo-morris-area-total',
		data: day_data,
		xkey: 'year',
		ykeys: ['value'],
		labels: ['value'],
		gridEnabled: true,
		lineColors: ['#66d9de'],
		lineWidth: 2,
		pointSize: 5,
		parseTime: true,
		resize:true,
		fillOpacity: 0.5,
		hideHover: 'auto'
	});

	// COMBINED AREA CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================

    month_data = [
     {
        month: "12-03",
        a: 400,
        b: 39
      }, {
        month: "12-04",
        a: 350,
        b: 128
      }, {
        month: "12-05",
        a: 400,
        b: 223
      }, {
        month: "12-06",
        a: 400,
        b: 332
      }, {
        month: "12-07",
        a: 450,
        b: 335
      }, {
        month: "12-08",
        a: 550,
        b: 337
      }, {
        month: "12-09",
        a: 480,
        b: 340
      }];
	Morris.Area({
		element: 'demo-morris-combo-area',
		data: month_data,
		xkey: 'month',
		ykeys: ['b'],
		labels: ['value B'],
		gridEnabled: true,
		lineColors: ['#66d9de'],
		lineWidth: 2,
		parseTime: false,
		resize:true,
		hideHover: 'auto'
	});



	// COMBINED LINE CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================

    month_data = [
      {
        month: "12-03",
        a: 400,
        b: 80
      }, {
        month: "12-04",
        a: 350,
        b: 89
      }, {
        month: "12-05",
        a: 400,
        b: 95
      }, {
        month: "12-06",
        a: 400,
        b: 109
      }, {
        month: "12-07",
        a: 450,
        b: 3
      }, {
        month: "12-08",
        a: 550,
        b: 2
      }, {
        month: "12-09",
        a: 480,
        b: 3
      }];
	Morris.Line({
		element: 'demo-morris-combo',
		data: month_data,
		xkey: 'month',
		ykeys: ['b'],
		labels: ['value B'],
		gridEnabled: true,
		lineColors: ['#66d9de'],
		lineWidth: 3,
		parseTime: false,
		resize:true,
		hideHover: 'auto'
	});



	// MORRIS BAR CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	Morris.Bar({
		element: 'demo-morris-bar',
		data: [
			{ y: '12-03', a: 39.5,b: 0,c:0},
			{ y: '12-04', a: 39,b: 0,c:0 },
			{ y: '12-05', a: 37.8,b: 0,c:0 },
			{ y: '12-06', a: 37.5,b: 0,c:0 },
			{ y: '12-07', a: 38.9,b: 0,c:0 },
			{ y: '12-08', a: 36.5,b: 0,c:0 },
			{ y: '12-09', a: 35.8,b: 0,c:0 },
		],
		xkey: 'y',
		ykeys: ['b','a','c'],
		labels: ['Series B','Series A','Series C'],
		gridEnabled: true,
		barColors: ['#99e5e9'],
		resize:true,
		hideHover: 'auto'
	});

	Morris.Bar({
		element: 'demo-morris-bar-total',
		data: [
			{ y: '12-03', b: 12371.5,a: 0,c:0},
			{ y: '12-04', b: 12410.5,a: 0,c:0},
			{ y: '12-05', b: 12448.3,a: 0,c:0},
			{ y: '12-06', b: 12485.8,a: 0,c:0},
			{ y: '12-07', b: 12524.7,a: 0,c:0},
			{ y: '12-08', b: 12561.2,a: 0,c:0},
			{ y: '12-09', b: 12597 ,a: 0,c:0},
		],
		xkey: 'y',
		ykeys: ['a','b','c'],
		labels: ['Series A','Series B','Series C'],
		gridEnabled: true,
		barColors: ['#4dd2d8'],
		resize:true,
		hideHover: 'auto'
	});
	
		Morris.Bar({
		element: 'demo-morris-bar-total1',
		data: [
			{ y: '12-03', b: 0.47,a: 0,c:0},
			{ y: '12-04', b: 0.46,a: 0,c:0},
			{ y: '12-05', b: 0.45,a: 0,c:0},
			{ y: '12-06', b: 0.45,a: 0,c:0},
			{ y: '12-07', b: 0.45,a: 0,c:0},
			{ y: '12-08', b: 0.45,a: 0,c:0},
			{ y: '12-09', b: 0.44 ,a: 0,c:0},
		],
		xkey: 'y',
		ykeys: ['a','b','c'],
		labels: ['Series A','Series B','Series C'],
		gridEnabled: true,
		barColors: ['#4dd2d8'],
		resize:true,
		hideHover: 'auto'
	});
	
		Morris.Bar({
		element: 'demo-morris-bar-total2',
		data: [
			{ y: '12-03', b: 148.50,a: 0,c:0},
			{ y: '12-04', b: 148.96,a: 0,c:0},
			{ y: '12-05', b: 149.41,a: 0,c:0},
			{ y: '12-06', b: 149.86,a: 0,c:0},
			{ y: '12-07', b: 150.31,a: 0,c:0},
			{ y: '12-08', b: 150.76,a: 0,c:0},
			{ y: '12-09', b: 151.2 ,a: 0,c:0},
		],
		xkey: 'y',
		ykeys: ['a','b','c'],
		labels: ['Series A','Series B','Series C'],
		gridEnabled: true,
		barColors: ['#4dd2d8'],
		resize:true,
		hideHover: 'auto'
	});

	Morris.Bar({
		element: 'demo-morris-stack-bar-total',
		data: [
			{ y: '12-03', a: 148.50},
			{ y: '12-04', a: 148.96},
			{ y: '12-05', a: 149.41},
			{ y: '12-06', a: 149.86},
			{ y: '12-07', a: 150.31},
			{ y: '12-08', a: 150.76},
			{ y: '12-09', a: 151.2},
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A'],
		gridEnabled: true,
		barColors: ['#01c0c8', '#4dd2d8', '#99e5e9'],
		resize:true,
		stacked: true,
		hideHover: 'auto'
	});


	// MORRIS STACK BAR CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	Morris.Bar({
		element: 'demo-morris-stack-bar',
		data: [
			{ y: '12-03', a: 0.47},
			{ y: '12-04', a: 0.46},
			{ y: '12-05', a: 0.45},
			{ y: '12-06', a: 0.45},
			{ y: '12-07', a: 0.45},
			{ y: '12-08', a: 0.45},
			{ y: '12-09', a: 0.44},
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A'],
		gridEnabled: true,
		barColors: ['#01c0c8', '#4dd2d8', '#99e5e9'],
		resize:true,
		stacked: true,
		hideHover: 'auto'
	});
	// MORRIS DONUT CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	Morris.Donut({
		element: 'demo-morris-donut',
		data: [
			{label: "Download Sales", value: 12},
			{label: "In-Store Sales", value: 30},
			{label: "COD Sales", value: 40},
			{label: "Mail-Order Sales", value: 20}
		],
		colors: ['#01c0c8', '#4dd2d8', '#99e5e9', '#33ccd3'],
		resize:true
	});

	// COLORFUL MORRIS DONUT CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	Morris.Donut({
		element: 'demo-morris-color-donut',
		data: [
			{label: "Download Sales", value: 12},
			{label: "In-Store Sales", value: 30},
			{label: "COD Sales", value: 40},
			{label: "Mail-Order Sales", value: 20}
		],
		colors: ['#E9422E', '#FAC552', '#3eb489', '#29b7d3'],
		resize:true
	});

	// FORMATTED MORRIS DONUT CHART
	// =================================================================
	// Require MorrisJS Chart
	// -----------------------------------------------------------------
	// http://morrisjs.github.io/morris.js/
	// =================================================================
	Morris.Donut({
		element: 'demo-morris-formatted-donut',
		data: [
			{label: "Download Sales", value: 12},
			{label: "In-Store Sales", value: 30},
			{label: "COD Sales", value: 40},
			{label: "Mail-Order Sales", value: 20}
		],
		formatter: function (x) { return '$'  + Morris.pad2(x % 60) },
		colors: ['#01c0c8', '#4dd2d8', '#99e5e9', '#33ccd3'],
		resize:true,
	});

});