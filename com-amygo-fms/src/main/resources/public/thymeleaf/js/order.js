new Vue({

	el:"#tasks",
	data: {
		orders:[
        {content:"接起乘客：张三", des: "安亭地铁站4号口", time: "今天 10：20"},
        {content:"收到新订单，在途中", des: "预计行驶时间15分钟", time: "今天 10：27"},
        {content:"放下乘客：张三", des: "总行驶时间：25分钟", time: "今天 11：15"},
        {content:"接起乘客：李四", des: "上海虹桥机场", time: "今天 12：20"},
        {content:"前往充电站充电", des: "预计剩余40分钟充满", time: "今天 14：25"},
        {content:"开往世旭广场停车场", des: "世旭广场地下停车场", time: "今天 18：25"}
    	],
		/*recv_passenger: "概览",
		on_road: "订单记录",
		get_down: "保养记录",
		get_up: "保养记录",
		recharge: "保养记录"*/
	}

});