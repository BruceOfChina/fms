package com.amygo.appserver.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.amygo.appserver.security.JwtUser;

public class UserUtil {
	
	public static JwtUser getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.isAuthenticated()?(JwtUser) authentication.getPrincipal():null;
	}

}
