package com.amygo.appserver.security;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.amygo.persis.domain.User;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
    	String[] authorities = StringUtils.split(user.getAuthority(), ",");
    	List<String> list = Arrays.asList(authorities);
        return new JwtUser(
                user.getId(),
                user.getTelephone(),
                user.getPassword(),
                mapToGrantedAuthorities(list),
                user.getLastPasswordResetDate(),
                user.getGuid(),
                user.getNickname()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> authorities) {
        return authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}

