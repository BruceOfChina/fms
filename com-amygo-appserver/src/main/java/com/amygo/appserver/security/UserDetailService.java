package com.amygo.appserver.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.amygo.persis.domain.User;
import com.amygo.persis.repository.UserJpaRepository;
@Component
public class UserDetailService implements UserDetailsService{
	@Autowired
	private UserJpaRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByTelephone(username).orElse(null);
		if(user==null) {
			throw new UsernameNotFoundException("用户名不存在");
		} else {
			return JwtUserFactory.create(user);
		}
	}

}
