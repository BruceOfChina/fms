package com.amygo.appserver.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.amygo.appserver.service.ApiLogService;
import com.amygo.appserver.service.UserTokenService;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.common.util.CommonUtils;
import com.amygo.persis.domain.ApiLog;
import com.amygo.persis.domain.UserToken;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
	
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;
    
    @Autowired
    private ApiLogService apiLogService;
    
    @Autowired
    private UserTokenService userTokenService;
    
    @Autowired
    private CommonUtils commonUtils;
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authHeader = request.getHeader(this.tokenHeader);
        if (authHeader != null && authHeader.startsWith(tokenHead)) {
            final String authToken = authHeader.substring(tokenHead.length()); // The part after "Bearer "
            String username = jwtTokenUtil.getUsernameFromToken(authToken);

            logger.info("checking authentication " + username);

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                JwtUser jwtUser = (JwtUser)userDetails;
                UserToken userToken = userTokenService.getUserToken(jwtUser.getId()).orElse(null);
                if(userToken!=null&&userToken.getIsExpired()==0&&userToken.getExpireTime().compareTo(new Date())==1) {
                	 if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                         UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                                 userDetails, null, userDetails.getAuthorities());
                         authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(
                                 request));
                         logger.info("authenticated user " + username + ", setting security context");
                         SecurityContextHolder.getContext().setAuthentication(authentication);
                     }
                }else if(userToken!=null&&(userToken.getIsExpired()==1||userToken.getExpireTime().compareTo(new Date())!=1)) {
                	logger.info("user " + username + ", token expired");
                	BaseResult br = new BaseResult();
                	br.setResultCode(BaseResult.ERROR);
                	br.setMessageCode(ApiResConstants.TOKEN_INVALID);
                	br.setMessage("token expired");
                	response.getWriter().write(commonUtils.toJson(br));//response返回api结果
                	return;
                }
            }else {
            	Long time = System.currentTimeMillis();
            	ApiLog apiLog = new ApiLog();
        		apiLog.setUrl(request.getRequestURL().toString());
        		apiLog.setHttpMethod(request.getMethod());
        		apiLog.setServerTime(time);
        		apiLog.setIpAddress(request.getRemoteAddr());
        		apiLog.setUserAgent(request.getHeader("User-Agent"));
        		apiLog.setDuration(0L);
        		apiLog.setServerType("com-amygo-appserver");
        		apiLog.setUserGuid("anonymousUser");
        		apiLogService.saveApiLog(apiLog);
        		log.info(apiLog.toString());
            }
        }
        filterChain.doFilter(request, response);
	}
}
