package com.amygo.appserver.security;

import java.io.Serializable;

public class  JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    private String telephone;
    private String password;
    private String token;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String telephone, String password) {
        this.setTelephone(telephone);
        this.setPassword(password);
    }

    public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
