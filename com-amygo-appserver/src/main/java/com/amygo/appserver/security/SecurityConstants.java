package com.amygo.appserver.security;

public class SecurityConstants {
	public static final String LOGIN_PHONE_NUMBER = "login.phoneNumber";
	public static final String LOGIN_SMS_CODE = "login.smsCode";
	public static final String LOGIN_SMS_CODE_TIME = "login.smsCode.time";
	
	public static final String LOGIN_CAPTCHA = "login.captcha";
	public static final String LOGIN_ONLINE_STATUS = "login.online.status";
	
	public static final String PHONE_NUMBER_PARAM_KEY = "phoneNo"; //old value: phoneNumber
	
	//add for: update phone number
	public static final String UPDATE_PHONE_NUMBER = "update.phoneNumber";
	public static final String UPDATE_PHONE_NUMBER_SMS_CODE = "updatePhoneNumber.smsCode";
	
}
