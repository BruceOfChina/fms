package com.amygo.appserver.scheduler;

import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import com.amygo.appserver.dto.VehicleResultDTO;
import com.amygo.common.enums.CarStatusDetailEnum;
import com.amygo.common.enums.CarStatusEnum;
import com.amygo.common.enums.OrderStatusEnum;
import com.amygo.common.util.ApplicationContextUtil;
import com.amygo.common.util.CoordinateConvertUtil;
import com.amygo.common.util.HttpUtil;
import com.amygo.common.util.LocationUtil;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.CarStatusRecord;
import com.amygo.persis.domain.Order;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.CarStatusJpaRepository;
import com.amygo.persis.repository.CarStatusRecordJpaRepository;
import com.amygo.persis.repository.OrderJpaRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//@Component
public class CarStatusScheduler implements DisposableBean{
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CarStatusRecordJpaRepository carStatusRecordJpaRepository;
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	@Autowired
	private OrderJpaRepository orderJpaRepository;
	@Autowired
	private CarJpaRepository carJpaRepository;
	
	@Value("${car.arrive.distance:}")
	private Integer carArriveDistance;
	
	@Scheduled(cron = "0/1 * * * * *")//每1秒执行一次
	public void work() {
		String url = "http://app.singulato.com/vehicle/center/v1/vehicle/detail/2c92838864da74fd0164ea0369ce0006";
    	String result = HttpUtil.clientGet(url);
//    	System.out.println(result);
    	ObjectMapper jsonMapper = new ObjectMapper();
		VehicleResultDTO vehicleStatusDTO;
		try {
			vehicleStatusDTO = jsonMapper.readValue(result, VehicleResultDTO.class);
			if(vehicleStatusDTO.getCode()!=200) {
				log.error("call api from tbox error.......url:"+url);
			}
			CarStatusRecord vehicleDetail = vehicleStatusDTO.getBusinessObj().getVehicleDetail();
			carStatusRecordJpaRepository.save(vehicleDetail);
			String vinCode = vehicleStatusDTO.getBusinessObj().getVin();
			CarStatus carStatus = carStatusJpaRepository.findCarStatusByVinCode(vinCode).orElse(new CarStatus());
			carStatus.setLat(vehicleDetail.getLatitude());
			carStatus.setLon(vehicleDetail.getLongitude());
			carStatusJpaRepository.save(carStatus);
			//判断车辆是否到达起点、终点
			Order order = orderJpaRepository.findOrdersByVinCode(vinCode).orElse(null);
			if(order==null||order.getStartLat()==null||order.getStartLon()==null||order.getEndLat()==null||order.getEndLon()==null) {
				log.error("vinCode binded order is null or lat , lon is null,vinCode"+vinCode);
				return;
			}
			//终点的经纬度
			double lat1 = vehicleDetail.getLatitude();//火星坐标
			double lon1 = vehicleDetail.getLongitude();
			if(lat1==0||lon1==0) {
				log.error("car located failure , vinCode"+vinCode);
				return;
			}
			double[] latLon = CoordinateConvertUtil.wgs2GCJ(order.getEndLat(),order.getEndLon());
			
			double distance = LocationUtil.getDistance(latLon[0], latLon[1], lat1, lon1);
			if(distance<=carArriveDistance) {//当前位置和终点位置距离差值<10m,认为车辆已到达;
				log.info("car arrive to end point.....");
				if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
					return;
				}
				order.setArriveTime(new Date());
				order.setOrderStatus(OrderStatusEnum.ORDER_TO_PAY.getStatus());
				orderJpaRepository.save(order);
				Car car = carJpaRepository.findOne(carStatus.getId());
				car.setStatusDetail(CarStatusDetailEnum.CAR_ARRIVE_END_POINT.getStatus());
				car.setStatus(CarStatusEnum.CAR_TO_USE.getStatus());
				car.setOrderCode(null);
				carJpaRepository.save(car);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//spring在卸载（销毁）实例时，会调用实例的destroy方法。通过实现DisposableBean接口覆盖destroy方法实现。在destroy方法中主动关闭线程。
	@Override
	public void destroy() throws Exception {
		//关闭线程或线程池
        ThreadPoolTaskScheduler scheduler = (ThreadPoolTaskScheduler)ApplicationContextUtil.getBean("scheduler");
        scheduler.shutdown();
	}
}
