package com.amygo.appserver.dto.response;

import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.Order;

public class OrderDetailDTO {
	private Order order;
	private Car car;
	private CarStatus carStatus;

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public CarStatus getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(CarStatus carStatus) {
		this.carStatus = carStatus;
	}

	public OrderDetailDTO(Order order, Car car, CarStatus carStatus) {
		super();
		this.order = order;
		this.car = car;
		this.carStatus = carStatus;
	}
}
