package com.amygo.appserver.dto.request;

public class CarSeatDTO {
	private Integer seat_master_back_adj;// 主驾座椅靠背角度调节 ,
	private boolean seat_master_heater;// 主驾座椅温度加热 ,
	private Integer seat_master_hight_pos;// 主座椅高度调节 ,
	private Integer seat_master_horiz_adj;// 主驾座椅前后位置调节 ,
	private Integer seat_slave_back_adj;// 副驾座椅靠背角度调节 ,
	private boolean seat_slave_heater;// 副驾座椅温度加热 ,
	private Integer seat_slave_hight_pos;// 副驾座椅高度调节 ,
	private Integer seat_slave_horiz_adj;// 副驾座椅前后位置调节

	public Integer getSeat_master_back_adj() {
		return seat_master_back_adj;
	}

	public void setSeat_master_back_adj(Integer seat_master_back_adj) {
		this.seat_master_back_adj = seat_master_back_adj;
	}

	public boolean isSeat_master_heater() {
		return seat_master_heater;
	}

	public void setSeat_master_heater(boolean seat_master_heater) {
		this.seat_master_heater = seat_master_heater;
	}

	public Integer getSeat_master_hight_pos() {
		return seat_master_hight_pos;
	}

	public void setSeat_master_hight_pos(Integer seat_master_hight_pos) {
		this.seat_master_hight_pos = seat_master_hight_pos;
	}

	public Integer getSeat_master_horiz_adj() {
		return seat_master_horiz_adj;
	}

	public void setSeat_master_horiz_adj(Integer seat_master_horiz_adj) {
		this.seat_master_horiz_adj = seat_master_horiz_adj;
	}

	public Integer getSeat_slave_back_adj() {
		return seat_slave_back_adj;
	}

	public void setSeat_slave_back_adj(Integer seat_slave_back_adj) {
		this.seat_slave_back_adj = seat_slave_back_adj;
	}

	public boolean isSeat_slave_heater() {
		return seat_slave_heater;
	}

	public void setSeat_slave_heater(boolean seat_slave_heater) {
		this.seat_slave_heater = seat_slave_heater;
	}

	public Integer getSeat_slave_hight_pos() {
		return seat_slave_hight_pos;
	}

	public void setSeat_slave_hight_pos(Integer seat_slave_hight_pos) {
		this.seat_slave_hight_pos = seat_slave_hight_pos;
	}

	public Integer getSeat_slave_horiz_adj() {
		return seat_slave_horiz_adj;
	}

	public void setSeat_slave_horiz_adj(Integer seat_slave_horiz_adj) {
		this.seat_slave_horiz_adj = seat_slave_horiz_adj;
	}
}
