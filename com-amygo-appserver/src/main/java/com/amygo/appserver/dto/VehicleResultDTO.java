package com.amygo.appserver.dto;

public class VehicleResultDTO {
	
	private Integer code;
	private String message;
	private VehicleStatusDTO businessObj;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public VehicleStatusDTO getBusinessObj() {
		return businessObj;
	}
	public void setBusinessObj(VehicleStatusDTO businessObj) {
		this.businessObj = businessObj;
	}
	

}
