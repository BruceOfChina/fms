package com.amygo.appserver.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class EndOrderRequestDTO {
	@NotBlank
	private String ipcId;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}
}
