package com.amygo.appserver.dto.request;

public class OrderAddStationDTO extends OrderBaseDTO{
	
	private String stationAddress;
	
	private Double lon;
	
	private Double lat;

	public OrderAddStationDTO(String stationAddress, Double lon, Double lat) {
		super();
		this.stationAddress = stationAddress;
		this.lon = lon;
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getStationAddress() {
		return stationAddress;
	}

	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}
}
