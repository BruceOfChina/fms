package com.amygo.appserver.dto.request;

public class UserInitRequestDTO{
	private String address;
	
	private Double initLat;
	
	private Double initLon;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getInitLat() {
		return initLat;
	}

	public void setInitLat(Double initLat) {
		this.initLat = initLat;
	}

	public Double getInitLon() {
		return initLon;
	}

	public void setInitLon(Double initLon) {
		this.initLon = initLon;
	}

	public UserInitRequestDTO(String address, Double initLat, Double initLon) {
		super();
		this.address = address;
		this.initLat = initLat;
		this.initLon = initLon;
	}
}
