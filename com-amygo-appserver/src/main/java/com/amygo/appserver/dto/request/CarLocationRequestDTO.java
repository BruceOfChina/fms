package com.amygo.appserver.dto.request;

public class CarLocationRequestDTO {
	
	private String vinCode;

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}
}
