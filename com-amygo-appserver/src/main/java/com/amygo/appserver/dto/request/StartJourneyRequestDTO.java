package com.amygo.appserver.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class StartJourneyRequestDTO {
	@NotBlank
	private String vinCode;

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}
}
