package com.amygo.appserver.dto.request;

public class AirConditionerDTO {

	private boolean ac_auto_mode; // 自动模式 ,
	private Integer ac_cycle_mode; // 循环模式枚举 ,
	private boolean ac_defrost_rear; // 后除霜开关 ,
	private Integer ac_driverside_temp; // 主驾温度设定 ,
	private boolean ac_force_cooling; // 强制制冷 ,
	private boolean ac_force_heating; // 强制制热 ,
	private Integer ac_passagerside_temp; // 副驾温度设定 ,
	private boolean ac_purification; // 空气净化开关 ,
	private boolean ac_switch; // 空调开关 ,
	private Integer ac_wind_dir; // 风向枚举 ,
	private Integer ac_wind_speed; // 风速枚举

	public boolean isAc_auto_mode() {
		return ac_auto_mode;
	}

	public void setAc_auto_mode(boolean ac_auto_mode) {
		this.ac_auto_mode = ac_auto_mode;
	}

	public Integer getAc_cycle_mode() {
		return ac_cycle_mode;
	}

	public void setAc_cycle_mode(Integer ac_cycle_mode) {
		this.ac_cycle_mode = ac_cycle_mode;
	}

	public boolean isAc_defrost_rear() {
		return ac_defrost_rear;
	}

	public void setAc_defrost_rear(boolean ac_defrost_rear) {
		this.ac_defrost_rear = ac_defrost_rear;
	}

	public Integer getAc_driverside_temp() {
		return ac_driverside_temp;
	}

	public void setAc_driverside_temp(Integer ac_driverside_temp) {
		this.ac_driverside_temp = ac_driverside_temp;
	}

	public boolean isAc_force_cooling() {
		return ac_force_cooling;
	}

	public void setAc_force_cooling(boolean ac_force_cooling) {
		this.ac_force_cooling = ac_force_cooling;
	}

	public boolean isAc_force_heating() {
		return ac_force_heating;
	}

	public void setAc_force_heating(boolean ac_force_heating) {
		this.ac_force_heating = ac_force_heating;
	}

	public Integer getAc_passagerside_temp() {
		return ac_passagerside_temp;
	}

	public void setAc_passagerside_temp(Integer ac_passagerside_temp) {
		this.ac_passagerside_temp = ac_passagerside_temp;
	}

	public boolean isAc_purification() {
		return ac_purification;
	}

	public void setAc_purification(boolean ac_purification) {
		this.ac_purification = ac_purification;
	}

	public boolean isAc_switch() {
		return ac_switch;
	}

	public void setAc_switch(boolean ac_switch) {
		this.ac_switch = ac_switch;
	}

	public Integer getAc_wind_dir() {
		return ac_wind_dir;
	}

	public void setAc_wind_dir(Integer ac_wind_dir) {
		this.ac_wind_dir = ac_wind_dir;
	}

	public Integer getAc_wind_speed() {
		return ac_wind_speed;
	}

	public void setAc_wind_speed(Integer ac_wind_speed) {
		this.ac_wind_speed = ac_wind_speed;
	}
}
