package com.amygo.appserver.dto.request;

public class OrderModifyDestinationDTO extends OrderBaseDTO{
	
	private String destinationAddress;
	
	private Double lon;
	
	private Double lat;

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}
}
