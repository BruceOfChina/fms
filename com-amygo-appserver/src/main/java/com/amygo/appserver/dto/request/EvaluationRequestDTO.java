package com.amygo.appserver.dto.request;

public class EvaluationRequestDTO {
	
	private String evaluationContent;
	
	private Integer isSatisfied;
	
	private Integer evaluationScore;

	public String getEvaluationContent() {
		return evaluationContent;
	}

	public void setEvaluationContent(String evaluationContent) {
		this.evaluationContent = evaluationContent;
	}

	public Integer getIsSatisfied() {
		return isSatisfied;
	}

	public void setIsSatisfied(Integer isSatisfied) {
		this.isSatisfied = isSatisfied;
	}

	public Integer getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(Integer evaluationScore) {
		this.evaluationScore = evaluationScore;
	}
}
