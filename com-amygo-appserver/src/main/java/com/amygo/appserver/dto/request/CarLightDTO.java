package com.amygo.appserver.dto.request;

public class CarLightDTO {
	private boolean light_ambient_light ;// 氛围灯 ,
	private boolean light_emergency_light ;// 双闪灯 ,
	private boolean light_front_fog_light ;// 前雾灯 ,
	private boolean light_highbeam ;// 远光灯 ,
	private boolean light_lowbeam ;// 近光灯 ,
	private boolean light_position_light ;// 示宽灯 ,
	private boolean light_rear_fog_light ;// 后雾灯
	public boolean isLight_ambient_light() {
		return light_ambient_light;
	}
	public void setLight_ambient_light(boolean light_ambient_light) {
		this.light_ambient_light = light_ambient_light;
	}
	public boolean isLight_emergency_light() {
		return light_emergency_light;
	}
	public void setLight_emergency_light(boolean light_emergency_light) {
		this.light_emergency_light = light_emergency_light;
	}
	public boolean isLight_front_fog_light() {
		return light_front_fog_light;
	}
	public void setLight_front_fog_light(boolean light_front_fog_light) {
		this.light_front_fog_light = light_front_fog_light;
	}
	public boolean isLight_highbeam() {
		return light_highbeam;
	}
	public void setLight_highbeam(boolean light_highbeam) {
		this.light_highbeam = light_highbeam;
	}
	public boolean isLight_lowbeam() {
		return light_lowbeam;
	}
	public void setLight_lowbeam(boolean light_lowbeam) {
		this.light_lowbeam = light_lowbeam;
	}
	public boolean isLight_position_light() {
		return light_position_light;
	}
	public void setLight_position_light(boolean light_position_light) {
		this.light_position_light = light_position_light;
	}
	public boolean isLight_rear_fog_light() {
		return light_rear_fog_light;
	}
	public void setLight_rear_fog_light(boolean light_rear_fog_light) {
		this.light_rear_fog_light = light_rear_fog_light;
	}
	
}
