package com.amygo.appserver.dto;

public class EpVehicleView {
	
	private String id;
	private String vid;
	private String bodyNumber;
	private String epVehicleNumber;
	private String epVehicleUsage;
	private String usePerson;
	private String usePersonMobile;
	private String epVehicleAlias;
	private String usePersonGuid;
	private Integer visiableInMap;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVid() {
		return vid;
	}
	public void setVid(String vid) {
		this.vid = vid;
	}
	public String getBodyNumber() {
		return bodyNumber;
	}
	public void setBodyNumber(String bodyNumber) {
		this.bodyNumber = bodyNumber;
	}
	public String getEpVehicleNumber() {
		return epVehicleNumber;
	}
	public void setEpVehicleNumber(String epVehicleNumber) {
		this.epVehicleNumber = epVehicleNumber;
	}
	public String getEpVehicleUsage() {
		return epVehicleUsage;
	}
	public void setEpVehicleUsage(String epVehicleUsage) {
		this.epVehicleUsage = epVehicleUsage;
	}
	public String getUsePerson() {
		return usePerson;
	}
	public void setUsePerson(String usePerson) {
		this.usePerson = usePerson;
	}
	public String getUsePersonMobile() {
		return usePersonMobile;
	}
	public void setUsePersonMobile(String usePersonMobile) {
		this.usePersonMobile = usePersonMobile;
	}
	public String getEpVehicleAlias() {
		return epVehicleAlias;
	}
	public void setEpVehicleAlias(String epVehicleAlias) {
		this.epVehicleAlias = epVehicleAlias;
	}
	public String getUsePersonGuid() {
		return usePersonGuid;
	}
	public void setUsePersonGuid(String usePersonGuid) {
		this.usePersonGuid = usePersonGuid;
	}
	public Integer getVisiableInMap() {
		return visiableInMap;
	}
	public void setVisiableInMap(Integer visiableInMap) {
		this.visiableInMap = visiableInMap;
	}
}
