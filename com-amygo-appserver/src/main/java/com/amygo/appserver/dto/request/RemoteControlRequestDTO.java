package com.amygo.appserver.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RemoteControlRequestDTO {
	private AirConditionerDTO air_conditioning ;//(空调请求封装类, optional),
	private DoorLockDTO door ;//(门锁请求封装类, optional),
	private EnergyStatusDTO energy ;//(能量状态请求封装类, optional),
	private CarLightDTO light ;//(灯光请求封装类, optional),
	private CarMediaDTO media ;//(多媒体请求封装类, optional),
	private CarSeatDTO seat ;//(座椅参数封装类, optional),
	private CarWindowDTO window ;//(窗请求封装类, optional)
	
	private Integer auto_parking ;// 0 未知 1 垂直 2 侧方 3 中断 ,
	private boolean engine ;//上高压电
	private boolean horn ;//喇叭
	private Integer motion ;// 运动控制 0 未知 1 前 2 后 3 左 4 右 5 停止 ,
	private String vid;
	public AirConditionerDTO getAir_conditioning() {
		return air_conditioning;
	}
	public void setAir_conditioning(AirConditionerDTO air_conditioning) {
		this.air_conditioning = air_conditioning;
	}
	public DoorLockDTO getDoor() {
		return door;
	}
	public void setDoor(DoorLockDTO door) {
		this.door = door;
	}
	public EnergyStatusDTO getEnergy() {
		return energy;
	}
	public void setEnergy(EnergyStatusDTO energy) {
		this.energy = energy;
	}
	public CarLightDTO getLight() {
		return light;
	}
	public void setLight(CarLightDTO light) {
		this.light = light;
	}
	public CarMediaDTO getMedia() {
		return media;
	}
	public void setMedia(CarMediaDTO media) {
		this.media = media;
	}
	public CarSeatDTO getSeat() {
		return seat;
	}
	public void setSeat(CarSeatDTO seat) {
		this.seat = seat;
	}
	public CarWindowDTO getWindow() {
		return window;
	}
	public void setWindow(CarWindowDTO window) {
		this.window = window;
	}
	public Integer getAuto_parking() {
		return auto_parking;
	}
	public void setAuto_parking(Integer auto_parking) {
		this.auto_parking = auto_parking;
	}
	public boolean isEngine() {
		return engine;
	}
	public void setEngine(boolean engine) {
		this.engine = engine;
	}
	public boolean isHorn() {
		return horn;
	}
	public void setHorn(boolean horn) {
		this.horn = horn;
	}
	public Integer getMotion() {
		return motion;
	}
	public void setMotion(Integer motion) {
		this.motion = motion;
	}
	public String getVid() {
		return vid;
	}
	public void setVid(String vid) {
		this.vid = vid;
	}
}
