package com.amygo.appserver.dto.request;

public class DoorLockDTO {
	private boolean door_four;// 全部四个门 ,
	private boolean door_left_front;// 左前门 ,
	private boolean door_left_rear;// 左后门 ,
	private boolean door_luggage_front;// 前行李箱 ,
	private boolean door_luggage_rear;// 后行李箱 ,
	private boolean door_right_front;// 右前门 ,
	private boolean door_right_rear;// 右后门

	public boolean isDoor_four() {
		return door_four;
	}

	public void setDoor_four(boolean door_four) {
		this.door_four = door_four;
	}

	public boolean isDoor_left_front() {
		return door_left_front;
	}

	public void setDoor_left_front(boolean door_left_front) {
		this.door_left_front = door_left_front;
	}

	public boolean isDoor_left_rear() {
		return door_left_rear;
	}

	public void setDoor_left_rear(boolean door_left_rear) {
		this.door_left_rear = door_left_rear;
	}

	public boolean isDoor_luggage_front() {
		return door_luggage_front;
	}

	public void setDoor_luggage_front(boolean door_luggage_front) {
		this.door_luggage_front = door_luggage_front;
	}

	public boolean isDoor_luggage_rear() {
		return door_luggage_rear;
	}

	public void setDoor_luggage_rear(boolean door_luggage_rear) {
		this.door_luggage_rear = door_luggage_rear;
	}

	public boolean isDoor_right_front() {
		return door_right_front;
	}

	public void setDoor_right_front(boolean door_right_front) {
		this.door_right_front = door_right_front;
	}

	public boolean isDoor_right_rear() {
		return door_right_rear;
	}

	public void setDoor_right_rear(boolean door_right_rear) {
		this.door_right_rear = door_right_rear;
	}
}
