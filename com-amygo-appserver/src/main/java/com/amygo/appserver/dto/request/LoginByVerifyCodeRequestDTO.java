package com.amygo.appserver.dto.request;

public class LoginByVerifyCodeRequestDTO {
	private Integer verifyCode;

	private String telephone;

	public Integer getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(Integer verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
