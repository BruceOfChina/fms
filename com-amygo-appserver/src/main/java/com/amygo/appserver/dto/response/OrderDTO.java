package com.amygo.appserver.dto.response;

import com.amygo.appserver.vo.CarVo;
import com.amygo.persis.domain.Order;

public class OrderDTO{
	private Order order;
	private CarVo carVo;

	public CarVo getCarVo() {
		return carVo;
	}

	public void setCarVo(CarVo carVo) {
		this.carVo = carVo;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public OrderDTO(CarVo carVo,Order order) {
		super();
		this.order = order;
		this.carVo = carVo;
	}
}
