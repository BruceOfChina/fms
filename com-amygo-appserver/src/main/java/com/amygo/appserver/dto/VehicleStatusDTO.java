package com.amygo.appserver.dto;

import com.amygo.persis.domain.CarStatusRecord;

public class VehicleStatusDTO {
	private String id;
	private String vin;
	private String vinBornAt;
	private String vname;
	private String plateType;
	private String plateColor;
	private String lifecycle;
	private String vnameBornAt;
	private String vehicleBornAt;
	private String vehicleSaleAt;
	private String vehiclePurposeType;
	private String vehiclePurposeEp1Test;
	private String vehiclePurpose;
	private String ownerGuid;
	private Integer oldGuid;
	private EpVehicleView epVehicleView;
	private CarStatusRecord vehicleDetail;
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getVinBornAt() {
		return vinBornAt;
	}
	public void setVinBornAt(String vinBornAt) {
		this.vinBornAt = vinBornAt;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getPlateType() {
		return plateType;
	}
	public void setPlateType(String plateType) {
		this.plateType = plateType;
	}
	public String getPlateColor() {
		return plateColor;
	}
	public void setPlateColor(String plateColor) {
		this.plateColor = plateColor;
	}
	public String getLifecycle() {
		return lifecycle;
	}
	public void setLifecycle(String lifecycle) {
		this.lifecycle = lifecycle;
	}
	public String getVnameBornAt() {
		return vnameBornAt;
	}
	public void setVnameBornAt(String vnameBornAt) {
		this.vnameBornAt = vnameBornAt;
	}
	public String getVehicleBornAt() {
		return vehicleBornAt;
	}
	public void setVehicleBornAt(String vehicleBornAt) {
		this.vehicleBornAt = vehicleBornAt;
	}
	public String getVehicleSaleAt() {
		return vehicleSaleAt;
	}
	public void setVehicleSaleAt(String vehicleSaleAt) {
		this.vehicleSaleAt = vehicleSaleAt;
	}
	public String getVehiclePurposeType() {
		return vehiclePurposeType;
	}
	public void setVehiclePurposeType(String vehiclePurposeType) {
		this.vehiclePurposeType = vehiclePurposeType;
	}
	public String getVehiclePurposeEp1Test() {
		return vehiclePurposeEp1Test;
	}
	public void setVehiclePurposeEp1Test(String vehiclePurposeEp1Test) {
		this.vehiclePurposeEp1Test = vehiclePurposeEp1Test;
	}
	public String getVehiclePurpose() {
		return vehiclePurpose;
	}
	public void setVehiclePurpose(String vehiclePurpose) {
		this.vehiclePurpose = vehiclePurpose;
	}
	public String getOwnerGuid() {
		return ownerGuid;
	}
	public void setOwnerGuid(String ownerGuid) {
		this.ownerGuid = ownerGuid;
	}
	public Integer getOldGuid() {
		return oldGuid;
	}
	public void setOldGuid(Integer oldGuid) {
		this.oldGuid = oldGuid;
	}
	public EpVehicleView getEpVehicleView() {
		return epVehicleView;
	}
	public void setEpVehicleView(EpVehicleView epVehicleView) {
		this.epVehicleView = epVehicleView;
	}

	public CarStatusRecord getVehicleDetail() {
		return vehicleDetail;
	}
	public void setVehicleDetail(CarStatusRecord vehicleDetail) {
		this.vehicleDetail = vehicleDetail;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
