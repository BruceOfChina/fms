package com.amygo.appserver.dto.request;

public class EnergyStatusDTO {
	private Integer energy_charge_status;// 充电状态 ,
	private Integer energy_remain_capacity;// 电池剩余容量 ,
	private Integer energy_remain_mileage;// 剩余里程

	public Integer getEnergy_charge_status() {
		return energy_charge_status;
	}

	public void setEnergy_charge_status(Integer energy_charge_status) {
		this.energy_charge_status = energy_charge_status;
	}

	public Integer getEnergy_remain_capacity() {
		return energy_remain_capacity;
	}

	public void setEnergy_remain_capacity(Integer energy_remain_capacity) {
		this.energy_remain_capacity = energy_remain_capacity;
	}

	public Integer getEnergy_remain_mileage() {
		return energy_remain_mileage;
	}

	public void setEnergy_remain_mileage(Integer energy_remain_mileage) {
		this.energy_remain_mileage = energy_remain_mileage;
	}
}
