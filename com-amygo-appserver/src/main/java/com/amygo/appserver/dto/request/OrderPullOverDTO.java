package com.amygo.appserver.dto.request;

public class OrderPullOverDTO extends OrderBaseDTO{
	
	private String pullOverAddress;
	
	private Double lon;
	
	private Double lat;

	public OrderPullOverDTO(String pullOverAddress, Double lon, Double lat) {
		super();
		this.pullOverAddress = pullOverAddress;
		this.lon = lon;
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getPullOverAddress() {
		return pullOverAddress;
	}

	public void setPullOverAddress(String pullOverAddress) {
		this.pullOverAddress = pullOverAddress;
	}
}
