package com.amygo.appserver.dto.request;

public class OrderRequestDTO {

	private Double startLat;

	private Double startLon;

	private Double endLat;

	private Double endLon;
	
	private String startAddress;
	
	private String endAddress;

	public Double getStartLat() {
		return startLat;
	}

	public void setStartLat(Double startLat) {
		this.startLat = startLat;
	}

	public Double getStartLon() {
		return startLon;
	}

	public void setStartLon(Double startLon) {
		this.startLon = startLon;
	}

	public Double getEndLat() {
		return endLat;
	}

	public void setEndLat(Double endLat) {
		this.endLat = endLat;
	}

	public Double getEndLon() {
		return endLon;
	}

	public void setEndLon(Double endLon) {
		this.endLon = endLon;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}
}
