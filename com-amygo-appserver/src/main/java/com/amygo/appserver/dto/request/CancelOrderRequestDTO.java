package com.amygo.appserver.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class CancelOrderRequestDTO {
	@NotBlank
	private String orderCode;
	
	private String cancelReason;

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
}
