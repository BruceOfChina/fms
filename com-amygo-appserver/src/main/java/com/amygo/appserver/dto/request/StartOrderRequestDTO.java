package com.amygo.appserver.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class StartOrderRequestDTO {
	@NotBlank
	private String orderCode;
	
	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

}
