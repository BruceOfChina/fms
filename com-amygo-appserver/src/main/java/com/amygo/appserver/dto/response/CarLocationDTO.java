package com.amygo.appserver.dto.response;

public class CarLocationDTO {
	private Double lat;
	
	private Double lon;
	
	private Integer statusDetail;//车辆当前状态
	
	private String orderCode;//绑定的订单号

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Integer getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public CarLocationDTO(Double lat, Double lon, Integer statusDetail, String orderCode) {
		super();
		this.lat = lat;
		this.lon = lon;
		this.statusDetail = statusDetail;
		this.orderCode = orderCode;
	}
}
