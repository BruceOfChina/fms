package com.amygo.appserver.dto.request;

public class CarMediaDTO {
	private String media_name ;// 多媒体名称 ,
	private Integer media_play_control ;// 多媒体控制 ,
	private Integer media_play_progress ;// 多媒体播放进度 ,
	private Integer media_play_status ;// 多媒体播放状态
	public String getMedia_name() {
		return media_name;
	}
	public void setMedia_name(String media_name) {
		this.media_name = media_name;
	}
	public Integer getMedia_play_control() {
		return media_play_control;
	}
	public void setMedia_play_control(Integer media_play_control) {
		this.media_play_control = media_play_control;
	}
	public Integer getMedia_play_progress() {
		return media_play_progress;
	}
	public void setMedia_play_progress(Integer media_play_progress) {
		this.media_play_progress = media_play_progress;
	}
	public Integer getMedia_play_status() {
		return media_play_status;
	}
	public void setMedia_play_status(Integer media_play_status) {
		this.media_play_status = media_play_status;
	}
}
