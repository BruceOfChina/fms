package com.amygo.appserver.dto;

public class VehicleDetail {
	private String guid;
	private String lastPowerOnTime;//上次上电时间
	private Double longitude;//经度
	private Double latitude;//纬度
	private String powerModel;//电源模式
	private String gear;//档位
	private String faultModel;//故障模式
	private Double mileage;//里程
	private Double speed;//车辆行驶速度
	private boolean highVoltage;//是否高压
	private boolean charge;//是否充电
	private String direction;//朝向
	private Double gpsPhysicalAddr;//GPS的地址
	private Double soc;//剩余电量
	private Double onlineRate;//在线率
	private Double workingTimeOnlineRate;//工作时间在线率？
	private Double speedRate;//速率？
	private Integer faultAmount;//故障数量
	private Integer chargingTime;//充电时间
	private Long travelTime;//行驶时间
	private Long totalOnlineTime;//总在线时间
	private Long totalChargingTime;//总充电时间

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getLastPowerOnTime() {
		return lastPowerOnTime;
	}

	public void setLastPowerOnTime(String lastPowerOnTime) {
		this.lastPowerOnTime = lastPowerOnTime;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getPowerModel() {
		return powerModel;
	}

	public void setPowerModel(String powerModel) {
		this.powerModel = powerModel;
	}

	public String getGear() {
		return gear;
	}

	public void setGear(String gear) {
		this.gear = gear;
	}

	public String getFaultModel() {
		return faultModel;
	}

	public void setFaultModel(String faultModel) {
		this.faultModel = faultModel;
	}

	public Double getMileage() {
		return mileage;
	}

	public void setMileage(Double mileage) {
		this.mileage = mileage;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public boolean isHighVoltage() {
		return highVoltage;
	}

	public void setHighVoltage(boolean highVoltage) {
		this.highVoltage = highVoltage;
	}

	public boolean isCharge() {
		return charge;
	}

	public void setCharge(boolean charge) {
		this.charge = charge;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Double getGpsPhysicalAddr() {
		return gpsPhysicalAddr;
	}

	public void setGpsPhysicalAddr(Double gpsPhysicalAddr) {
		this.gpsPhysicalAddr = gpsPhysicalAddr;
	}

	public Double getSoc() {
		return soc;
	}

	public void setSoc(Double soc) {
		this.soc = soc;
	}

	public Double getOnlineRate() {
		return onlineRate;
	}

	public void setOnlineRate(Double onlineRate) {
		this.onlineRate = onlineRate;
	}

	public Double getWorkingTimeOnlineRate() {
		return workingTimeOnlineRate;
	}

	public void setWorkingTimeOnlineRate(Double workingTimeOnlineRate) {
		this.workingTimeOnlineRate = workingTimeOnlineRate;
	}

	public Double getSpeedRate() {
		return speedRate;
	}

	public void setSpeedRate(Double speedRate) {
		this.speedRate = speedRate;
	}

	public Integer getFaultAmount() {
		return faultAmount;
	}

	public void setFaultAmount(Integer faultAmount) {
		this.faultAmount = faultAmount;
	}

	public Integer getChargingTime() {
		return chargingTime;
	}

	public void setChargingTime(Integer chargingTime) {
		this.chargingTime = chargingTime;
	}

	public Long getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(Long travelTime) {
		this.travelTime = travelTime;
	}

	public Long getTotalOnlineTime() {
		return totalOnlineTime;
	}

	public void setTotalOnlineTime(Long totalOnlineTime) {
		this.totalOnlineTime = totalOnlineTime;
	}

	public Long getTotalChargingTime() {
		return totalChargingTime;
	}

	public void setTotalChargingTime(Long totalChargingTime) {
		this.totalChargingTime = totalChargingTime;
	}
}
