package com.amygo.appserver.dto.request;

public class OrderResumeDTO extends OrderBaseDTO{
	
	private String resumeAddress;
	
	private Double lon;
	
	private Double lat;

	public OrderResumeDTO(String resumeAddress, Double lon, Double lat) {
		super();
		this.resumeAddress = resumeAddress;
		this.lon = lon;
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public String getResumeAddress() {
		return resumeAddress;
	}

	public void setResumeAddress(String resumeAddress) {
		this.resumeAddress = resumeAddress;
	}
}
