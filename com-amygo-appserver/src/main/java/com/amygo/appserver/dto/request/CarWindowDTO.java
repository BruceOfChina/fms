package com.amygo.appserver.dto.request;

public class CarWindowDTO {
	private boolean window_four;// 全部四个门窗 ,
	private boolean window_left_front;// 左前门窗 ,
	private boolean window_left_rear;// 左后门窗 ,
	private boolean window_right_front;// 右前门窗 ,
	private boolean window_right_rear;// 右后门窗 ,
	private Integer window_sunroof;// 天窗 透明度

	public boolean isWindow_four() {
		return window_four;
	}

	public void setWindow_four(boolean window_four) {
		this.window_four = window_four;
	}

	public boolean isWindow_left_front() {
		return window_left_front;
	}

	public void setWindow_left_front(boolean window_left_front) {
		this.window_left_front = window_left_front;
	}

	public boolean isWindow_left_rear() {
		return window_left_rear;
	}

	public void setWindow_left_rear(boolean window_left_rear) {
		this.window_left_rear = window_left_rear;
	}

	public boolean isWindow_right_front() {
		return window_right_front;
	}

	public void setWindow_right_front(boolean window_right_front) {
		this.window_right_front = window_right_front;
	}

	public boolean isWindow_right_rear() {
		return window_right_rear;
	}

	public void setWindow_right_rear(boolean window_right_rear) {
		this.window_right_rear = window_right_rear;
	}

	public Integer getWindow_sunroof() {
		return window_sunroof;
	}

	public void setWindow_sunroof(Integer window_sunroof) {
		this.window_sunroof = window_sunroof;
	}
}
