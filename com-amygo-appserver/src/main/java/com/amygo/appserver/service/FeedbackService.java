package com.amygo.appserver.service;

import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.appserver.dto.request.FeedbackRequestDTO;
import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.util.UserUtil;
import com.amygo.persis.domain.Feedback;
import com.amygo.persis.repository.FeedbackJpaRepository;
@Service
public class FeedbackService {
	@Autowired
	private FeedbackJpaRepository feedbackJpaRepository;
	@Autowired
	private ModelMapper modelMapper;
	
	public void save(FeedbackRequestDTO dto) {
		Feedback feedback = new Feedback();
		modelMapper.map(dto, feedback);
		JwtUser jwtUser = UserUtil.getCurrentUser();
		feedback.setUserId(jwtUser.getId());
		feedback.setTelephone(jwtUser.getUsername());
		feedback.setIsReplied(Feedback.NOT_REPLIED);
		feedback.setFeedbackTime(new Date());
		feedbackJpaRepository.save(feedback);
	}
}
