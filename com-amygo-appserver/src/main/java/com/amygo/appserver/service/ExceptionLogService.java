package com.amygo.appserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.ExceptionLog;
import com.amygo.persis.repository.ExceptionLogJpaRepository;

@Service
public class ExceptionLogService {
	@Autowired
	private ExceptionLogJpaRepository exceptionLogJpaRepository;
	
	public void saveExceptionLog(ExceptionLog exceptionLog) {
		exceptionLogJpaRepository.save(exceptionLog);
	}
}
