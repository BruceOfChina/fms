package com.amygo.appserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.ApiLog;
import com.amygo.persis.repository.ApiLogJpaRepository;

@Service
public class ApiLogService {
	@Autowired
	private ApiLogJpaRepository apiLogJpaRepository;
	
	public void saveApiLog(ApiLog apiLog) {
		apiLogJpaRepository.save(apiLog);
	}

}
