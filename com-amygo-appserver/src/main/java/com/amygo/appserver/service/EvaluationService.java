package com.amygo.appserver.service;

import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.appserver.dto.request.EvaluationRequestDTO;
import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.util.UserUtil;
import com.amygo.persis.domain.Evaluation;
import com.amygo.persis.domain.Feedback;
import com.amygo.persis.repository.EvaluationJpaRepository;
@Service
public class EvaluationService {
	@Autowired
	private EvaluationJpaRepository evaluationJpaRepository;
	@Autowired
	private ModelMapper modelMapper;
	
	public void save(EvaluationRequestDTO dto) {
		Evaluation evaluation = new Evaluation();
		modelMapper.map(dto, evaluation);
		JwtUser jwtUser = UserUtil.getCurrentUser();
		evaluation.setUserId(jwtUser.getId());
		evaluation.setTelephone(jwtUser.getUsername());
		evaluation.setIsReplied(Feedback.NOT_REPLIED);
		evaluation.setEvaluationTime(new Date());
		evaluationJpaRepository.save(evaluation);
	}
}
