package com.amygo.appserver.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.appserver.vo.CarVo;
import com.amygo.common.enums.CarStatusDetailEnum;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.SystemVariable;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.CarStatusJpaRepository;
import com.amygo.persis.repository.SystemVariableJpaRepository;
import com.amygo.persis.vo.CarStatusVo;

@Service
public class CarService {
	@Autowired
	private CarJpaRepository carJpaRepository;
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	@Autowired
	private SystemVariableJpaRepository systemConfigJpaRepository;
	
	
	
	public void updateCar(Car car) {
		carJpaRepository.save(car);
	}
	
	public Car getCar(String vinCode) {
		return carJpaRepository.findByVinCode(vinCode).orElse(null);
	}
	
	public Car findOne(Long id) {
		return carJpaRepository.findOne(id);
	}
	/**
	 * 此处的搜车算法，需要继续完善
	 * @param map
	 * @return
	 */
	public CarVo findNearestCar(Map<String,Double> map) {
		SystemVariable systemVariableOfElectricity = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ELECTRICITY", "AVA_CAR_ELECTRICITY").orElse(null);
		SystemVariable systemVariableOfEnduranceMileage = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ENDURANCE_MILEAGE", "AVA_CAR_ENDURANCE_MILEAGE")
				.orElse(null);
		Double electricity = Double.parseDouble(systemVariableOfElectricity.getVariableValue());
		Long enduranceMileage = Long.parseLong(systemVariableOfEnduranceMileage.getVariableValue());
		List<CarStatus> list = carStatusJpaRepository.findNearestCar(map.get("lon"),map.get("lat"),electricity,enduranceMileage,10);
		if(!list.isEmpty()) {
			CarStatus carStatus = list.get(0);
			Car car = carJpaRepository.findByVinCode(carStatus.getVinCode()).orElse(new Car());
			CarVo carVo = new CarVo(car.getId(),
					car.getVinCode(),
					car.getPlateNumber(),
					carStatus.getLat(),
					carStatus.getLon(),
					null,
					CarStatusDetailEnum.CAR_WAIT_TO_ORDER.getStatus(),
					car.getVehicleModel());
			return carVo;
		}
		return null;
	}
}
