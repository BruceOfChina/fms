package com.amygo.appserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.repository.CarStatusJpaRepository;

@Service
public class CarStatusService {
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	
	public CarStatus getCarStatus(Long carId) {
		return carStatusJpaRepository.findOne(carId);
	}

}
