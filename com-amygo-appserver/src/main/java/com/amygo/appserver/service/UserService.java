package com.amygo.appserver.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.amygo.appserver.AuthoritiesConstants;
import com.amygo.appserver.dto.UserRegisterRequestDTO;
import com.amygo.appserver.security.JwtTokenUtil;
import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.security.UserDetailService;
import com.amygo.appserver.util.UserUtil;
import com.amygo.common.BaseResult;
import com.amygo.persis.domain.User;
import com.amygo.persis.domain.UserToken;
import com.amygo.persis.repository.UserJpaRepository;

@Service
public class UserService {
	@Value("${jwt.tokenHead}")
	private String tokenHead;
	@Autowired
	private UserJpaRepository userRepository;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
    private UserDetailService userDetailsService;
	@Autowired
    private JwtTokenUtil jwtTokenUtil;
	@Autowired
    private UserTokenService userTokenService;
	@Transactional(propagation=Propagation.REQUIRED)
	public BaseResult saveUser(UserRegisterRequestDTO dto) {
		BaseResult br = new BaseResult();
		Integer telephoneUsedFlag = userRepository.getUserbyTelephone(dto.getTelephone());
		if(telephoneUsedFlag!=null) {
			br.setResultCode(BaseResult.WARN);
			br.setMessageCode("W001");
			br.setMessage("手机号被占用");
			return br;
		}
		
		User user = new User();
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setTelephone(dto.getTelephone());
		user.setPassword(encoder.encode(dto.getPassword()));
		user.setBakword(dto.getPassword());
		Date date = new Date();
		user.setLastPasswordResetDate(date);
		user.setNickname(dto.getNickname());
		user.setGuid(UUID.randomUUID().toString());
		user.setCreate_time(date);
		user.setAuthority(AuthoritiesConstants.ADMIN);
//		try {
			userRepository.save(user);
			Map<String,String> map = this.login(dto.getTelephone(), dto.getPassword());
			br.setData(map);
			br.setResultCode(BaseResult.SUCCESS);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return BaseResult.DEFAULT_FAILURE;
//		}
		
		return br;
	}
	
	public String getUserByTelephone(String username,String password) {
		UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
		final Authentication authentication = authenticationManager.authenticate(upToken);
        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
		return token;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
    public Map<String,String> login(String username, String password) {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = authenticationManager.authenticate(upToken);
        JwtUser jwtUser = (JwtUser)authentication.getPrincipal();

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        UserToken userToken = userTokenService.getUserToken(jwtUser.getId()).orElse(new UserToken());
        userToken.setUserId(jwtUser.getId());
        userToken.setToken(token);
        userToken.setUsername(jwtUser.getUsername());
        Calendar calendar = Calendar.getInstance();
        userToken.setCreateTime(calendar.getTime());
        calendar.add(Calendar.DATE, 7);
        userToken.setIsExpired(UserToken.NOT_EXPIRED);
        userToken.setExpireTime(calendar.getTime());
        userTokenService.saveUserToken(userToken);
        Map<String,String> map = new HashMap<String,String>();
        map.put("telephone", username);
        map.put("token", token);
        map.put("nickname", jwtUser.getNickname());
        return map;
    }
    
    public BaseResult logout() {
    	JwtUser jwtUser = UserUtil.getCurrentUser();
    	userTokenService.updateUserToken(jwtUser.getId());
        return BaseResult.DEFAULT_SUCCESS;
    }

    public Map<String,String> refresh(String oldToken,UserToken userToken) {
    	Map<String,String> map = new HashMap<String,String>();
        final String token = oldToken.substring(tokenHead.length());
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser jwtUser = (JwtUser) userDetailsService.loadUserByUsername(username);
        if (jwtTokenUtil.canTokenBeRefreshed(token, jwtUser.getLastPasswordResetDate())){
            userToken.setUserId(jwtUser.getId());
            userToken.setToken(token);
            userToken.setUsername(jwtUser.getUsername());
            Calendar calendar = Calendar.getInstance();
            userToken.setCreateTime(calendar.getTime());
            calendar.add(Calendar.DATE, 7);
            userToken.setIsExpired(UserToken.NOT_EXPIRED);
            userToken.setExpireTime(calendar.getTime());
            userTokenService.saveUserToken(userToken);
        	map.put("telephone", username);
        	map.put("token", jwtTokenUtil.refreshToken(token));
        	map.put("nickname", jwtUser.getNickname());
            return map;
        }
        return null;
    }
    
}
