package com.amygo.appserver.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amygo.appserver.dto.request.CancelOrderRequestDTO;
import com.amygo.appserver.dto.request.EndOrderRequestDTO;
import com.amygo.appserver.dto.request.OrderAddStationDTO;
import com.amygo.appserver.dto.request.OrderModifyDestinationDTO;
import com.amygo.appserver.dto.request.OrderPullOverDTO;
import com.amygo.appserver.dto.request.OrderRequestDTO;
import com.amygo.appserver.dto.request.StartJourneyRequestDTO;
import com.amygo.appserver.dto.request.StartOrderRequestDTO;
import com.amygo.appserver.dto.response.OrderDTO;
import com.amygo.appserver.dto.response.OrderDetailDTO;
import com.amygo.appserver.interfaces.VehicleFeignClient;
import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.util.UserUtil;
import com.amygo.appserver.vo.CarVo;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.common.enums.CarStatusDetailEnum;
import com.amygo.common.enums.CarStatusEnum;
import com.amygo.common.enums.OrderBehaviorEnum;
import com.amygo.common.enums.OrderStatusEnum;
import com.amygo.common.util.CommonUtils;
import com.amygo.common.util.HttpUtil;
import com.amygo.common.util.PathPlanCommandDTO;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.Order;
import com.amygo.persis.domain.OrderBehaviorRecord;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.OrderBehaviorRecordJpaRepository;
import com.amygo.persis.repository.OrderJpaRepository;

@Service
public class OrderService {
	@Autowired
	private OrderJpaRepository orderJpaRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private CarService carService;
	@Autowired
	private CarStatusService carStatusService;
	@Autowired
	private CarJpaRepository carJpaRepository;
	@Autowired
	private OrderBehaviorRecordJpaRepository orderBehaviorRecordJpaRepository;
	@Autowired
	private CommonUtils commonUtils;
	@Value("${vcm.service.url:}")
	private String vcmServiceUrl;
	
	@Value("${vcm.destinationPathPlan.path:}")
	private String vcmDestinationPathPlanPath;
	
	@Autowired
	private VehicleFeignClient vehicleFeignClient;
	
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	/**
	 * 需要加锁，做并发控制
	 * @param orderRequestDTO
	 * @return
	 */
	public BaseResult createOrder(OrderRequestDTO orderRequestDTO) {
		//判断用户有没有未支付订单,若有，跳转到待支付页面
		BaseResult br = new BaseResult();
		JwtUser user = UserUtil.getCurrentUser();
		if(orderJpaRepository.findByUserIdAndOrderStatus(user.getId())!=null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_FINISH);
			br.setMessage("您有未完成订单");
			return br;
		}
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("lat", orderRequestDTO.getStartLat());
		CarVo carVo = carService.findNearestCar(map);//系统选择最近的车辆
		if(carVo==null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.CARS_TOO_FAR);
			br.setMessage("您的周围暂时没有可用车辆,所以暂时不能提供服务");
			return br;
		}
		Order order = new Order();
		modelMapper.map(orderRequestDTO, order);
		order.setCarId(carVo.getCarId());
		order.setCreateTime(new Date());
		order.setGuid(UUID.randomUUID().toString());
		order.setOrderCode(getOrderIdByUUId());// 必须保证在高并发情况下的唯一性
		order.setOrderStatus(OrderStatusEnum.ORDER_CREATE.getStatus());
		order.setStartAddress(orderRequestDTO.getStartAddress());
		order.setEndAddress(orderRequestDTO.getEndAddress());
		order.setUserId(user.getId());
		order.setUsername(user.getUsername());
		
		Car car = carService.findOne(carVo.getCarId());
		
		
		//To Do send start command to car
//		String url = "http://212.64.55.44:8080/api/toStartPoint?ipcId="+car.getIpcId();
//		String result = HttpUtil.clientGet(url);
		
		String url = vcmServiceUrl+vcmDestinationPathPlanPath;
		Map<String,String> paramMap = new HashMap<String,String>();
		//命令格式可能需要改动 To Do
		Long startLon = new Double(orderRequestDTO.getStartLon()*Math.pow(10, 7)).longValue();
		Long startLat = new Double(orderRequestDTO.getStartLat()*Math.pow(10, 7)).longValue();
		Long endLon = new Double(orderRequestDTO.getEndLon()*Math.pow(10, 7)).longValue();
		Long endLat = new Double(orderRequestDTO.getEndLat()*Math.pow(10, 7)).longValue();
		log.info("send command to ipcId:"+car.getIpcId());
//		paramMap.put("ipcId", car.getIpcId());
//		paramMap.put("startLon", startLon);
//		paramMap.put("startLat", startLat);
//		paramMap.put("endLon", endLon);
//		paramMap.put("endLat", endLat);
//		String result = HttpUtil.clientPostJson(url, commonUtils.toJson(paramMap), "utf-8");
//		BaseResult sendCommandResult = commonUtils.fromJson(result, BaseResult.class);
		PathPlanCommandDTO pathPlanCommandDTO = new PathPlanCommandDTO(car.getIpcId(),startLon,startLat,endLon,endLat);
		BaseResult sendCommandResult = vehicleFeignClient.sendPathPlan(pathPlanCommandDTO);
		if(sendCommandResult.getResultCode()!= BaseResult.SUCCESS) {
			log.info("path plan command send error:" + commonUtils.toJson(sendCommandResult));
			return sendCommandResult;
		}else {
			orderJpaRepository.save(order);
			car.setStatus(CarStatusEnum.CAR_IN_USE.getStatus());
			car.setStatusDetail(CarStatusDetailEnum.CAR_ARRIVE_START_POINT.getStatus());//CAR_TO_START_POINT->CAR_ARRIVE_START_POINT
			car.setOrderCode(order.getOrderCode());
			carService.updateCar(car);
			carVo.setOrderCode(order.getOrderCode());
			carVo.setStatusDetail(car.getStatusDetail());
			br.setData(new OrderDTO(carVo,order));
			return br;
		}
	}
	
	public BaseResult cancelOrder(CancelOrderRequestDTO cancelOrderRequestDTO) {
		//判断用户有没有未支付订单,若有，跳转到待支付页面
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(cancelOrderRequestDTO.getOrderCode()).orElse(null);
//		为演示方便，暂且去除这一限制
//		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()==order.getOrderStatus()) {//进行中的订单无法取消
//			br.setResultCode(BaseResult.ERROR);
//			br.setMessageCode(ApiResConstants.ORDER_IN_PROCESS);
//			return br;
//		}
		Car car = carService.findOne(order.getCarId());//判断车辆是否可以预约
		order.setOrderStatus(OrderStatusEnum.ORDER_CANCEL_PAID.getStatus());//目前认为取消后，不需要付款
		car.setStatus(CarStatusEnum.CAR_TO_USE.getStatus());
		car.setOrderCode(null);//取消车辆绑定的订单
		order.setCancelReason(cancelOrderRequestDTO.getCancelReason());
		order.setCancelTime(new Date());
		orderJpaRepository.save(order);
		carService.updateCar(car);
		//To Do send flameout command to car
		return BaseResult.DEFAULT_SUCCESS;
	}
	/**
	 * 解锁并上车
	 * @param startOrderRequestDTO
	 * @return
	 */
	public BaseResult unlockAndGetOn(StartOrderRequestDTO startOrderRequestDTO) {
		//开门,
		// to do:发动开锁指令：需要保证程序结果和实际的执行结果一致
		// 发送路径规划指令到IPC
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(startOrderRequestDTO.getOrderCode()).orElse(null);
		if(order==null||OrderStatusEnum.ORDER_CREATE.getStatus()!= order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_IN_PROCESS);
			return br;
		}
		order.setStartTime(new Date());
		order.setOrderStatus(OrderStatusEnum.ORDER_IN_PROCESS.getStatus());
		orderJpaRepository.save(order);
		Car car = carService.findOne(order.getCarId());
		car.setStatusDetail(CarStatusDetailEnum.CAR_PASSENGER_GET_ON.getStatus());//乘客已上车
		carService.updateCar(car);
		
//		String url = "http://212.64.55.44:8080/api/toEndPoint?ipcId="+car.getIpcId();
//		String result = HttpUtil.clientGet(url);
//		log.info("call vcm api result:"+result);
		
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public BaseResult finishOrder(EndOrderRequestDTO endOrderRequestDTO) {
		//锁门,用车结束，需要结束计费
		// to do:发动锁门指令：需要保证程序结果和实际的执行结果一致,调用tbox api
		BaseResult br = new BaseResult();
		Car car = carJpaRepository.findByIpcId(endOrderRequestDTO.getIpcId()).orElse(null);
		if(car==null) {
			log.error("no car bind to ipc:"+endOrderRequestDTO.getIpcId());
		}
		Order order = orderJpaRepository.findByOrderCode(car.getOrderCode()).orElse(null);
		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_IN_PROCESS);
			return br;
		}
		order.setFinishTime(new Date());
		order.setOrderStatus(OrderStatusEnum.ORDER_TO_PAY.getStatus());
		orderJpaRepository.save(order);
		car.setStatus(CarStatusEnum.CAR_TO_USE.getStatus());
		car.setStatusDetail(null);
		car.setOrderCode(null);
		carService.updateCar(car);//车辆恢复可预约状态
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public Integer findUserUnFinishedOrder(){
		JwtUser user = UserUtil.getCurrentUser();
		return orderJpaRepository.findByUserIdAndOrderStatus(user.getId());
	}
	
	public BaseResult getOrderDetail(String orderCode) {
		
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public BaseResult findOrdersByUserIdAndOrderStatus() {
		BaseResult br = new BaseResult();
		JwtUser user = UserUtil.getCurrentUser();
		Order order = orderJpaRepository.findOrdersByUserIdAndOrderStatus(user.getId()).orElse(null);
		if(order!=null) {
			Car car = carService.findOne(order.getCarId());
			CarStatus carStatus = carStatusService.getCarStatus(order.getCarId());
			CarVo carVo = new CarVo(car.getId(),
									car.getVinCode(),
									car.getPlateNumber(),
									carStatus.getLat(),
									carStatus.getLon(),
									order.getOrderCode(),car.getStatusDetail());
			br.setData(new OrderDTO(carVo,order));
		}
		return br;
	}
	
	public BaseResult getOrderDetail(StartOrderRequestDTO orderRequestDTO) {
		//  判断用户有没有未支付订单,若有，跳转到待支付页面
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(orderRequestDTO.getOrderCode()).orElse(null);
		Car car = carService.findOne(order.getCarId());
		br.setData(new OrderDetailDTO(order,car,null));
		return br;
	}
	public BaseResult getOrderDetailByVinCode(StartJourneyRequestDTO startJourneyRequestDTO) {
		//  判断用户有没有未支付订单,若有，跳转到待支付页面
		BaseResult br = new BaseResult();
		Car car = carService.getCar(startJourneyRequestDTO.getVinCode());
		Order order = orderJpaRepository.findByOrderCode(car.getOrderCode()).orElse(null);
		br.setData(new OrderDetailDTO(order,car,null));
		return br;
	}
	public BaseResult startJourney(StartJourneyRequestDTO startJourneyRequestDTO) {
		// 开门,订单开始，需要开始计费
		BaseResult br = new BaseResult();
		Car car = carService.getCar(startJourneyRequestDTO.getVinCode());//判断车辆是否可以预约
		car.setStatusDetail(CarStatusDetailEnum.CAR_START_TO_END.getStatus());
		carService.updateCar(car);
		Order order = orderJpaRepository.findByOrderCode(car.getOrderCode()).orElse(null);
		if(order==null||OrderStatusEnum.ORDER_CREATE.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_IN_PROCESS);
			return br;
		}
		order.setStartTime(new Date());
// 		订单开始计费，根据车辆上报的经纬度计算,计费由VCM服务执行
		order.setOrderStatus(OrderStatusEnum.ORDER_IN_PROCESS.getStatus());
		orderJpaRepository.save(order);
		
		

		
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public BaseResult modifyDestination(OrderModifyDestinationDTO modifyDestinationDTO) {
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(modifyDestinationDTO.getOrderCode()).orElse(null);
		//判断是否是运行中的订单
		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_IN_PROCESS);
			br.setMessageCode("非进行中的订单无法更改目的地");
			return br;
		}
		
// 		to do:发动更改目的地指令：发送终点到车辆，需要保证程序结果和实际的执行结果一致
//		String url = "http://212.64.55.44:8080/api/toEndPoint?ipcId="+car.getIpcId();
//		String result = HttpUtil.clientGet(url);
//		log.info("call vcm api result:"+result);
//		指令下发成功 才能更改目的地
//		记录订单更改目的地的行为		
		OrderBehaviorRecord orderBehaviorRecord = new OrderBehaviorRecord();
		orderBehaviorRecord.setOrderCode(modifyDestinationDTO.getOrderCode());
		orderBehaviorRecord.setGuid(UUID.randomUUID().toString());
		orderBehaviorRecord.setCreateTime(new Date());
		orderBehaviorRecord.setBehavior(OrderBehaviorEnum.ORDER_MODIFY_DESTINATION.getName());
		orderBehaviorRecord.setBehaviorDetailJson(commonUtils.toJson(modifyDestinationDTO));
		orderBehaviorRecordJpaRepository.save(orderBehaviorRecord);
//		订单修改目的地
		order.setEndAddress(modifyDestinationDTO.getDestinationAddress());
		order.setEndLat(modifyDestinationDTO.getLat());
		order.setEndLon(modifyDestinationDTO.getLon());
		orderJpaRepository.save(order);
		return br;
	}
	
	public BaseResult addStation(OrderAddStationDTO orderAddStationDTO) {
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(orderAddStationDTO.getOrderCode()).orElse(null);
		//判断是否是运行中的订单
		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_IN_PROCESS);
			br.setMessageCode("非进行中的订单无法增加途经站点");
			return br;
		}
		
// 		to do:发动增加途经点指令：发送增加的途经点和终点经纬度到车辆，需要保证程序结果和实际的执行结果一致，车辆是否要自动停车？
//		String url = "http://212.64.55.44:8080/api/pathPlan?ipcId="+car.getIpcId();
//		String result = HttpUtil.clientGet(url);
//		log.info("call vcm api result:"+result);
//		指令下发成功 才能更改目的地
//		记录订单更改目的地的行为		
		OrderBehaviorRecord orderBehaviorRecord = new OrderBehaviorRecord();
		orderBehaviorRecord.setOrderCode(orderAddStationDTO.getOrderCode());
		orderBehaviorRecord.setGuid(UUID.randomUUID().toString());
		orderBehaviorRecord.setCreateTime(new Date());
		orderBehaviorRecord.setBehavior(OrderBehaviorEnum.ORDER_ADD_STATION.getName());
		orderBehaviorRecord.setBehaviorDetailJson(commonUtils.toJson(orderAddStationDTO));
		orderBehaviorRecordJpaRepository.save(orderBehaviorRecord);
		return br;
	}
	
	public BaseResult pullOver(OrderPullOverDTO orderPullOverDTO) {
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(orderPullOverDTO.getOrderCode()).orElse(null);
		//判断是否是运行中的订单
		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_IN_PROCESS);
			br.setMessageCode("非进行中的订单无法靠边停车");
			return br;
		}
		OrderBehaviorRecord orderBehaviorRecord = new OrderBehaviorRecord();
		orderBehaviorRecord.setOrderCode(orderPullOverDTO.getOrderCode());
		orderBehaviorRecord.setGuid(UUID.randomUUID().toString());
		orderBehaviorRecord.setCreateTime(new Date());
		orderBehaviorRecord.setBehavior(OrderBehaviorEnum.ORDER_PULL_OVER.getName());
		orderBehaviorRecord.setBehaviorDetailJson(commonUtils.toJson(orderPullOverDTO));
		orderBehaviorRecordJpaRepository.save(orderBehaviorRecord);
		return br;
	}
	
	public BaseResult resume(OrderPullOverDTO orderPullOverDTO) {
		BaseResult br = new BaseResult();
		Order order = orderJpaRepository.findByOrderCode(orderPullOverDTO.getOrderCode()).orElse(null);
		//判断是否是运行中的订单
		if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_IN_PROCESS);
			br.setMessageCode("非进行中的订单无法重新启动");
			return br;
		}
		OrderBehaviorRecord orderBehaviorRecord = new OrderBehaviorRecord();
		orderBehaviorRecord.setOrderCode(orderPullOverDTO.getOrderCode());
		orderBehaviorRecord.setGuid(UUID.randomUUID().toString());
		orderBehaviorRecord.setCreateTime(new Date());
		orderBehaviorRecord.setBehavior(OrderBehaviorEnum.ORDER_RESUME.getName());
		orderBehaviorRecord.setBehaviorDetailJson(commonUtils.toJson(orderPullOverDTO));
		orderBehaviorRecordJpaRepository.save(orderBehaviorRecord);
		return br;
	}
	
	public static String getOrderIdByUUId() {
		 int machineId = 1;//最大支持1-9个集群机器部署
		 int hashCodeV = UUID.randomUUID().toString().hashCode();
		 if(hashCodeV < 0){//有可能是负数
			 hashCodeV = - hashCodeV;
		 }
		 return machineId+ String.format("%015d", hashCodeV);
	}
}
