package com.amygo.appserver.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.amygo.common.BaseResult;
import com.amygo.persis.domain.UserToken;
import com.amygo.persis.repository.UserTokenJpaRepository;

@Service
public class UserTokenService {
	@Autowired
	private UserTokenJpaRepository userTokenJpaRepository;
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public BaseResult saveUserToken(UserToken userToken) {
		userTokenJpaRepository.save(userToken);
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public BaseResult updateUserToken(Long userId) {
		UserToken userToken = userTokenJpaRepository.findByUserId(userId).orElse(null);
		if (userToken!=null) {
			userToken.setIsExpired(UserToken.EXPIRED);
			userTokenJpaRepository.save(userToken);
		}
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	public Optional<UserToken> getUserToken(Long userId) {
		return userTokenJpaRepository.findByUserId(userId);
	}
}
