package com.amygo.appserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.SmsRecord;
import com.amygo.persis.repository.SmsRecordJpaRepository;

@Service
public class SmsRecordService {
	@Autowired
	private SmsRecordJpaRepository smsRecordJpaRepository;
	
	public void saveSmsRecord(SmsRecord smsRecord) {
		smsRecordJpaRepository.save(smsRecord);
	}

}
