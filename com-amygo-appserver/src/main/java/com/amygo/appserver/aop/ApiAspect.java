package com.amygo.appserver.aop;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.service.ApiLogService;
import com.amygo.common.util.CommonUtils;
import com.amygo.persis.domain.ApiLog;

import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

@Configuration
@Aspect
public class ApiAspect {

	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());

	private final String pointCut = "execution(* com.amygo.appserver.rest..*.*(..))";

	@Autowired
	private ApiLogService apiLogService;

	@Autowired
	private CommonUtils commonUtils;

	@Pointcut(pointCut)
	public void restPointCut() {
	}

	@Around("restPointCut()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes == null ? null : attributes.getRequest();

		long beginTime = System.currentTimeMillis();
		Object result = null;
		try {
			result = joinPoint.proceed();
		} catch (Throwable e) {
			throw e;
		} finally {
			try {
				long endTime = System.currentTimeMillis();
				String userGuid = "guest";
				if (request != null) {
					Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
					if (request.getHeader("Authorization") != null) {
						JwtUser jwtUser = (JwtUser) authentication.getPrincipal();
						userGuid = jwtUser.getGuid();
					}
					ApiLog apiLog = new ApiLog();
					apiLog.setUrl(request.getRequestURL().toString());
					apiLog.setHttpMethod(request.getMethod());
					apiLog.setClassMethod(joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
					apiLog.setServerTime(beginTime);
					apiLog.setIpAddress(getRemoteHost(request));
					apiLog.setUserAgent(request.getHeader("User-Agent"));
					apiLog.setUserGuid(userGuid);
					apiLog.setDuration(endTime - beginTime);
					apiLog.setServerType("com-amygo-appserver");
					apiLog.setServerName("com-amygo-appserver");
					apiLog.setUserGuid(userGuid);
					String requestParamStr = request.getMethod().equals(RequestMethod.GET.toString())?request.getQueryString():getRequestParams(joinPoint);
					apiLog.setRequestParamStr(requestParamStr.length()>5000?requestParamStr.substring(0,5000):requestParamStr);
					apiLog.setResult(result == null ? null : commonUtils.toJson(result));
					apiLogService.saveApiLog(apiLog);
					log.info(apiLog.toString());
				}
			} catch (Throwable e) {
				e.printStackTrace();
				log.error("HttpRequestAspect Error:" + e.getMessage());
			}
		}
		return result;
	}

	/**
	 * 获取目标主机的ip
	 * 
	 * @param request
	 * @return
	 */
	private String getRemoteHost(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
	}

	
	/**
	 * @Description 请求参数和值
	 * @Author JianBiaoHuang
	 * @date 2018年11月08日
	 * @return Map<String,Object>
	 */
	public String getRequestParams(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] args = joinPoint.getArgs();
		String classType = joinPoint.getTarget().getClass().getName();
		Class<?> clazz = Class.forName(classType);
		String clazzName = clazz.getName();
		String methodName = joinPoint.getSignature().getName(); // 获取方法名称
		// 获取参数名称和值
		Map<String, Object> nameAndArgs = this.getFieldsName(this.getClass(), clazzName, methodName, args);
		// nameAndArgs的两种类型，用实体类接收的类似这样：
		// reqParams=com.ynet.finmall.innermanage.event.SaveOrUpdateRoleReqParam@616b9c0e
		// 用Map<String,Object>接收的是这样：menuNo=56473283，遍历这个map区分两种不同，使用不同的取值方式。
		// 根据获取到的值所属的不同类型通过两种不同的方法获取参数
		boolean flag = false;
		if (nameAndArgs != null && nameAndArgs.size() > 0) {
			for (Map.Entry<String, Object> entry : nameAndArgs.entrySet()) {
				if (entry.getValue() instanceof String) {
					flag = true;
					break; // 跳出循环
				}
			}
		}
		StringBuffer sb = new StringBuffer();
		if (flag) {
			// 从Map中获取
			sb.append(commonUtils.toJson(nameAndArgs));
		} else {
			if (args != null) {
				for (Object object : args) {
					if (object != null) {
						if (object instanceof MultipartFile || object instanceof ServletRequest
								|| object instanceof ServletResponse) {
							continue;
						}
						sb.append(commonUtils.toJson(object));
					}
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * @Description 获取字段名和字段值
	 * @Author JianBiaoHuang
	 * @date 2018年11月08日
	 * @return Map<String,Object>
	 */
	private Map<String, Object> getFieldsName(Class cls, String clazzName, String methodName, Object[] args)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		ClassPool pool = ClassPool.getDefault();
		ClassClassPath classPath = new ClassClassPath(cls);
		pool.insertClassPath(classPath);

		CtClass cc = pool.get(clazzName);
		CtMethod cm = cc.getDeclaredMethod(methodName);
		MethodInfo methodInfo = cm.getMethodInfo();
		CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
		LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
		if (attr == null) {
			// exception
		}
		int pos = Modifier.isStatic(cm.getModifiers()) ? 0 : 1;
		for (int i = 0; i < cm.getParameterTypes().length; i++) {
			map.put(attr.variableName(i + pos), args[i]);// paramNames即参数名
		}
		return map;
	}
}
