package com.amygo.appserver.interfaces;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amygo.common.BaseResult;
import com.amygo.common.util.PathPlanCommandDTO;

@FeignClient(name="com-amygo-vehicle")
public interface VehicleFeignClient {
	
	@RequestMapping(value="/api/sendPathPlanCommand",method = RequestMethod.POST)
	public BaseResult sendPathPlan(@RequestBody PathPlanCommandDTO dto);

}
