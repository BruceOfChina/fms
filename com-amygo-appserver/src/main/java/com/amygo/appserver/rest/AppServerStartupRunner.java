package com.amygo.appserver.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import com.amygo.persis.repository.CarStatusRecordJpaRepository;

//@Component
public class AppServerStartupRunner implements CommandLineRunner{
	
	@Autowired
	private CarStatusRecordJpaRepository carStatusRecordJpaRepository;
	
	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("start command line runner.................");
//		new Timer().schedule(new CarStatusTimerTask(carStatusRecordJpaRepository), new Date(),1000);
	}
}
