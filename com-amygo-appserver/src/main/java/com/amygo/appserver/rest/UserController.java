package com.amygo.appserver.rest;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.UserRegisterRequestDTO;
import com.amygo.appserver.dto.request.LoginByVerifyCodeRequestDTO;
import com.amygo.appserver.security.JwtAuthenticationRequest;
import com.amygo.appserver.security.JwtTokenUtil;
import com.amygo.appserver.security.JwtUser;
import com.amygo.appserver.service.SmsRecordService;
import com.amygo.appserver.service.UserService;
import com.amygo.appserver.util.ValidateUtils;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.exception.CustomException;
import com.amygo.persis.domain.SmsRecord;
import com.amygo.persis.domain.User;
import com.amygo.persis.domain.UserToken;
import com.amygo.persis.repository.UserJpaRepository;
import com.amygo.persis.repository.UserTokenJpaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "UserController", description = "用户接口：登录 注册 获取验证码 登出 刷新token ")
@RestController
@RequestMapping("/")
public class UserController {
	
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Value("${register.verify.message:}")
	private String registerVerifyMessageFormat;
	
	@Value("${jwt.header}")
	private String tokenHeader;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserJpaRepository userJpaRepository;
	
	@Autowired
	private SmsRecordService smsRecordService;
	
	@Autowired
	private UserTokenJpaRepository userTokenJpaRepository;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Value("${sms.cheat.code:}")
	private int smsCheatCode;
	
	@ApiOperation(value="获取登录验证码", notes="该接口用来发送用户登录的验证码，需要判断手机号是否是注册用户，否则，拒绝发送验证码")
    @RequestMapping(value = "/auth/getLoginVerifyCode",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult getLoginVerifyCode(HttpServletRequest request,@RequestParam String telephone){
		User user = userJpaRepository.findByTelephone(telephone).orElse(null);
		BaseResult br = new BaseResult();
		if(user==null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.USER_NOT_FOUND);
			return br;
		}else {
			HttpSession session = request.getSession();
			int verifyCode = (int)((Math.random()*9+1)*1000);
			session.setAttribute("verifyCode"+telephone, verifyCode);
			session.setMaxInactiveInterval(60*5);//验证码有效期5分钟
			String registerVerifyMessage = MessageFormat.format(registerVerifyMessageFormat, verifyCode);
			log.info(telephone+registerVerifyMessage);
			SmsRecord smsRecord = new SmsRecord();
			smsRecord.setContent(registerVerifyMessage);
			smsRecord.setSendTime(new Date());
			smsRecord.setTelephone(telephone);
			smsRecord.setSystemName("com-amygo-appserver");
			try {
				smsRecordService.saveSmsRecord(smsRecord);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return BaseResult.DEFAULT_SUCCESS;
		}
    }
	
	@ApiOperation(value="用户登录1", notes="用户根据短信验证码登录接口")
    @RequestMapping(value = "/auth/loginByVerifyCode",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult loginByVerifyCode(HttpServletRequest request,@RequestBody LoginByVerifyCodeRequestDTO dto){
		User user = userJpaRepository.findByTelephone(dto.getTelephone()).orElse(null);
		BaseResult br = new BaseResult();
		if(user==null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.USER_NOT_FOUND);
			return br;
		}
		HttpSession session = request.getSession();
		Integer verifyCodeFrom = (Integer) session.getAttribute("verifyCode"+dto.getTelephone());
		if(verifyCodeFrom==null&&dto.getVerifyCode()!=smsCheatCode){
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_EXPIRE);
		}else if((verifyCodeFrom!=null&&verifyCodeFrom.intValue()!=dto.getVerifyCode())&&dto.getVerifyCode()!=smsCheatCode) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_ERROR);
		}else if((verifyCodeFrom!=null&&verifyCodeFrom.intValue()==dto.getVerifyCode())||dto.getVerifyCode()==smsCheatCode){
			Map<String,String> map= userService.login(user.getTelephone(), user.getBakword());
			 // Return the token
			br.setData(map);
			br.setResultCode(BaseResult.SUCCESS);
		}
		return br;
		
    }
	
	@ApiOperation(value="用户登录2", notes="用户根据密码或令牌登录接口，token字段不为空，则根据token登录")
    @RequestMapping(value = "/auth/loginByPassword",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult loginByPassword(HttpServletRequest request,@RequestBody JwtAuthenticationRequest dto){
		BaseResult br = new BaseResult();
		Map<String,String> map = new HashMap<String,String>();
		String token = dto.getToken();
		if(StringUtils.isNotBlank(token)) {//根据token登录,返回新的token
			UserToken userToken = userTokenJpaRepository.findByToken(token.substring(7)).orElse(null);
			if(userToken==null||(userToken!=null&&(userToken.getIsExpired()==1||userToken.getExpireTime().compareTo(new Date())!=1))) {//token过期
	        	br.setResultCode(BaseResult.ERROR);
	        	br.setMessageCode(ApiResConstants.TOKEN_INVALID);
			}else {//token未过期
				br.setResultCode(BaseResult.SUCCESS);
				map.put("telephone", dto.getTelephone());
		        map.put("token", token.substring(7));
		        String username = jwtTokenUtil.getUsernameFromToken(token.substring(7));
		        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		        JwtUser jwtUser = (JwtUser)userDetails;
		        map.put("nickname", jwtUser.getNickname());
			}
		}else {
			map = userService.login(dto.getTelephone(), dto.getPassword());//根据用户名、密码登录
			 // Return the token
			br.setResultCode(BaseResult.SUCCESS);
		}
		br.setData(map);
		return br;
    }
	
	@ApiOperation(value="验证手机号注册还是登录", notes="输入手机号,若手机号未注册，则发送注册验证码,此乃注册第一步;若手机号已注册,则跳转到输入密码进行登录页面")
    @RequestMapping(value = "/auth/checkTelephoneOrSendVerifyCode",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult getCode(HttpServletRequest request,@RequestParam(name="telephone",required=true) String telephone){
		if(!ValidateUtils.isMobile(telephone)) {
			throw new CustomException("请输入合法的手机号");
		}
		int verifyCode = (int)((Math.random()*9+1)*1000);
		HttpSession session = request.getSession();
		if(session.getAttribute("verifyCode"+telephone)!=null){
			throw new CustomException("请不要重复获取验证码");
		}
		BaseResult br = new BaseResult();
		if(userJpaRepository.getUserbyTelephone(telephone)!=null) {//手机号已注册，跳转到输入密码进行登录页面
			br.setResultCode(BaseResult.SUCCESS);
			br.setMessageCode("W0001");
			br.setMessage("手机号已注册");
		}else {
			//call api to send sms
			session.setAttribute("verifyCode"+telephone, verifyCode);
			session.setMaxInactiveInterval(60*5);//验证码有效期5分钟
			String registerVerifyMessage = MessageFormat.format(registerVerifyMessageFormat, verifyCode);
			log.info(telephone+registerVerifyMessage);
			br.setResultCode(BaseResult.SUCCESS);
			br.setMessageCode("W0002");
			br.setMessage("手机号未注册");
			SmsRecord smsRecord = new SmsRecord();
			smsRecord.setContent(registerVerifyMessage);
			smsRecord.setSendTime(new Date());
			smsRecord.setTelephone(telephone);
			smsRecord.setSystemName("com-amygo-appserver");
			try {
				smsRecordService.saveSmsRecord(smsRecord);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return br;
		
    }
	
	@ApiOperation(value="验证注册验证码", notes="验证注册验证码接口，注册第二步")
    @RequestMapping(value = "/auth/checkCode",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult checkCode(HttpServletRequest request,@RequestParam(name="verifyCode",required=true) Integer verifyCode,
    		@RequestParam(name="telephone",required=true) String telephone){
		HttpSession session = request.getSession();
		Integer verifyCodeFrom = (Integer) session.getAttribute("verifyCode"+telephone);
		BaseResult br = new BaseResult();
		if(verifyCodeFrom==null&&verifyCode.intValue()!=smsCheatCode){
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_EXPIRE);
		}else if(verifyCodeFrom!=null&&verifyCode.intValue()!=verifyCodeFrom.intValue()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_ERROR);
		}
		return br;
    }
	
	@ApiOperation(value="输入用户昵称", notes="用户输入昵称接口，注册第三步")
    @RequestMapping(value = "/auth/enterNickname",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult enterNickname(HttpServletRequest request,@RequestParam(name="nickname",required=true) String nickname){
		return BaseResult.DEFAULT_SUCCESS;
    }
	
	@ApiOperation(value="输入登录密码并注册", notes="用户输入登录密码接口，注册第四步,此步完成，用户注册成功")
    @RequestMapping(value = "/auth/register",method = {RequestMethod.POST})
    @ResponseBody
    public BaseResult register(HttpServletRequest request,@RequestBody UserRegisterRequestDTO dto){
		HttpSession session = request.getSession();
		BaseResult br = new BaseResult();
		Integer verifyCode = (Integer) session.getAttribute("verifyCode"+dto.getTelephone());
		if((verifyCode!=null&&verifyCode.intValue()==dto.getVerifyCode())||dto.getVerifyCode().intValue()==smsCheatCode){
			br = userService.saveUser(dto);
		}else if(verifyCode==null&&dto.getVerifyCode().intValue()!=smsCheatCode){
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_EXPIRE);
		}else if(verifyCode!=null&&verifyCode!=dto.getVerifyCode()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.VERIFY_CODE_ERROR);
		}
		return br;
    }
	
	@ApiOperation(value="用户选择默认支付方式", notes="此步可选，用户可跳过")
    @RequestMapping(value = "/api/setPayMethod",method = {RequestMethod.POST})
    @ResponseBody
    public BaseResult setPayMethod(HttpServletRequest request,@RequestParam String payMethod){
		
		return BaseResult.DEFAULT_SUCCESS;
    }
	
	@ApiOperation(value="用户登出", notes="用户登出接口")
    @RequestMapping(value = "/api/logOut",method = {RequestMethod.POST})
    @ResponseBody
    public BaseResult logOut(HttpServletRequest request){
		return userService.logout();
    }
	
	@ApiOperation(value="刷新token", notes="原有token过期，可调用此接口重新生成")
    @RequestMapping(value = "/api/refresh", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult refreshAndGetAuthenticationToken(
            HttpServletRequest request) throws AuthenticationException{
        String token = request.getHeader(tokenHeader);
//        Map<String,String> map = userService.refresh(token);
        BaseResult br = new BaseResult();
//        if(map == null) {
//        	br.setResultCode(BaseResult.ERROR);
//        	br.setMessageCode(ApiResConstants.TOKEN_INVALID);
//        } else {
//        	br.setResultCode(BaseResult.SUCCESS);
//        	br.setData(map);
//        }
        return br;
    }

}
