package com.amygo.appserver.rest;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.request.RemoteControlRequestDTO;
import com.amygo.common.BaseResult;
import com.amygo.common.util.CommonUtils;
import com.amygo.common.util.HttpUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "TestController", description="测试接口")
@RestController
@RequestMapping("/app")
public class TestController {
	
	private Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CommonUtils commonUtils;
	
	@Value("${car.tbox.url:}")
	private String carTboxUrl;
	
	@Value("${remote.control.path:}")
	private String remoteControlPath;
	
	@ApiOperation(value="远程控制测试接口", notes="该接口仅用于测试")
    @RequestMapping(value = "/testRemoteControl",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult testRemoteControl(HttpServletRequest request,@RequestBody RemoteControlRequestDTO dto){
		String url = carTboxUrl+MessageFormat.format(remoteControlPath, dto.getVid());
		String result = HttpUtil.clientPostJson(url, commonUtils.toJson(dto), "utf-8");
		log.info("call tbox remote control api result:"+result);
		BaseResult sendCommandResult = commonUtils.fromJson(result, BaseResult.class);
		if(sendCommandResult.getResultCode()!=BaseResult.SUCCESS) {
			log.info("path plan command send error:"+result);
		}
		return BaseResult.DEFAULT_SUCCESS;
    }
	
	@ApiOperation(value="获得车辆当前位置", notes="用于APP端实时刷新车辆当前位置 ")
    @RequestMapping(value = "/test",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult getCarLocation(HttpServletRequest request){
		String url = "http://212.64.55.44:8080/api/sendPathPlanCommand";
		Map<String,String> paramMap = new HashMap<String,String>();
		String startLon = String.valueOf(121);
		String startLat = String.valueOf(31);
		String endLon = String.valueOf(121);
		String endLat = String.valueOf(31);
		paramMap.put("ipcId", "10000000000000001");
		paramMap.put("startLon", startLon);
		paramMap.put("startLat", startLat);
		paramMap.put("endLon", endLon);
		paramMap.put("endLat", endLat);
		String result = HttpUtil.clientPostJson(url, commonUtils.toJson(paramMap), "utf-8");
		BaseResult sendCommandResult = commonUtils.fromJson(result, BaseResult.class);
		if(sendCommandResult.getResultCode()!=BaseResult.SUCCESS) {
			log.info("path plan command send error:"+result);
		}
		log.info("call vcm api result:"+result);
		return BaseResult.DEFAULT_SUCCESS;
    }
}
