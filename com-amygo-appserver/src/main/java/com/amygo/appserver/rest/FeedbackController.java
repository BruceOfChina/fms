package com.amygo.appserver.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.request.FeedbackRequestDTO;
import com.amygo.appserver.service.FeedbackService;
import com.amygo.common.BaseResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "FeedbackController", description="反馈接口")
@RestController
@RequestMapping("/api")
public class FeedbackController {
	@Autowired
	private FeedbackService feedbackService;
	
	@ApiOperation(value="添加反馈", notes="添加反馈")
    @RequestMapping(value = "/addFeedback",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult addFeedback(HttpServletRequest request,@RequestBody FeedbackRequestDTO dto){
		feedbackService.save(dto);
		return BaseResult.DEFAULT_SUCCESS;
    }
}
