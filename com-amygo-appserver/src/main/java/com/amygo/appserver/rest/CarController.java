package com.amygo.appserver.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.response.CarLocationDTO;
import com.amygo.common.BaseResult;
import com.amygo.common.util.CacheUtil;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.CarStatusJpaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "OrderController", description="车辆接口")
@RestController
@RequestMapping("/api")
public class CarController {
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	@Autowired
	private CarJpaRepository carJpaRepository;
	
	private static long lat = 31296050L;
	private static long lon = 121162380L;
	@ApiOperation(value="获得车辆当前位置", notes="用于APP端实时刷新车辆当前位置 {\r\n" + 
			"  \"resultCode\": 0,\r\n" + 
			"  \"data\": {\r\n" + 
			"    \"lat\": 31,\r\n" + 
			"    \"lon\": 121\r\n" + 
			"  }\r\n" + 
			"}")
    @RequestMapping(value = "/getCarLocation",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult getCarLocation(HttpServletRequest request,@RequestParam String vinCode){
		Object obj = CacheUtil.get("vehicle_location", vinCode);
		BaseResult br = new BaseResult();
		Car car = carJpaRepository.findByVinCode(vinCode).orElse(null);
		CarStatus carStatus = carStatusJpaRepository.findCarStatusByVinCode(vinCode).orElse(null);
		if(car == null) {
			return null;
		}
		if(obj==null) {
			lat=lat+1;
			lon=lon+1;
			br.setData(new CarLocationDTO(carStatus.getLat(),carStatus.getLon(),car.getStatusDetail(),car.getOrderCode()));
		}else {
//			CacheUtil.remove("vehicle_location", vinCode);
			CarLocationDTO carLocationDTO = (CarLocationDTO)obj;
			br.setData(carLocationDTO);
		}
		return br;
    }
	
	@ApiOperation(value="更新车辆当前位置", notes="用于测试")
    @RequestMapping(value = "/updateCarLocation",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult updateCarLocation(HttpServletRequest request,
    		@RequestParam String vinCode,
    		@RequestParam Double lon,
    		@RequestParam Double lat,
    		@RequestParam Integer statusDetail){
		Car car = carJpaRepository.findByVinCode(vinCode).orElse(null);
		CacheUtil.put("vehicle_location", vinCode, new CarLocationDTO(lat,lon,statusDetail,car.getOrderCode()));
		return BaseResult.DEFAULT_SUCCESS;
    }
}
