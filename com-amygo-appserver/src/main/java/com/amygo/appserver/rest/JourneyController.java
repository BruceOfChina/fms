package com.amygo.appserver.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.request.OrderAddStationDTO;
import com.amygo.appserver.dto.request.OrderModifyDestinationDTO;
import com.amygo.appserver.dto.request.OrderPullOverDTO;
import com.amygo.appserver.dto.request.StartJourneyRequestDTO;
import com.amygo.appserver.service.OrderService;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.common.util.PointPolygonUtil;
import com.amygo.persis.domain.SystemVariable;
import com.amygo.persis.repository.SystemVariableJpaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "JourneyController", description="用户行程接口，为PAD提供")
@RestController
@RequestMapping("/api")
public class JourneyController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private SystemVariableJpaRepository systemConfigJpaRepository;
	
	@ApiOperation(value="行程开始", notes="用户点击PAD,开始行程，车辆启动,订单开始计费")
    @RequestMapping(value = "/startJourney",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult startJourney(HttpServletRequest request,@RequestBody StartJourneyRequestDTO startJourneyRequestDTO){
		return orderService.startJourney(startJourneyRequestDTO);
    }
	
	@ApiOperation(value="订单详情", notes="显示订单详细信息	" +
			"   ORDER_CREATE(\"已预约订单\", 1), \r\n" + 
			"	ORDER_IN_PROCESS(\"进行中订单\", 2), \r\n" + 
			"	ORDER_CANCEL(\"已取消订单\", 3), \r\n" + 
			"	ORDER_TO_PAY(\"待支付订单\", 4),\r\n" + 
			"	ORDER_TO_EVALUATE(\"待评价订单\", 5),\r\n" + 
			"	ORDER_FINISHED(\"已完成订单\", 6),\r\n" + 
			"	ORDER_CANCEL_TO_PAY(\"取消后待支付订单\", 7),\r\n" + 
			"	ORDER_CANCEL_PAID(\"取消后已支付订单\", 8),\r\n"+ 
			"	ORDER_WAIT_TO_START(\"到达起点等待启动的订单\", 8);")
    @RequestMapping(value = "/orderDetailByVinCode",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult orderDetailByVinCode(HttpServletRequest request,@RequestBody StartJourneyRequestDTO startJourneyRequestDTO){
		return orderService.getOrderDetailByVinCode(startJourneyRequestDTO);
    }
	
	/**
	 * 更改目的地
	 * @param request
	 * @param modifyDestinationDTO
	 * @return
	 */
	@ApiOperation(value="更改目的地", notes="PAD/APP发起请求，用以更改订单的目的地")
    @RequestMapping(value = "/modifyDestination",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult modifyDestination(HttpServletRequest request,@RequestBody OrderModifyDestinationDTO modifyDestinationDTO){
    	BaseResult br = new BaseResult();
    	//终点是否在可运营范围内
		List<SystemVariable> scopeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_REGION");
		boolean flag = false;
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("lon", modifyDestinationDTO.getLon());
		map.put("lat", modifyDestinationDTO.getLat());
		for(SystemVariable item:scopeList) {
			if (PointPolygonUtil.isInPolygon(map, item.getVariableValue())) {//只要用户属于一个运营范围即可
				flag = true;
				break;
			}
		}
		if(!flag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_SCOPE);
			br.setMessage("由于我们的运营范围还没有包括您的目的地,所以暂时不能提供服务");
			return br;
		}
    	return orderService.modifyDestination(modifyDestinationDTO);
    }
	/**
	 * 增加途经站点   
	 * @param request
	 * @param orderAddStationDTO
	 * @return
	 */
	@ApiOperation(value="增加途经站点", notes="PAD/APP发起请求，用以增加订单途经的站点，由此更改路线增加的费用由用户承担")
    @RequestMapping(value = "/addStation",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult addStation(HttpServletRequest request,@RequestBody OrderAddStationDTO orderAddStationDTO){
    	BaseResult br = new BaseResult();
    	//终点是否在可运营范围内
		List<SystemVariable> scopeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_REGION");
		boolean flag = false;
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("lon", orderAddStationDTO.getLon());
		map.put("lat", orderAddStationDTO.getLat());
		for(SystemVariable item:scopeList) {
			if (PointPolygonUtil.isInPolygon(map, item.getVariableValue())) {//只要用户属于一个运营范围即可
				flag = true;
				break;
			}
		}
		if(!flag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_SCOPE);
			br.setMessage("由于我们的运营范围还没有包括您指定的地址,所以车辆将按照原路线继续行驶");
			return br;
		}
    	return orderService.addStation(orderAddStationDTO);
    }
	
	@ApiOperation(value="靠边停车", notes="PAD发起请求，PAD直接发送靠边停车指令到车辆，车辆寻找可能的停车点，直接停车，订单停止计费")
    @RequestMapping(value = "/pullOver",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult pullOver(HttpServletRequest request,@RequestBody OrderPullOverDTO orderPullOverDTO){
    	return orderService.pullOver(orderPullOverDTO);
    }
	
	@ApiOperation(value="重新启动", notes="PAD发起请求，PAD直接发送启动指令到车辆，车辆开始运行，订单恢复计费")
    @RequestMapping(value = "/resume",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult resume(HttpServletRequest request,@RequestBody OrderPullOverDTO orderPullOverDTO){
    	return orderService.resume(orderPullOverDTO);
    }
}
