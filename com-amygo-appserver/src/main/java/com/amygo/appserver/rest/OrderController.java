package com.amygo.appserver.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.request.CancelOrderRequestDTO;
import com.amygo.appserver.dto.request.EndOrderRequestDTO;
import com.amygo.appserver.dto.request.OrderRequestDTO;
import com.amygo.appserver.dto.request.StartOrderRequestDTO;
import com.amygo.appserver.service.OrderService;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.common.enums.CarStatusEnum;
import com.amygo.common.util.DateUtils;
import com.amygo.common.util.LocationUtil;
import com.amygo.common.util.PointPolygonUtil;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.SystemVariable;
import com.amygo.persis.repository.CarStatusJpaRepository;
import com.amygo.persis.repository.SystemVariableJpaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "OrderController", description="用户订单接口")
@RestController
@RequestMapping("/api")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private SystemVariableJpaRepository systemConfigJpaRepository;
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	
	@ApiOperation(value="出行信息初始化", notes="根据用户当前位置判断是否可以提供服务,用户登录成功后，在初始页面，由APP主动发送请求 10001:不在运营范围 10002：不在运营时间段 10003：附近没有可用车辆 10004：有未完成订单")
    @RequestMapping(value = "/infoInitial",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult infoInitial(HttpServletRequest request,
    		@RequestParam(name="lat",required=true) Double lat,
    		@RequestParam(name="lon",required=true) Double lon){
		BaseResult br = new BaseResult();
		if(orderService.findUserUnFinishedOrder()!=null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.ORDER_NOT_FINISH);
			br.setMessage("您有未完成的订单");
			return br;
		}
		//当前用户在运营范围内,partitionLocation 从运营范围获取
		List<SystemVariable> scopeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_REGION");
		boolean flag = false;
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("lon", lon);
		map.put("lat", lat);
		for(SystemVariable item:scopeList) {
			if (PointPolygonUtil.isInPolygon(map, item.getVariableValue())) {//只要用户属于一个运营范围即可
				flag = true;
				break;
			}
		}
		if(!flag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_SCOPE);
			br.setMessage("由于我们的运营范围还没有包括您所在地区,所以暂时不能提供服务");
			return br;
		}
		//当前时间在运营时间内
		List<SystemVariable> timeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_TIME");
		boolean timeFlag = false;
		for(SystemVariable item : timeList) {
			String[] timeInterval = item.getVariableValue().split("-");
			Date date = new Date();
			String currentDate = DateUtils.getDateByFormat(date, "yyyy-MM-dd");
			Long startTime = DateUtils.getTimeByStr(currentDate + " " + timeInterval[0],"yyyy-MM-dd HH:mm:ss");
			Long endTime = DateUtils.getTimeByStr(currentDate + " " + timeInterval[1],"yyyy-MM-dd HH:mm:ss");
			if (date.getTime()>=startTime&&date.getTime()<=endTime) {//只要用户属于一个运营范围即可
				timeFlag = true;
				break;
			}
		}
		if(!timeFlag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_TIME);
			br.setMessage("由于现在超出运营时间,所以暂时不能提供服务");
			return br;
		}
		//周围是否有可用车辆
		SystemVariable systemVariableOfElectricity = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ELECTRICITY", "AVA_CAR_ELECTRICITY").orElse(null);
		SystemVariable systemVariableOfEnduranceMileage = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ENDURANCE_MILEAGE", "AVA_CAR_ENDURANCE_MILEAGE")
				.orElse(null);
		Double electricity = Double.parseDouble(systemVariableOfElectricity.getVariableValue());
		Long enduranceMileage = Long.parseLong(systemVariableOfEnduranceMileage.getVariableValue());
		List<CarStatus> carList = carStatusJpaRepository.findByStatus(electricity, enduranceMileage,
				CarStatusEnum.CAR_TO_USE.getStatus());
		SystemVariable systemVariable = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("NEAREST_CAR_DISTANCE", "NEAREST_CAR_DISTANCE").orElse(null);
		double nearDistance = systemVariable != null ? Double.parseDouble(systemVariable.getVariableValue()) : 5000.;
		for (Iterator<CarStatus> it = carList.iterator(); it.hasNext();) {
			CarStatus item = it.next();
			Double distance = LocationUtil.getDistance(lat, lon, item.getLat(),item.getLon());
			if (distance >= nearDistance) {
				it.remove();
			}
		}

		if (carList.isEmpty()) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.CARS_TOO_FAR);
			br.setMessage("您的周围暂时没有可用车辆,所以暂时不能提供服务");
			return br;
		} else {
			br.setData(carList);
			return br;
		}
    }
	
	
	@ApiOperation(value="输入目的地点", notes="根据上车地点和目的地点，规划路径")
    @RequestMapping(value = "/selectEndLocation",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult enterEndLocation(HttpServletRequest request,
    		@RequestParam(name="lat",required=true) Double lat,
    		@RequestParam(name="lon",required=true) Double lon){
		BaseResult br = new BaseResult();
		//终点是否在可运营范围内
		List<SystemVariable> scopeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_REGION");
		boolean flag = false;
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("lon", lon);
		map.put("lat", lat);
		for(SystemVariable item:scopeList) {
			if (PointPolygonUtil.isInPolygon(map, item.getVariableValue())) {//只要用户属于一个运营范围即可
				flag = true;
				break;
			}
		}
		if(!flag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_SCOPE);
			br.setMessage("由于我们的运营范围还没有包括您的目的地,所以暂时不能提供服务");
			return br;
		}
		//当前时间在运营时间内
		List<SystemVariable> timeList = systemConfigJpaRepository.findByVariableCategory("OPERATION_TIME");
		boolean timeFlag = false;
		for(SystemVariable item : timeList) {
			String[] timeInterval = item.getVariableValue().split("-");
			Date date = new Date();
			String currentDate = DateUtils.getDateByFormat(date, "yyyy-MM-dd");
			Long startTime = DateUtils.getTimeByStr(currentDate + " " + timeInterval[0],"yyyy-MM-dd HH:mm:ss");
			Long endTime = DateUtils.getTimeByStr(currentDate + " " + timeInterval[1],"yyyy-MM-dd HH:mm:ss");
			if (date.getTime()>=startTime&&date.getTime()<=endTime) {//只要用户属于一个运营范围即可
				timeFlag = true;
				break;
			}
		}
		if(!timeFlag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.NOT_IN_OPERATION_TIME);
			br.setMessage("由于现在超出运营时间,所以暂时不能提供服务");
			return br;
		}
		//周围是否有可用车辆：电量、里程、续航里程
		SystemVariable systemVariableOfElectricity = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ELECTRICITY", "AVA_CAR_ELECTRICITY").orElse(null);
		SystemVariable systemVariableOfEnduranceMileage = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("AVA_CAR_ENDURANCE_MILEAGE", "AVA_CAR_ENDURANCE_MILEAGE")
				.orElse(null);
		Double electricity = Double.parseDouble(systemVariableOfElectricity.getVariableValue());
		Long enduranceMileage = Long.parseLong(systemVariableOfEnduranceMileage.getVariableValue());
		List<CarStatus> carList = carStatusJpaRepository.findByStatus(electricity, enduranceMileage,
				CarStatusEnum.CAR_TO_USE.getStatus());
		SystemVariable systemVariable = systemConfigJpaRepository
				.findByVariableCategoryAndVariableKey("NEAREST_CAR_DISTANCE", "NEAREST_CAR_DISTANCE").orElse(null);
		boolean distanceFlag = false;
		double nearDistance = systemVariable!=null?Double.parseDouble(systemVariable.getVariableValue()):5000.;
		for(CarStatus item : carList) {
			Double distance = LocationUtil.getDistance(lat, lon, item.getLat(), item.getLon());
			if(distance<=nearDistance) {
				distanceFlag = true;
				break;
			}
		}
		if(!distanceFlag) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessageCode(ApiResConstants.CARS_TOO_FAR);
			br.setMessage("您的周围暂时没有可用车辆,所以暂时不能提供服务");
			return br;
		}
		return br;
    }

	@ApiOperation(value="输入起点", notes="推荐可能的上车点")
    @RequestMapping(value = "/selectStartLocation",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult enterStartLocation(HttpServletRequest request,
    		@RequestParam(name="lat",required=true) Double lat,
    		@RequestParam(name="lon",required=true) Double lon){
		//步行距离由前端判断
		SystemVariable recomendBoardPoint = systemConfigJpaRepository.findByVariableCategoryAndVariableKey("RECOMMEND_BOARD_POINT", "RECOMMEND_BOARD_POINT").orElse(null);
		BaseResult br = new BaseResult();
		br.setData(StringUtils.isBlank(recomendBoardPoint.getVariableValue())?null:recomendBoardPoint.getVariableValue().split(","));
		return br;
    }
	/**
	 * 执行到这一步，通常预约是不会失败的，
	 * @param request
	 * @param orderRequestDTO
	 * @return
	 */
	@ApiOperation(value="确认下单", notes="创建订单 {\r\n" + 
			"  \"resultCode\": 0,\r\n" + 
			"  \"data\": {\r\n" + 
			"    \"carId\": 1,\r\n" + 
			"    \"vinCode\": \"VIN12345678910123\",\r\n" + 
			"    \"plateNumber\": \"沪A00001\",\r\n" + 
			"    \"lat\": 117.228117,\r\n" + 
			"    \"lon\": 31.830429\r\n" + 
			"  }\r\n" + 
			"}")
    @RequestMapping(value = "/createOrder",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult createOrder(HttpServletRequest request,@RequestBody OrderRequestDTO orderRequestDTO){
		return orderService.createOrder(orderRequestDTO);
    }
	
	@ApiOperation(value="取消下单", notes="取消下单")
    @RequestMapping(value = "/cancelOrder",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult cancelOrder(HttpServletRequest request,@RequestBody CancelOrderRequestDTO cancelOrderRequestDTO){
		return orderService.cancelOrder(cancelOrderRequestDTO);
    }
	
	@ApiOperation(value="开始上车", notes="用户开锁上车、发送目的地经纬度到IPC，车辆启动")
    @RequestMapping(value = "/unlockAndGetOn",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult startOrder(HttpServletRequest request,@RequestBody StartOrderRequestDTO startOrderRequestDTO){
		return orderService.unlockAndGetOn(startOrderRequestDTO);
    }
	
	@ApiOperation(value="结束订单", notes="用户下车后锁门、订单结束计费,跳转到支付页面,IPC收到车辆到达指令,调用该接口,系统自动结束订单,更新订单和车辆状态")
    @RequestMapping(value = "/finishOrder",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult endOrder(HttpServletRequest request,@RequestBody EndOrderRequestDTO endOrderRequestDTO){
		return orderService.finishOrder(endOrderRequestDTO);
    }
	
	@ApiOperation(value="订单详情", notes="显示订单详细信息	" +
			"   ORDER_CREATE(\"已预约订单\", 1), \r\n" + 
			"	ORDER_IN_PROCESS(\"进行中订单\", 2), \r\n" + 
			"	ORDER_CANCEL(\"已取消订单\", 3), \r\n" + 
			"	ORDER_TO_PAY(\"待支付订单\", 4),\r\n" + 
			"	ORDER_TO_EVALUATE(\"待评价订单\", 5),\r\n" + 
			"	ORDER_FINISHED(\"已完成订单\", 6),\r\n" + 
			"	ORDER_CANCEL_TO_PAY(\"取消后待支付订单\", 7),\r\n" + 
			"	ORDER_CANCEL_PAID(\"取消后已支付订单\", 8);")
    @RequestMapping(value = "/orderDetail",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult orderDetail(HttpServletRequest request,@RequestBody StartOrderRequestDTO startOrderRequestDTO){
		return orderService.getOrderDetail(startOrderRequestDTO);
    }
	
	@ApiOperation(value="用户未完成订单", notes="显示用户未完成订单")
    @RequestMapping(value = "/getUnFinishedOrders",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult findOrdersByUserIdAndOrderStatus(HttpServletRequest request){
		return orderService.findOrdersByUserIdAndOrderStatus();
    }
	

}
