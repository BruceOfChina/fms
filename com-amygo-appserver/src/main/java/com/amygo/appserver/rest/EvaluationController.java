package com.amygo.appserver.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.appserver.dto.request.EvaluationRequestDTO;
import com.amygo.appserver.service.EvaluationService;
import com.amygo.common.BaseResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "EvaluationController", description="评价接口")
@RestController
@RequestMapping("/api")
public class EvaluationController {
	@Autowired
	private EvaluationService evaluationService;
	
	@ApiOperation(value="添加评价", notes="添加评价")
    @RequestMapping(value = "/addEvaluation",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResult addEvaluation(HttpServletRequest request,@RequestBody EvaluationRequestDTO dto){
		evaluationService.save(dto);
		return BaseResult.DEFAULT_SUCCESS;
    }
}
