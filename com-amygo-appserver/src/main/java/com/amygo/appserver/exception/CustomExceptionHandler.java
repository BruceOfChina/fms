package com.amygo.appserver.exception;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.amygo.appserver.service.ExceptionLogService;
import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.exception.CustomException;
import com.amygo.persis.domain.ExceptionLog;

@ControllerAdvice 
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
	@Autowired
	private ExceptionLogService businessExceptionService;
	/**
	 *  拦截Exception类的异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String,Object> exceptionHandler(Exception e){
    	//创建一个统一的异常响应实体
        Map<String,Object> result = new HashMap<String,Object>();
        if(e instanceof CustomException) {
        	CustomException customException = (CustomException)e;
        	result.put("messageCode", customException.getErrorCode());
        	result.put("message", e.getMessage());
        }if(e instanceof BadCredentialsException) {
        	result.put("messageCode", ApiResConstants.USERNAME_PASSWORD_WRONG);
        	result.put("resultCode", BaseResult.ERROR);
        	result.put("message", "用户名或密码错误");
        }else {
        	result.put("messageCode", "9999");
        	result.put("message", e.getMessage());
        }
        
        
        ExceptionLog exceptionLog = 
        		new ExceptionLog(
        				UUID.randomUUID().toString(),
        				e.getClass().getName(),
        				e.getStackTrace().toString(),
        				e.getMessage(),
        				new Date());
        e.printStackTrace();
        businessExceptionService.saveExceptionLog(exceptionLog);//保存异常记录
        return result; 
    }
}
