package com.amygo.appserver;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.amygo.common.UtilConfig;
import com.amygo.common.base.BaseApplication;
@EnableDiscoveryClient
@Import({ UtilConfig.class})
@ComponentScan("com.amygo")
@SpringBootApplication(exclude = {
		SecurityAutoConfiguration.class
})
@EnableFeignClients
@EnableTransactionManagement
@EnableScheduling
public class AppserverApplication extends BaseApplication
{
	private static final Logger log = LoggerFactory.getLogger(AppserverApplication.class);
    public static void main( String[] args ) throws UnknownHostException
    {
    	SpringApplication app = new SpringApplication(AppserverApplication.class);

		SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);

		BaseApplication.start(app, source, log, args);
    }
}
