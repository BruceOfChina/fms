package com.amygo.appserver.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CarVo {
	@JsonIgnore
	private Long carId;
	
	private String vinCode;
	
	private String plateNumber;
	
	private Double lat;
	
	private Double lon;
	
	private String orderCode;
	
	private Integer statusDetail;
	
	private String vehicleModel;

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Integer getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}
	
	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public CarVo(Long carId,String vinCode, String plateNumber, Double lat, Double lon, String orderCode, Integer statusDetail,String vehicleModel) {
		super();
		this.carId = carId;
		this.vinCode = vinCode;
		this.plateNumber = plateNumber;
		this.lat = lat;
		this.lon = lon;
		this.orderCode = orderCode;
		this.statusDetail = statusDetail;
		this.vehicleModel = vehicleModel;
	}
	
	public CarVo(Long carId,String vinCode, String plateNumber, Double lat, Double lon, String orderCode, Integer statusDetail) {
		super();
		this.carId = carId;
		this.vinCode = vinCode;
		this.plateNumber = plateNumber;
		this.lat = lat;
		this.lon = lon;
		this.orderCode = orderCode;
		this.statusDetail = statusDetail;
	}
}
