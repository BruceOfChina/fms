package com.amygo.appserver;

public class AuthoritiesConstants {
    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	public static final  String REMEBERME_KEY = "security.rememberme.key";
	
    private AuthoritiesConstants() {
    }
}
