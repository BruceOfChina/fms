package com.amygo.appserver;

import java.io.IOException;

import com.amygo.appserver.dto.VehicleResultDTO;
import com.amygo.common.util.HttpUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    public static void main(String[] args) {
    	String url = "http://app.singulato.com/vehicle/center/v1/vehicle/detail/2c92838864da74fd0164ea0369ce0006";
    	String result = HttpUtil.clientGet(url);
    	System.out.println(result);
    	ObjectMapper jsonMapper = new ObjectMapper();
    	try {
			VehicleResultDTO vehicleStatusDTO = jsonMapper.readValue(result, VehicleResultDTO.class);
			System.out.println(jsonMapper.writeValueAsString(vehicleStatusDTO));
			System.out.println(jsonMapper.writeValueAsString(vehicleStatusDTO.getBusinessObj().getVehicleDetail()));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
	}
}
