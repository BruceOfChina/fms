package com.amygo.persis.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.Car;

public interface CarJpaRepository extends JpaRepository<Car, Long>{
	
	public Optional<Car> findByVinCode(String vinCode);
	
	public Optional<Car> findByIpcId(String ipcId);
	
}
