package com.amygo.persis.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.SystemVariable;

public interface SystemVariableJpaRepository extends JpaRepository<SystemVariable, Long>{
	
	@Query(value="select * from system_variable where variable_category=?1",nativeQuery=true)
	public List<SystemVariable> findByVariableCategory(String variableCategory);
	
	public Optional<SystemVariable> findByVariableCategoryAndVariableKey(String variableCategory,String variableKey);
}
