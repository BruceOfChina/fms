package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.CarStatusRecord;

public interface CarStatusRecordJpaRepository extends JpaRepository<CarStatusRecord, Long>{
	


}
