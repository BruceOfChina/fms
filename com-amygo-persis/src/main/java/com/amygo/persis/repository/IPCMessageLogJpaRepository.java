package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.IPCMessageLog;

public interface IPCMessageLogJpaRepository extends JpaRepository<IPCMessageLog, Long>{

}
