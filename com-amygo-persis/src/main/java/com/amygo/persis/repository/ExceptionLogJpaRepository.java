package com.amygo.persis.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.ExceptionLog;

public interface ExceptionLogJpaRepository extends JpaRepository<ExceptionLog, Long>{
	
	public Optional<ExceptionLog> findByGuid(String guid);

}
