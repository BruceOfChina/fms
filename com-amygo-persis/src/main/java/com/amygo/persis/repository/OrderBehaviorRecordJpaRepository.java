package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.OrderBehaviorRecord;

public interface OrderBehaviorRecordJpaRepository extends JpaRepository<OrderBehaviorRecord, Long>{
	
}
