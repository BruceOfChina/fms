package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.SystemVariableCategory;

public interface SystemVariableCategoryJpaRepository extends JpaRepository<SystemVariableCategory, Long>{
	
	


}
