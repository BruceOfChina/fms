package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.Feedback;

public interface FeedbackJpaRepository extends JpaRepository<Feedback, Long>{

}
