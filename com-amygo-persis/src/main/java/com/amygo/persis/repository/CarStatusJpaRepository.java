package com.amygo.persis.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.vo.CarStatusVo;

public interface CarStatusJpaRepository extends JpaRepository<CarStatus, Long>{
	
	@Query(value="select * from car_status where lat>0 and lon>0 and remain_electricity>?1 and endurance_mileage>?2 and vin_code in(select vin_code from car where car.status=?3) ",nativeQuery=true)
	public List<CarStatus> findByStatus(double remainElectricity,Long enduranceMileage,Integer status);
	
	@Query(value="SELECT car_status.*," + 
			" (SQRT(POWER(MOD(ABS(lon - ?1),360),2) + POWER(ABS(lat - ?2),2))*80*1000) AS distance " + 
			" FROM car_status where remain_electricity>?3 and endurance_mileage>?4 and vin_code in(select vin_code from car where car.status=1)" + 
			" ORDER BY distance LIMIT ?5 ",nativeQuery=true)
	public List<CarStatus> findNearestCar(Double lon,Double lat,double remainElectricity,Long enduranceMileage,Integer count);


	public Optional<CarStatus> findCarStatusByVinCode(String vinCode);
	
	
	public Optional<CarStatus> findCarStatusByIpcId(String ipcId);
	
}
