package com.amygo.persis.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.User;

public interface UserJpaRepository extends JpaRepository<User, Long>{
	
	@Query(value="select 1 from user where telephone=?1",nativeQuery=true)
	public Integer getUserbyTelephone(String telephone);
	
	public Optional<User> findByTelephone(String telephone);

}
