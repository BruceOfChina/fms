package com.amygo.persis.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.UserToken;

public interface UserTokenJpaRepository extends JpaRepository<UserToken, Long>{
	@Query(value="select * from user_token where user_id=?1 and is_expired=0",nativeQuery=true)
	public Optional<UserToken> findByUserId(Long userId);
	
	public Optional<UserToken> findByToken(String token);
}
