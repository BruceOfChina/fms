package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.SmsRecord;

public interface SmsRecordJpaRepository extends JpaRepository<SmsRecord, Long>{

}
