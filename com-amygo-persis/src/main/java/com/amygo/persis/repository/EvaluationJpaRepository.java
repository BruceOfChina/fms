package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.Evaluation;

public interface EvaluationJpaRepository extends JpaRepository<Evaluation, Long>{

}
