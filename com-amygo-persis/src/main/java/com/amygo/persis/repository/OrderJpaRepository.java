package com.amygo.persis.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amygo.persis.domain.Order;

public interface OrderJpaRepository extends JpaRepository<Order, Long>{
	
	
	public Optional<Order> findByOrderCode(String orderCode);
	
	@Query(value="select 1 from orders where user_id=? and order_status in(1,2,4,7)",nativeQuery=true)
	public Integer findByUserIdAndOrderStatus(Long userId);
	
	
	@Query(value="select * from orders where user_id=? and order_status in(1,2,4,7) limit 1",nativeQuery=true)
	public Optional<Order> findOrdersByUserIdAndOrderStatus(Long userId);
	
	
	@Query(value="select * from orders where order_code = (select order_code from car where vin_code=?1);",nativeQuery=true)
	public Optional<Order> findOrdersByVinCode(String vinCode);

}
