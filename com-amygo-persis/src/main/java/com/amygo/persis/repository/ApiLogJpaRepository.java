package com.amygo.persis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amygo.persis.domain.ApiLog;

public interface ApiLogJpaRepository extends JpaRepository<ApiLog, Long>{

}
