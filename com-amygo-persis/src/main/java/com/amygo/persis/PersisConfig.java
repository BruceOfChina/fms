package com.amygo.persis;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.amygo.persis.repository")
@EntityScan("com.amygo.persis.domain")  
public class PersisConfig {

}
