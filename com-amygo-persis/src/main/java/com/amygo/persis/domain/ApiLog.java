package com.amygo.persis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "api_log")
public class ApiLog {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "url", length = 500)
	private String url;

	@Column(name = "http_method", length = 10)
	private String httpMethod;

	@Column(name = "class_method", length = 200)
	private String classMethod;

	@Column(name = "server_time")
	private Long serverTime;

	@Column(name = "ip_address", length = 50)
	private String ipAddress;

	@Column(name = "user_agent", length = 500)
	private String userAgent;

	@Column(name = "user_guid", length = 40)
	private String userGuid;

	@Column(name = "duration")
	private Long duration;

	@Column(name = "server_type", length = 20)
	private String serverType;

	@Column(name = "server_name", length = 50)
	private String serverName;
	
	@Column(name = "request_param_str", length = 500)
	private String requestParamStr;
	
	@Column(name = "result", length = 500)
	private String result;

	public ApiLog() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getHttpMethod() {
		return this.httpMethod;
	}

	public void setClassMethod(String classMethod) {
		this.classMethod = classMethod;
	}

	public String getClassMethod() {
		return this.classMethod;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	public Long getServerTime() {
		return this.serverTime;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserGuid(String userGuid) {
		this.userGuid = userGuid;
	}

	public String getUserGuid() {
		return this.userGuid;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getDuration() {
		return this.duration;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	public String getServerType() {
		return this.serverType;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerName() {
		return this.serverName;
	}

	public String getRequestParamStr() {
		return requestParamStr;
	}

	public void setRequestParamStr(String requestParamStr) {
		this.requestParamStr = requestParamStr;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ApiLog [id=" + id + ", url=" + url + ", httpMethod=" + httpMethod + ", classMethod=" + classMethod
				+ ", serverTime=" + serverTime + ", ipAddress=" + ipAddress + ", userAgent=" + userAgent + ", userGuid="
				+ userGuid + ", duration=" + duration + ", serverType=" + serverType + ", serverName=" + serverName
				+ ", requestParamStr=" + requestParamStr + ", result=" + result + "]";
	}
}
