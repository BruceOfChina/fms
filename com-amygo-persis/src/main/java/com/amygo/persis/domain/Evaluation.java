package com.amygo.persis.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "evaluation")
public class Evaluation {
	
	public static final Integer REPLIED = 1;

	public static final Integer NOT_REPLIED = 0;

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "is_satisfied")
	private Integer isSatisfied;

	@Column(name = "evaluation_score")
	private Integer evaluationScore;

	@Column(name = "evaluation_content")
	private String evaluationContent;

	@Column(name = "reply_content")
	private String replyContent;

	@Column(name = "evaluation_time")
	private Date evaluationTime;

	@Column(name = "reply_time")
	private Date replyTime;

	@Column(name = "telephone")
	private String telephone;

	@Column(name = "is_replied")
	private Integer isReplied;

	public Evaluation() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public Date getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getIsReplied() {
		return isReplied;
	}

	public void setIsReplied(Integer isReplied) {
		this.isReplied = isReplied;
	}

	public Integer getIsSatisfied() {
		return isSatisfied;
	}

	public void setIsSatisfied(Integer isSatisfied) {
		this.isSatisfied = isSatisfied;
	}

	public Integer getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(Integer evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

	public String getEvaluationContent() {
		return evaluationContent;
	}

	public void setEvaluationContent(String evaluationContent) {
		this.evaluationContent = evaluationContent;
	}

	public Date getEvaluationTime() {
		return evaluationTime;
	}

	public void setEvaluationTime(Date evaluationTime) {
		this.evaluationTime = evaluationTime;
	}
}
