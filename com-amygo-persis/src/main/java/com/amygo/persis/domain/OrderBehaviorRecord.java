package com.amygo.persis.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_behavior_record")
public class OrderBehaviorRecord {
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotNull
	@Column(name = "order_code")
	private String orderCode;
	@JsonIgnore
	@Column(name = "guid")
	private String guid;
	
	@Column(name = "create_time")
	private Date createTime;
	
	@Column(name = "behavior")
	private String behavior;
	
	@Column(name = "behavior_detail_json")
	private String behaviorDetailJson;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBehavior() {
		return behavior;
	}

	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}

	public String getBehaviorDetailJson() {
		return behaviorDetailJson;
	}

	public void setBehaviorDetailJson(String behaviorDetailJson) {
		this.behaviorDetailJson = behaviorDetailJson;
	}
}
