package com.amygo.persis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "car_status_record")
public class CarStatusRecord {

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "car_id")
	private Long carId;
	/**
	 * 车辆VIN码
	 */
	@Column(name = "vin_code")
	private String vinCode;
	
	@JsonIgnore
	@Column(name = "guid")
	private String guid;
	/**
	 * 纬度
	 */
	@NotNull
	@Column(name = "lat")
	private Double latitude;
	/**
	 * 经度
	 */
	@Column(name = "lon")
	private Double longitude;
	/**
	 * 高德纬度
	 */
	@Column(name = "gcj_lat")
	private Double gcjLat;
	/**
	 * 高德经度
	 */
	@Column(name = "gcj_lon")
	private Double gcjLon;
	
	@Column(name = "last_poweron_time")
	private Long lastPowerOnTime;//上次上电时间
	
	@Column(name = "power_model")
	private String powerModel;//电源模式
	
	@Column(name = "gear")
	private String gear;//档位
	
	@Column(name = "falut_model")
	private String faultModel;//故障模式
	
	@Column(name = "mileage")
	private Double mileage;//里程
	
	@Column(name = "speed")
	private Double speed;//车辆行驶速度
	
	@Column(name = "high_voltage")
	private boolean highVoltage;//是否高压
	
	@Column(name = "charge")
	private boolean charge;//是否充电
	
	@Column(name = "direction")
	private String direction;//朝向
	
	@Column(name = "gps_physical_addr")
	private Double gpsPhysicalAddr;//GPS的地址
	
	@Column(name = "soc")
	private Double soc;//剩余电量
	
	@Column(name = "online_rate")
	private Double onlineRate;//在线率
	
	@Column(name = "working_time_online_rate")
	private Double workingTimeOnlineRate;//工作时间在线率？
	
	@Column(name = "speed_rate")
	private Double speedRate;//有车速率
	
	@Column(name = "fault_amount")
	private Integer faultAmount;//故障数量
	
	@Column(name = "charging_time")
	private Integer chargingTime;//充电时间
	
	@Column(name = "travel_time")
	private Long travelTime;//行驶时间
	
	@Column(name = "total_online_time")
	private Long totalOnlineTime;//总在线时间
	
	@Column(name = "total_charging_time")
	private Long totalChargingTime;//总充电时间
	
	@Column(name = "server_time")
	private Long serverTime;
	
	@Column(name = "device_time")
	private Long deviceTime;
	
	@Column(name = "order_code")
	private String orderCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Long getLastPowerOnTime() {
		return lastPowerOnTime;
	}

	public void setLastPowerOnTime(Long lastPowerOnTime) {
		this.lastPowerOnTime = lastPowerOnTime;
	}

	public String getPowerModel() {
		return powerModel;
	}

	public void setPowerModel(String powerModel) {
		this.powerModel = powerModel;
	}

	public String getGear() {
		return gear;
	}

	public void setGear(String gear) {
		this.gear = gear;
	}

	public String getFaultModel() {
		return faultModel;
	}

	public void setFaultModel(String faultModel) {
		this.faultModel = faultModel;
	}

	public Double getMileage() {
		return mileage;
	}

	public void setMileage(Double mileage) {
		this.mileage = mileage;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public boolean isHighVoltage() {
		return highVoltage;
	}

	public void setHighVoltage(boolean highVoltage) {
		this.highVoltage = highVoltage;
	}

	public boolean isCharge() {
		return charge;
	}

	public void setCharge(boolean charge) {
		this.charge = charge;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Double getGpsPhysicalAddr() {
		return gpsPhysicalAddr;
	}

	public void setGpsPhysicalAddr(Double gpsPhysicalAddr) {
		this.gpsPhysicalAddr = gpsPhysicalAddr;
	}

	public Double getSoc() {
		return soc;
	}

	public void setSoc(Double soc) {
		this.soc = soc;
	}

	public Double getOnlineRate() {
		return onlineRate;
	}

	public void setOnlineRate(Double onlineRate) {
		this.onlineRate = onlineRate;
	}

	public Double getWorkingTimeOnlineRate() {
		return workingTimeOnlineRate;
	}

	public void setWorkingTimeOnlineRate(Double workingTimeOnlineRate) {
		this.workingTimeOnlineRate = workingTimeOnlineRate;
	}

	public Double getSpeedRate() {
		return speedRate;
	}

	public void setSpeedRate(Double speedRate) {
		this.speedRate = speedRate;
	}

	public Integer getFaultAmount() {
		return faultAmount;
	}

	public void setFaultAmount(Integer faultAmount) {
		this.faultAmount = faultAmount;
	}

	public Integer getChargingTime() {
		return chargingTime;
	}

	public void setChargingTime(Integer chargingTime) {
		this.chargingTime = chargingTime;
	}

	public Long getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(Long travelTime) {
		this.travelTime = travelTime;
	}

	public Long getTotalOnlineTime() {
		return totalOnlineTime;
	}

	public void setTotalOnlineTime(Long totalOnlineTime) {
		this.totalOnlineTime = totalOnlineTime;
	}

	public Long getTotalChargingTime() {
		return totalChargingTime;
	}

	public void setTotalChargingTime(Long totalChargingTime) {
		this.totalChargingTime = totalChargingTime;
	}

	public Double getGcjLat() {
		return gcjLat;
	}

	public void setGcjLat(Double gcjLat) {
		this.gcjLat = gcjLat;
	}

	public Double getGcjLon() {
		return gcjLon;
	}

	public void setGcjLon(Double gcjLon) {
		this.gcjLon = gcjLon;
	}

	public Long getServerTime() {
		return serverTime;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	public Long getDeviceTime() {
		return deviceTime;
	}

	public void setDeviceTime(Long deviceTime) {
		this.deviceTime = deviceTime;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
}
