package com.amygo.persis.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ipc_message_log")
public class IPCMessageLog{
    
    @JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	/**
	 * 工控机编号
	 */
	@NotNull
	@Column(name = "ipc_id")
	private String ipcId;
	/**
	 * 消息报文
	 */
	@Column(name = "message")
	private String message;
	/**
	 * 服务器时间
	 */
	@NotNull
	@Column(name = "server_time")
	private Date serverTime;
	/**
	 * 设备时间
	 */
	@Column(name = "device_time")
	private Date deviceTime;
	/**
	 * 消息ID
	 */
	@Column(name = "application_id")
	private String applicationId;
	/**
	 * 消息方向ID
	 */
	@Column(name = "message_id")
	private Integer messageId;
	/**
	 * 消息解析后内容
	 */
	@Column(name = "message_json")
	private String messageJson;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getServerTime() {
		return serverTime;
	}

	public void setServerTime(Date serverTime) {
		this.serverTime = serverTime;
	}

	public Date getDeviceTime() {
		return deviceTime;
	}

	public void setDeviceTime(Date deviceTime) {
		this.deviceTime = deviceTime;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getMessageJson() {
		return messageJson;
	}

	public void setMessageJson(String messageJson) {
		this.messageJson = messageJson;
	}

	public IPCMessageLog(String ipcId, String message, Date serverTime, Date deviceTime, String applicationId,Integer messageId,
			String messageJson) {
		super();
		this.ipcId = ipcId;
		this.message = message;
		this.serverTime = serverTime;
		if(deviceTime!=null) {
			this.deviceTime = deviceTime;
		}
		this.applicationId = applicationId;
		this.messageJson = messageJson;
		this.messageId = messageId;
	}

	public IPCMessageLog() {
		super();
	}
}

