package com.amygo.persis.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "car")
public class Car{
    
    @JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	/**
	 * 车辆VIN码
	 */
	@NotNull
	@Column(name = "vin_code")
	private String vinCode;
	
	@Column(name = "guid")
	private String guid;
	/**
	 * 车身颜色
	 */
	@NotNull
	@Column(name = "color")
	private String color;
	/**
	 * 车牌号码
	 */
	@Column(name = "plate_number")
	private String plateNumber;
	/**
	 * 生产年份
	 */
	@Column(name = "year")
	private Integer year;
	/**
	 * 车辆状态
	 */
	@Column(name = "status")
	private Integer status;
	/**
	 * 状态明细
	 */
	@Column(name = "status_detail")
	private Integer statusDetail;
	/**
	 * 创建时间(上线时间)
	 */
	@Column(name = "create_time")
	private Date create_time;
	/**
	 * T盒编号
	 */
	@JsonIgnore
	@Column(name = "tbox_id")
	private String tboxId;
	/**
	 * 工控机唯一编号
	 */
	@JsonIgnore
	@Column(name = "ipc_id")
	private String ipcId;
	/**
	 * 根秘钥
	 */
	@JsonIgnore
	@Column(name = "root_key",length=16)
	private String rootKey;
	/**
	 * 初始向量
	 */
	@JsonIgnore
	@Column(name = "init_iv",length=16)
	private String initIv;
	/**
	 * 预约单号
	 */
	@Column(name = "order_code")
	private String orderCode;
	/**
	 * 采购日期
	 */
	@Column(name = "purchase_date")
	private String purchaseDate;
	/**
	 * 品牌
	 */
	@Column(name = "brand")
	private String brand;
	/**
	 * 车型
	 */
	@Column(name = "vehicle_model")
	private String vehicleModel;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTboxId() {
		return tboxId;
	}

	public void setTboxId(String tboxId) {
		this.tboxId = tboxId;
	}

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public String getRootKey() {
		return rootKey;
	}

	public void setRootKey(String rootKey) {
		this.rootKey = rootKey;
	}

	public String getInitIv() {
		return initIv;
	}

	public void setInitIv(String initIv) {
		this.initIv = initIv;
	}

	public Integer getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
}

