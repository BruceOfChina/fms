package com.amygo.persis.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Order {
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotNull
	@Column(name = "order_code")
	private String orderCode;
	@JsonIgnore
	@Column(name = "guid")
	private String guid;
	
	@Column(name = "create_time")
	private Date createTime;
	
	@Column(name = "start_time")
	private Date startTime;
	
	@Column(name = "arrive_time")
	private Date arriveTime;
	
	@Column(name = "finish_time")
	private Date finishTime;
	
	@Column(name = "cancel_time")
	private Date cancelTime;
	
	@Column(name = "order_status")
	private Integer orderStatus;
	@JsonIgnore
	@Column(name = "user_id")
	private Long userId;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "start_address")
	private String startAddress;
	
	@Column(name = "end_address")
	private String endAddress;
	
	@Column(name = "start_lat")
	private Double startLat;
	
	@Column(name = "start_lon")
	private Double startLon;
	
	@Column(name = "end_lat")
	private Double endLat;
	
	@Column(name = "end_lon")
	private Double endLon;
	@JsonIgnore
	@Column(name = "car_id")
	private Long carId;
	
	/**
	 * 订单原费用
	 */
	@Column(name = "original_fee")
	private Integer originalFee;
	/**
	 * 订单实际支付费用
	 */
	@Column(name = "actual_fee")
	private BigDecimal actualFee;
	/**
	 * 取消原因
	 */
	@Column(name = "cancel_reason")
	private String cancelReason;
	/**
	 * 订单里程
	 */
	@Column(name = "order_mileage")
	private Integer orderMileage=0;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}

	public Double getStartLat() {
		return startLat;
	}

	public void setStartLat(Double startLat) {
		this.startLat = startLat;
	}

	public Double getStartLon() {
		return startLon;
	}

	public void setStartLon(Double startLon) {
		this.startLon = startLon;
	}

	public Double getEndLat() {
		return endLat;
	}

	public void setEndLat(Double endLat) {
		this.endLat = endLat;
	}

	public Double getEndLon() {
		return endLon;
	}

	public void setEndLon(Double endLon) {
		this.endLon = endLon;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Integer getOriginalFee() {
		return originalFee;
	}

	public void setOriginalFee(Integer originalFee) {
		this.originalFee = originalFee;
	}

	public BigDecimal getActualFee() {
		return actualFee;
	}

	public void setActualFee(BigDecimal actualFee) {
		this.actualFee = actualFee;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Date getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(Date arriveTime) {
		this.arriveTime = arriveTime;
	}

	public Integer getOrderMileage() {
		return orderMileage;
	}

	public void setOrderMileage(Integer orderMileage) {
		this.orderMileage = orderMileage;
	}
}
