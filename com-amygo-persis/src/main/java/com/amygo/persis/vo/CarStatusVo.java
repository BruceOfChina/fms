package com.amygo.persis.vo;

public class CarStatusVo {
	private Long id;
	private String vinCode;
	private String guid;
	private String ipcId;
	private Double lat;
	private Double lon;
	private Double remainElectricity;
	private Long enduranceMileage;
	private Integer lockStatus;
	private Integer doorStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVinCode() {
		return vinCode;
	}
	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getIpcId() {
		return ipcId;
	}
	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public Double getRemainElectricity() {
		return remainElectricity;
	}
	public void setRemainElectricity(Double remainElectricity) {
		this.remainElectricity = remainElectricity;
	}
	public Long getEnduranceMileage() {
		return enduranceMileage;
	}
	public void setEnduranceMileage(Long enduranceMileage) {
		this.enduranceMileage = enduranceMileage;
	}
	public Integer getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
	}
	public Integer getDoorStatus() {
		return doorStatus;
	}
	public void setDoorStatus(Integer doorStatus) {
		this.doorStatus = doorStatus;
	}
}
