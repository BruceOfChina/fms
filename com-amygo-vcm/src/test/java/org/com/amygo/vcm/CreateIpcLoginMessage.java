package org.com.amygo.vcm;

import java.util.LinkedHashMap;
import java.util.Map;

import com.amygo.common.Constants;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.util.DataParseUtil;

public class CreateIpcLoginMessage {
	public static void main(String[] args) {
		String ipcId = "10000000000000001";
		String loginMessage = createIpcLoginMessage(ipcId);
		System.out.println(loginMessage);
	}
	
	public static String createIpcLoginMessage(String ipcId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("iccid", "hgjagljghjhjdkauifj");
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_LOGIN);
		ms.setMessageId(1);
		ms.setApplicationDataLength(19);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
}
