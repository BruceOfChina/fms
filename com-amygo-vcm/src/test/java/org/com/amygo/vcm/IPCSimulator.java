package org.com.amygo.vcm;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.amygo.common.Constants;
import com.amygo.common.util.ExcelUtils;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.TcpClient;
import com.amygo.vcm.util.DataParseUtil;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class IPCSimulator {
	public static void main(String[] args) {
		String ipcId = "10000000000000001";
        File file = new File("D:\\testdata\\gps1.xls");
        List excelList = ExcelUtils.readExcel(file);
        System.out.println("list中的数据打印出来");
        for (int i = 0; i < excelList.size(); i++) {
            List list = (List) excelList.get(i);
            for (int j = 0; j < list.size(); j++) {
                String[] gps = StringUtils.split((String)list.get(j),',');
                Long lat = Math.round(Double.parseDouble(gps[0])*Math.pow(10, 7));
                Long lon = Math.round(Double.parseDouble(gps[1])*Math.pow(10, 7));
                String gpsMessage = createIpcLocationMessage(ipcId,lat,lon);
                
                System.out.println(gpsMessage);
                ByteBuf buffer =   Unpooled.buffer(1000);  
        		buffer.writeBytes(TextUtils.hexStringToByte(gpsMessage));
        		try {
					TcpClient.sendfeezu(buffer);
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
            System.out.println();
            try {
				Thread.currentThread().sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        String ipcDestinationMessage = CreateIpcDestinationMessage.createIpcDestinationOfStartPointMessage(ipcId);
        
        System.out.println(ipcDestinationMessage);
        ByteBuf buffer =   Unpooled.buffer(1000);  
		buffer.writeBytes(TextUtils.hexStringToByte(ipcDestinationMessage));
		try {
			TcpClient.sendfeezu(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static String createIpcLocationMessage(String ipcId,Long lat,Long lon) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("lat", lat);
		map.put("lon", lon);
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_LOCATION);
		ms.setMessageId(1);
		ms.setApplicationDataLength(8);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}

}
