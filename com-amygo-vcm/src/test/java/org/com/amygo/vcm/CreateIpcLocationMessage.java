package org.com.amygo.vcm;

import java.util.LinkedHashMap;
import java.util.Map;

import com.amygo.common.Constants;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.util.DataParseUtil;

public class CreateIpcLocationMessage {
	public static void main(String[] args) {
		String ipcId = "10000000000000001";
		String loginMessage = createIpcLocationMessage(ipcId);
		System.out.println(loginMessage);
	}
	
	public static String createIpcLocationMessage(String ipcId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("lat", 31*Math.pow(10, 7));
		map.put("lon", 121*Math.pow(10, 7));
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_LOCATION);
		ms.setMessageId(1);
		ms.setApplicationDataLength(8);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
}
