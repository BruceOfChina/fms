package org.com.amygo.vcm;

import java.util.LinkedHashMap;
import java.util.Map;

import com.amygo.common.Constants;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.TcpClient;
import com.amygo.vcm.util.DataParseUtil;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class CreateIpcDestinationMessage {
	public static void main(String[] args) {
		String ipcId = "10000000000000001";
		String IpcDestinationOfStartPointMessage = createIpcDestinationOfStartPointMessage(ipcId);
		String IpcDestinationOfEndPointMessage = createIpcDestinationOfStartPointMessage(ipcId);
		System.out.println(IpcDestinationOfStartPointMessage);
		 ByteBuf buffer =   Unpooled.buffer(1000);  
 		buffer.writeBytes(TextUtils.hexStringToByte(IpcDestinationOfEndPointMessage));
 		try {
				TcpClient.sendfeezu(buffer);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public static String createIpcDestinationOfStartPointMessage(String ipcId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		Long lat = Math.round(31*Math.pow(10, 7));
		Long lon = Math.round(121*Math.pow(10, 7));
		map.put("lat", lat);
		map.put("lon", lon);
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_ARRIVE_DESTINATION);
		ms.setMessageId(2);
		ms.setApplicationDataLength(8);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
	
	public static String createIpcDestinationOfEndPointMessage(String ipcId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("lat", 31*Math.pow(10, 7));
		map.put("lon", 121*Math.pow(10, 7));
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_ARRIVE_DESTINATION);
		ms.setMessageId(2);
		ms.setApplicationDataLength(8);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
}
