package com.amygo.vcm.timerTask;

import java.io.File;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import com.amygo.common.util.ExcelUtils;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.TcpClient;
import com.amygo.vcm.TcpClientNotStatic;
import com.amygo.vcm.util.MessageUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class IPCStartTimerTask extends TimerTask{
	
	public IPCStartTimerTask(String ipcId,String filePath) {
		super();
		this.ipcId = ipcId;
		this.filePath = filePath;
	}

	private String ipcId;
	
	private String filePath;
	
	@Override
	public void run() {
		String path = StringUtils.isNotBlank(filePath)?filePath:this.getClass().getResource("/").getPath();
		File file = new File(path+"/gps1.xls");
        List excelList = ExcelUtils.readExcel(file);
        System.out.println("list中的数据打印出来");
        int size = excelList.size();
        for (int i = 0; i < size; i++) {
            List list = (List) excelList.get(i);
            for (int j = 0; j < list.size(); j++) {
                String[] gps = StringUtils.split((String)list.get(j),',');
                Long lat = Math.round(Double.parseDouble(gps[0])*Math.pow(10, 7));
                Long lon = Math.round(Double.parseDouble(gps[1])*Math.pow(10, 7));
                String gpsMessage = MessageUtils.createIpcLocationMessage(ipcId,lat,lon);
                
                System.out.println(gpsMessage);
                ByteBuf buffer =   Unpooled.buffer(1000);  
        		buffer.writeBytes(TextUtils.hexStringToByte(gpsMessage));
        		try {
					TcpClientNotStatic.sendfeezu(buffer);
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
            System.out.println();
            try {
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        String ipcDestinationMessage = MessageUtils.createIpcDestination(ipcId,1);
        
        System.out.println(ipcDestinationMessage);
        ByteBuf buffer =   Unpooled.buffer(1000);  
		buffer.writeBytes(TextUtils.hexStringToByte(ipcDestinationMessage));
		try {
//			TcpClient.sendfeezu(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
