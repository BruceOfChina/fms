package com.amygo.vcm.timerTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import com.amygo.common.util.CoordinateConvertUtil;
import com.amygo.common.util.ExcelUtils;
import com.amygo.common.util.TextUtils;
import com.amygo.persis.domain.CarStatusRecord;
import com.amygo.persis.repository.CarStatusRecordJpaRepository;
import com.amygo.vcm.TcpClientNotStatic;
import com.amygo.vcm.util.MessageUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class IPCStartToEndTimerTask extends TimerTask{
	
	public IPCStartToEndTimerTask(String ipcId,String filePath,CarStatusRecordJpaRepository carStatusRecordJpaRepository) {
		super();
		this.ipcId = ipcId;
		this.filePath = filePath;
		this.carStatusRecordJpaRepository = carStatusRecordJpaRepository;
	}

	private String ipcId;
	
	private String filePath;
	
	private CarStatusRecordJpaRepository carStatusRecordJpaRepository;
	
	@Override
	public void run() {
		String path = StringUtils.isNotBlank(filePath)?filePath:this.getClass().getResource("/").getPath();
		File file = new File(path+"/gps2.xls");
        List excelList = ExcelUtils.readExcel(file);
        System.out.println("list中的数据打印出来");
        List<CarStatusRecord> recordList = new ArrayList<CarStatusRecord>();
        for (int i = 0; i < excelList.size(); i++) {
            List list = (List) excelList.get(i);
            for (int j = 0; j < list.size(); j++) {
                String[] gps = StringUtils.split((String)list.get(j),',');
                CarStatusRecord carStatusRecord = new CarStatusRecord();
                Long lat = Math.round(Double.parseDouble(gps[0])*Math.pow(10, 7));
                Long lon = Math.round(Double.parseDouble(gps[1])*Math.pow(10, 7));
                String gpsMessage = MessageUtils.createIpcLocationMessage(ipcId,lat,lon);
                
                System.out.println(gpsMessage);
                ByteBuf buffer =   Unpooled.buffer(1000);  
        		buffer.writeBytes(TextUtils.hexStringToByte(gpsMessage));
        		try {
					TcpClientNotStatic.sendfeezu(buffer);
				} catch (Exception e) {
					e.printStackTrace();
				}
                carStatusRecord.setLatitude(Double.parseDouble(gps[0]));
                carStatusRecord.setLongitude(Double.parseDouble(gps[1]));
                double[] gcjLatLon = CoordinateConvertUtil.wgs2GCJ(carStatusRecord.getLatitude(), carStatusRecord.getLongitude());
        		carStatusRecord.setGcjLat(gcjLatLon[0]);
        		carStatusRecord.setGcjLon(gcjLatLon[1]);
                carStatusRecord.setOrderCode("1000000035550737");
                carStatusRecord.setCarId(1L);
                recordList.add(carStatusRecord);
                try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
               
            }
            carStatusRecordJpaRepository.save(recordList);
            System.out.println();
//            try {
//				Thread.currentThread().sleep(3000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
        }
        String ipcDestinationMessage = MessageUtils.createIpcDestination(ipcId,2);
        
        System.out.println(ipcDestinationMessage);
        ByteBuf buffer =   Unpooled.buffer(1000);  
		buffer.writeBytes(TextUtils.hexStringToByte(ipcDestinationMessage));
		try {
//			TcpClient.sendfeezu(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
