package com.amygo.vcm.rest;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.common.BaseResult;
import com.amygo.common.Constants;
import com.amygo.common.util.CacheUtil;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.dto.DestinationPlanCommandDTO;
import com.amygo.vcm.dto.PathPlanCommandDTO;
import com.amygo.vcm.dto.VehicleAccelerationDecelerationCommandDTO;
import com.amygo.vcm.dto.VehicleRemoteSetCommandDTO;
import com.amygo.vcm.dto.VehicleStartStopCommandDTO;
import com.amygo.vcm.dto.VehicleSteeringWheelCommandDTO;
import com.amygo.vcm.service.IPCChannelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "IPCRemoteControlController", description = "IPC控制接口：路径规划 ")
@RestController
@RequestMapping("/")
public class IPCRemoteControlController {
	@Autowired
	private IPCChannelService ipcChannelService;
	
	@ApiOperation(value="发送路径规划指令", notes="该接口用来发送路径规划指令到IPC")
    @RequestMapping(value = "/api/sendPathPlanCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendPathPlan(@RequestBody PathPlanCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("startLon", dto.getStartLon());
		map.put("startLat", dto.getStartLat());
		map.put("endLon", dto.getEndLon());
		map.put("endLat", dto.getEndLat());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_PATH_PLAN);
		ms.setMessageId(1);
		ms.setApplicationDataLength(16);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
//		ipcChannelService.sendMessage(ms);
		
//		return BaseResult.DEFAULT_SUCCESS;
		return ipcChannelService.getCommandResponseDTO(ms);
	}
	
	@ApiOperation(value="发送单点规划指令", notes="该接口用来发送单点路径规划指令到IPC")
    @RequestMapping(value = "/api/sendDestinationPlanCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendDestinationPlan(@RequestBody DestinationPlanCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("destinationType", dto.getDestinationLon());
		map.put("destinationLon", dto.getDestinationLon());
		map.put("destinationLat", dto.getDestinationLat());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_DESTINATION_PLAN);
		ms.setMessageId(1);
		ms.setApplicationDataLength(9);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		return ipcChannelService.getCommandResponseDTO(ms);
	}
	
	@ApiOperation(value="发送车辆启停指令", notes="该接口用来发送单点路径规划指令到IPC 1:启动 0：熄火")
    @RequestMapping(value = "/api/sendVehicleStartStopCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendVehicleStartStopCommand(@RequestBody VehicleStartStopCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("commandType", dto.getCommandType());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_VEHICLE_START_STOP);
		ms.setMessageId(1);
		ms.setApplicationDataLength(1);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		return ipcChannelService.getCommandResponseDTO(ms);
	}
	
	@ApiOperation(value="发送车辆转向控制指令", notes="该接口用来发送转向控制指令到IPC 1:左转 0：右转")
    @RequestMapping(value = "/api/sendVehicleSteeringWheelCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendVehicleSteeringWheelCommand(@RequestBody VehicleSteeringWheelCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("controlDirection", dto.getControlDirection());
		map.put("angelValue", dto.getAngelValue());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_VEHICLE_STEERING_WHEEL);
		ms.setMessageId(1);
		ms.setApplicationDataLength(3);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		return ipcChannelService.getCommandResponseDTO(ms);
	}
	
	@ApiOperation(value="加减速控制指令", notes="该接口用来发送加减速控制指令到IPC 1:加速 0：减速")
    @RequestMapping(value = "/api/sendVehicleAccelerationDecelerationCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendAccelerationDecelerationCommand(@RequestBody VehicleAccelerationDecelerationCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("accelerationOrDeceleration", dto.getAccelerationOrDeceleration());
		map.put("commandValue", dto.getCommandValue());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_VEHICLE_ACCELERATION_DECELERATION);
		ms.setMessageId(1);
		ms.setApplicationDataLength(2);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		return ipcChannelService.getCommandResponseDTO(ms);
	}
	
	@ApiOperation(value="远程设置指令", notes="该接口用来发送远程设置指令")
    @RequestMapping(value = "/api/sendRemoteSetCommand",method = {RequestMethod.POST},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult sendRemoteSetCommand(@RequestBody VehicleRemoteSetCommandDTO dto) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("paramType", dto.getParamType());
		map.put("paramValue", dto.getParamValue());
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(dto.getIpcId());
		ms.setApplicationId(ApplicationConstants.IPC_VEHICLE_REMOTE_SET);
		ms.setMessageId(1);
		ms.setApplicationDataLength(2);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		return ipcChannelService.getCommandResponseDTO(ms);
	}
}
