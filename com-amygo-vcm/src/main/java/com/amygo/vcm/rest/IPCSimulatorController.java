package com.amygo.vcm.rest;

import java.util.Timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amygo.common.BaseResult;
import com.amygo.persis.repository.CarStatusRecordJpaRepository;
import com.amygo.vcm.timerTask.IPCStartTimerTask;
import com.amygo.vcm.timerTask.IPCStartToEndTimerTask;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "IPCSimulatorController", description = "IPC模拟接口：模拟位置上报和到站提醒")
@RestController
@RequestMapping("/api")
public class IPCSimulatorController {
	@Value("${file.path:}")
	private String filePath;
	@Autowired
	private CarStatusRecordJpaRepository carStatusRecordJpaRepository;
	
	@ApiOperation(value="预约后，车辆从当前位置行驶到上车点", notes="定期发送gps，到达目的地后，发送到站提醒")
    @RequestMapping(value = "/toStartPoint",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult toStartPoint(@RequestParam String ipcId) {
		new Timer().schedule(new IPCStartTimerTask(ipcId,filePath), 3000);
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	@ApiOperation(value="用户上车后，车辆从上车点行驶到终点", notes="定期发送gps，到达目的地后，发送到站提醒")
    @RequestMapping(value = "/toEndPoint",method = {RequestMethod.GET},produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public BaseResult toEndPoint(@RequestParam String ipcId) {
		new Timer().schedule(new IPCStartToEndTimerTask(ipcId,filePath,carStatusRecordJpaRepository), 3000);
		return BaseResult.DEFAULT_SUCCESS;
	}
	
	
	

}
