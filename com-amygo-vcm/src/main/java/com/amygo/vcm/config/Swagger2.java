package com.amygo.vcm.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2 {
	@Bean
	public Docket createRestApi() {
		// 添加head参数start
		ParameterBuilder tokenPar = new ParameterBuilder();        
		List<Parameter> pars = new ArrayList<Parameter>();      	
		tokenPar.name("Authorization")
		.description("令牌")
		.modelRef(new ModelRef("string"))
		.parameterType("header").required(false).build();//header中的ticket参数非必填，传空也可以    	
		pars.add(tokenPar.build());    //根据每个方法名也知道当前方法在设置什么参数


		// 添加head参数end
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				// 为当前包路径
				.apis(RequestHandlerSelectors.basePackage("com.amygo.vcm.rest")).paths(PathSelectors.any())
				.build().globalOperationParameters(pars);
	}

	// 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				// 页面标题
				.title("Amygo-VCM 接口文档说明")
				// 创建人
				.contact(new Contact("BruceHuang", "http://www.baidu.com", ""))
				// 版本号
				.version("1.0")
				// 描述
				.description("API 描述").build();
	}
}
