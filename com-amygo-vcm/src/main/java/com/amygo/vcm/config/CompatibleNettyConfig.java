package com.amygo.vcm.config;

import java.util.concurrent.ThreadFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.amygo.vcm.netty.NettyServerDecodeHandler;
import com.amygo.vcm.netty.NettyServerEncodeHandler;
import com.amygo.vcm.netty.NettyServerHandler;
import com.amygo.vcm.netty.NettyServerInitializer;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

@Configuration
public class CompatibleNettyConfig {

	@Value("${longConnection.useSSL:false}")
	private boolean useSSL;

	@Value("${longConnection.workerThreads:200}")
	private int workerThreads = 200;

	@Bean
	@Scope(value = "prototype")
	NettyServerDecodeHandler nettyServerDecodeHandler() {
		return new NettyServerDecodeHandler();

	}

	@Bean
	@Scope(value = "prototype")
	NettyServerEncodeHandler nettyServerEncodeHandler() {
		return new NettyServerEncodeHandler();

	}
	
	@Bean
	@Scope(value = "prototype")
	NettyServerHandler nettyServerHandler() {
		return new NettyServerHandler();

	}

	@Bean
	public NettyServerInitializer netServerInitializer() {
		return new NettyServerInitializer() {

			@Override
			protected NettyServerDecodeHandler createNettyServerDecodeHandler() {
				return nettyServerDecodeHandler();
			}

			@Override
			protected NettyServerEncodeHandler createNettyServerEncodeHandler() {
				return nettyServerEncodeHandler();
			}

			@Override
			protected NettyServerHandler createNettyServerHandler() {
				return nettyServerHandler();
			}
		};
	}

	@Bean
	public ServerBootstrap serverBootstrap() throws Exception {
		ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("Netty-compatible-Server-%d")
				.setDaemon(true).build();
		EventLoopGroup bossGroup = new NioEventLoopGroup(1, threadFactory);
		ThreadFactory threadWorkerFactory = new ThreadFactoryBuilder()
				.setNameFormat("Netty-compatible-Server-worker-%d").setDaemon(true).build();
		EventLoopGroup workerGroup = new NioEventLoopGroup(workerThreads, threadWorkerFactory);

		ServerBootstrap serverBoot = new ServerBootstrap();
		serverBoot.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
				.option(ChannelOption.SO_BACKLOG, 128)
				.option(ChannelOption.TCP_NODELAY, true)
				.childOption(ChannelOption.SO_KEEPALIVE, true)
				.handler(new LoggingHandler(LogLevel.INFO)).childHandler(netServerInitializer());

		return serverBoot;

	}
}
