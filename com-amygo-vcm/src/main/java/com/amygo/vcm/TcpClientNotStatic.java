package com.amygo.vcm;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amygo.common.util.TextUtils;
import com.amygo.vcm.netty.NettyServerDecodeHandler;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class TcpClientNotStatic extends Thread{
	private static Logger logger = LoggerFactory.getLogger(TcpClientNotStatic.class);

	public static String HOST = "212.64.55.44";//
//	public static String HOST = "127.0.0.1";
    public static int PORT = 9001;//test nginx port
    public static Bootstrap bootstrap = getBootstrap();
    public static Channel channel = getChannel(HOST, PORT);
    private static ChannelFutureListener channelFutureListener;

    /**
     * 初始化Bootstrap
     * 
     * @return
     */
    public static final Bootstrap getBootstrap() {
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
//        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.option(ChannelOption.TCP_NODELAY, true);
//        b.option(ChannelOption.SO_TIMEOUT, 50000);
        b.group(group).channel(NioSocketChannel.class);
        b.handler(new ChannelInitializer<Channel>() {
            @Override
            protected void initChannel(Channel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
//                pipeline.addLast("frameDecoder",new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0,4, 0, 4));
//                pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
//                pipeline.addLast("decoder",new StringDecoder(CharsetUtil.UTF_8));
//                pipeline.addLast("encoder",new StringEncoder(CharsetUtil.UTF_8));
               
        		pipeline.addLast(new DelimiterBasedFrameDecoder(NettyServerDecodeHandler.maxPackageLength, Unpooled.copiedBuffer(new byte[] {NettyServerDecodeHandler.startValue})));
        		pipeline.addLast(new TcpClientHandler());
            }
        });
        
        channelFutureListener= new ChannelFutureListener() {
            public void operationComplete(ChannelFuture f) throws Exception {
                //  Log.d(Config.TAG, "isDone:" + f.isDone() + "     isSuccess:" + f.isSuccess() +
                //          "     cause" + f.cause() + "        isCancelled" + f.isCancelled());

                if (f.isSuccess()) {
                    System.out.println("重新连接服务器成功");

                } else {
                	System.out.println( "重新连接服务器失败");
                    //  3秒后重新连接
                    f.channel().eventLoop().schedule(new Runnable() {
                        @Override
                        public void run() {
                            doConnect();
                        }
                    }, 3, TimeUnit.SECONDS);
                }
            }
        };
        b.register().addListener(channelFutureListener);
        return b;
    }

    
//  连接到服务端
    public static void doConnect() {
    	System.out.println( "doConnect");
        ChannelFuture future = null;
        try {
            future = bootstrap.connect(new InetSocketAddress(HOST, PORT));
            future.addListener(channelFutureListener);

        } catch (Exception e) {
            e.printStackTrace();
            //future.addListener(channelFutureListener);
            System.out.println("关闭连接");
        }
    }

    public static final Channel getChannel(String host, int port) {
        Channel channel = null;
        try {
            channel = bootstrap.connect(host, port).sync().channel();
        } catch (Exception e) {
            logger.error(
                    String.format("连接Server(IP[%s],PORT[%s])失败", host, port), e);
            return null;
        }
        return channel;
    }

    public static void sendfeezu(ByteBuf feezu) throws Exception {
        if (channel != null) {
            channel.writeAndFlush(feezu).sync();
//            channel.close();
        } else {
            logger.warn("消息发送失败,连接尚未建立!");
        }
    }

    public static void main(String[] args) throws Exception {
        try {
        		String authorizationCode = "7E0035010001023130303030303030303030303030303031C784D88B14D8E9FFBF350F5D1D4F825389725E7D51E215BF3D8DEB28181B24F890367E";
        		String temp = "7E 00 41 01 00 01 01 31 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 31 BB EB 7C F8 FA 89 B2 F9 98 58 B5 C4 47 6B 7A 34 C4 A6 42 D8 FB 5F 92 63 44 7B 52 09 7F E6 B1 33 9D 7E";
        		
        		ByteBuf buffer =   Unpooled.buffer(1000);  
//        		buffer.writeBytes(TextUtils.hexStringToByte(CreateMessage.createLoginMessage("contihuangjian003","abcdefg")));
        		buffer.writeBytes(TextUtils.hexStringToByte(temp));
        		TcpClientNotStatic.sendfeezu(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String[] createMessage(){
    	String[] tboxIdArray = new String[9];
		String[] iCCIDArray = new String[9];
		String[] loginMessage = new String[9];
		for (int i = 0; i < tboxIdArray.length; i++) {
			StringBuffer sb = new StringBuffer();
			for (int j = 0; j < 17; j++) {
				sb.append(String.valueOf(i+1));
			}
			tboxIdArray[i] = sb.toString();
			iCCIDArray[i] = sb.append(String.valueOf(i+1)).append(String.valueOf(i+1)).toString();
			System.out.println(tboxIdArray[i]);
			System.out.println(iCCIDArray[i]);
		}
		
		return loginMessage;
    }
}