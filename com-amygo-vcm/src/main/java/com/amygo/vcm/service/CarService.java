package com.amygo.vcm.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.amygo.persis.domain.Car;
import com.amygo.persis.repository.CarJpaRepository;

public class CarService {
	@Autowired
	private CarJpaRepository carJpaRepository;
	
	public void updateCar(Car car) {
		carJpaRepository.save(car);
	}

}
