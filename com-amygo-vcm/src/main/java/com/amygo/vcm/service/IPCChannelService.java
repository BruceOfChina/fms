package com.amygo.vcm.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.common.BaseResult;
import com.amygo.common.constants.IPCResConstants;
import com.amygo.common.util.TextUtils;
import com.amygo.persis.domain.IPCMessageLog;
import com.amygo.persis.repository.IPCMessageLogJpaRepository;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.util.DataParseUtil;

import io.netty.channel.Channel;

@Service
public class IPCChannelService {
	
	private static Logger log = LoggerFactory.getLogger(IPCChannelService.class);
	
	public static Map<String,Channel> map = new ConcurrentHashMap<String,Channel>();
	
	public static Map<String, LinkedBlockingQueue<Object>> acknowledgeQueueMap = new HashMap<String, LinkedBlockingQueue<Object>>();
	
	@Autowired
	private IPCMessageLogJpaRepository ipcMessageLogJpaRepository;
	
	public void add(String ipcId,Channel channel) {
		map.put(ipcId, channel);
	}
	
	public Channel get(String ipcId) {
		return map.get(ipcId);
	}
	
	public BaseResult getCommandResponseDTO(MessageStructure ms) {
		BaseResult br = new BaseResult();
		if(!this.sendMessage(ms)) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessage("设备未登录");
			br.setMessageCode(IPCResConstants.IPC_NOT_LOGIN);
			return br;
		}
		LinkedBlockingQueue<Object> acknowledgeQueue = this.acknowledgeQueueMap.get(ms.getIpcId());
		if(acknowledgeQueue==null) {
			acknowledgeQueue = new LinkedBlockingQueue<Object>();
		}
		acknowledgeQueueMap.put(ms.getIpcId(), acknowledgeQueue);
		
		Object obj = null;
		try {
			obj = acknowledgeQueue.poll(10, TimeUnit.SECONDS);
		} catch (Exception ex) {
			log.error("error occured when poll response from ipc : " + ms.getIpcId(), ex);
		} finally {
			acknowledgeQueue.clear();
		}
		if(obj==null) {
			br.setResultCode(BaseResult.ERROR);
			br.setMessage("设备响应超时");
		}else if(obj!=null) {
			br.setResultCode(BaseResult.SUCCESS);
			br.setData(obj);
		}
		
		return br;
	}
	
	public boolean sendMessage(MessageStructure ms) {
		boolean result = true;
		try {
			byte[] bytes = DataParseUtil.parseData(ms);
			String hexString = TextUtils.toHexString(bytes);
			log.info("Begin to send message: " + hexString +" to device: " + ms.getIpcId());
			Channel channel = map.get(ms.getIpcId());
			if (channel != null) {
				switch (ms.getApplicationId()) {
				case ApplicationConstants.IPC_PATH_PLAN:
					log.info("Begin to send ipc path plan message: " + hexString +" to device: " + ms.getIpcId());
					break;
				default:
					log.info("Begin to send other command message: " + hexString +" to device: " + ms.getIpcId()+"ApplicationId:" + ms.getApplicationId());
					break;
				}
				channel.writeAndFlush(bytes);
				log.info("Send message: " + hexString +" to device: " + ms.getIpcId() + "..... success");
				IPCMessageLog ipcMessageLog = new IPCMessageLog(
						ms.getIpcId(),
						hexString,
						new Date(),
						null,
						Integer.toHexString(ms.getApplicationId()),
						ms.getMessageId(),
						null);
				ipcMessageLogJpaRepository.save(ipcMessageLog);
			}else {
				log.error("No channel for device:" + ms.getIpcId() + "....error");
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Send message: hexString "+" to device: " + ms.getIpcId() + "..... error");
			result = false;
		}
		return result;
	}

}
