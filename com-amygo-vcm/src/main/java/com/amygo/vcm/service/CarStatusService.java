package com.amygo.vcm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.repository.CarStatusJpaRepository;

@Service
public class CarStatusService {
	@Autowired
	private CarStatusJpaRepository carStatusJpaRepository;
	
	public void updateCarStatus(CarStatus carStatus) {
		carStatusJpaRepository.save(carStatus);
	}
	
	public CarStatus getCarStatus(String ipcId) {
		return carStatusJpaRepository.findCarStatusByIpcId(ipcId).orElse(new CarStatus());
	}
	
	

}
