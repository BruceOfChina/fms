package com.amygo.vcm;

public class ApplicationConstants {
	public static final String HANDLER = "HANDLER";
	
	public static final int IPC_LOGIN = 0x0101;
	
	public static final int IPC_PATH_PLAN = 0x0102;
	
	public static final int PARAM_CONFIG = 0x0103;
	
	public static final int IPC_LOCATION = 0x0104;
	
	public static final int IPC_ARRIVE_START_END_POINT = 0x0105;
	
	public static final int IPC_DESTINATION_PLAN = 0x0106;
	
	public static final int IPC_ARRIVE_DESTINATION = 0x0107;
	
	public static final int IPC_VEHICLE_START_STOP = 0x0108;	
	
	public static final int IPC_VEHICLE_STEERING_WHEEL = 0x0109;
	
	public static final int IPC_VEHICLE_ACCELERATION_DECELERATION = 0x010A;
	
	public static final int IPC_VEHICLE_REMOTE_SET = 0x010B;
	
}
