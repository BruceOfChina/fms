package com.amygo.vcm.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amygo.common.Constants;
import com.amygo.common.util.BitsHelper;
import com.amygo.common.util.HexXOR;
import com.amygo.common.util.SecurityUtils;
import com.amygo.common.util.TextUtils;
import com.amygo.exception.CustomException;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;

public class DataParseUtil {
    
    private static Logger log = LoggerFactory.getLogger(DataParseUtil.class);
	
	public static byte[] parseData(MessageStructure commonMessage) {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();  
		try {
			bo.write(BitsHelper.LongToByteArray(System.currentTimeMillis()/1000));
			bo.write(BitsHelper.IntegerToByteArray(0));
			bo.write(BitsHelper.IntegerToByteArray(commonMessage.getMessageId()));
			bo.write(BitsHelper.IntegerToByteArray(commonMessage.getApplicationDataLength()));
			Map<String,Object> map = commonMessage.getApplicationDataMap();
			parseMapDataToByte(bo, map);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		byte[] encryptMessageBody = SecurityUtils.AES_cbc_encrypt(bo.toByteArray(), commonMessage.getsKey(), commonMessage.getsIv());
//		byte[] encryptMessageBody = bo.toByteArray();
		System.out.println("unencrypt message body:" + TextUtils.toHexStringWithSpace(bo.toByteArray()));
		int messageLength = 21 + encryptMessageBody.length;//编码后的消息长度：messageCount到message Tail 2+加密后的message Body	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		try {
			baos.write(BitsHelper.IntegerToByteArray(messageLength));
			short securityType = commonMessage.getSecurityType()!=null?commonMessage.getSecurityType().shortValue():1;
			baos.write(BitsHelper.ShortToByteArray(securityType));
			baos.write(BitsHelper.ShortToByteArray(commonMessage.getMessageCount()));
			baos.write(BitsHelper.IntegerToByteArray(commonMessage.getApplicationId()));
			baos.write(commonMessage.getIpcId().getBytes());
			baos.write(encryptMessageBody);
			byte messageTail = HexXOR.xor(baos.toByteArray());
			baos.write(messageTail);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] result = baos.toByteArray();
		return encode7E(result);
	}

	
	/**
	 * Parse different type of data value to hex string
	 * 
	 * @param value
	 * 			<code>String</code> Use Method {@link TextUtils}， <code>Integer</code> Use MethodInteger.toHexString ,
	 * 			<code>Long</code> Use Method Long.toHexString， <code>Byte</code> Use Method {@link TextUtils}} toHexString
	 * @return hexStr
	 */
	private static void parseMapDataToByte(ByteArrayOutputStream bo,Map<String,Object> map) {
		try {
			for(Map.Entry<String, Object> entry:map.entrySet()){
				Object value = entry.getValue();
				if(value instanceof String){
					bo.write(((String) value).getBytes());
				}else if(value instanceof Integer){
					bo.write(BitsHelper.IntegerToByteArray((Integer)value));
				}else if(value instanceof Long){
					bo.write(BitsHelper.LongToByteArray((Long)value));
				}else if(value instanceof byte[]){
					bo.write(((byte[]) value));
				}else if(value instanceof Short){
					bo.write(BitsHelper.ShortToByteArray((Short)value));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * parse object data to byte
	 * @param message
	 * @param applicationId
	 * @return
	 */
	public static byte[] parseObjectToByte(Object obj){
		byte[] bytes = null;  
	    try {  
	        // object to bytearray  
	        ByteArrayOutputStream bo = new ByteArrayOutputStream();  
	        ObjectOutputStream oo = new ObjectOutputStream(bo);  
	        oo.writeObject(obj);  
	        bytes = bo.toByteArray();  
	        bo.close();  
	        oo.close();  
	    } catch (Exception e) {  
	        log.info("parse data " + obj.toString()+" error");  
	    }  
	    return bytes;  
	}
	
	
	public static String padLeft(String s, int length)
	{
	    byte[] bs = new byte[length];
	    byte[] ss = s.getBytes();
	    Arrays.fill(bs, (byte) (48 & 0xff));
	    System.arraycopy(ss, 0, bs,length - ss.length, ss.length);
	    return new String(bs);
	}
	/**
	 * encode the bytes : 7E -> 5E7D; 5E ->5E5D
	 * @param inBuf
	 * @param startPos
	 * @param length
	 * @return
	 */
	public static byte[] encode7E(byte[] inBuf, int startPos, int length){
		int endIndex = startPos + length;
		assert (inBuf.length >= endIndex);
		if (inBuf.length == 0) {
			throw new CustomException("can not decode 7E", TextUtils.toHexStringWithSpace(inBuf));
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		out.write(0x7E);
		for (int i = startPos; i < endIndex; i++) {
			if (inBuf[i] == 0x7E) {
				out.write(0x5E);
				out.write(0x7D);
			} else if(inBuf[i] == 0x5E){
				out.write(0x5E);
				out.write(0x5D);
			}else{
				out.write(inBuf[i]);
			}
		}
		out.write(0x7E);
		return out.toByteArray();
	}
	
	public static byte[] encode7E(byte[] inBuf){
		return encode7E(inBuf, 0, inBuf.length);
	}
	/**
	 * decode the bytes : 5E 5D -> 5E; 5E 7D -> 7E
	 * 
	 * @param inBuf
	 * @param startPos
	 * @param length
	 * @return
	 */
	public static byte[] decode7E(byte[] inBuf, int startPos, int length) {

		int endIndex = startPos + length;
		assert (inBuf.length >= endIndex);
		if (inBuf.length == 0 || inBuf[endIndex - 1] == 0x5E) {
			throw new CustomException("can not decode 7E", TextUtils.toHexStringWithSpace(inBuf));
		}

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		for (int i = startPos; i < endIndex; i++) {
			if (inBuf[i] != 0x5E) {
				out.write(inBuf[i]);
			} else {
				i++;
				if (inBuf[i] == 0x5D)// 5E 5D -> 5E
				{
					out.write(0x5E);
				} else if (inBuf[i] == 0x7D)// 5E 7D -> 7E
				{
					out.write(0x7E);
				} else {
					throw new CustomException("can not decode 7E", TextUtils.toHexStringWithSpace(inBuf));
				}
			}
		}

		return out.toByteArray();
	}
	
	public static byte[] decode7E(byte[] inBuf) {
		return decode7E(inBuf,0,inBuf.length);
	}
	
	public static String decode7E(String str, int startPos, int length){
		int endIndex = startPos + length;
		return null;
	}
	
	
}
