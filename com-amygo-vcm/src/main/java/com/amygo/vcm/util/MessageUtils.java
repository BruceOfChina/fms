package com.amygo.vcm.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.amygo.common.Constants;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;

public class MessageUtils {
	public static String createIpcLocationMessage(String ipcId,Long lat,Long lon) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		if(lat==null) {
			lat = Math.round(31*Math.pow(10, 7));
		}
		if(lon==null) {
			lon = Math.round(121*Math.pow(10, 7));
		}
		
		map.put("isLocation", (short)1);//1
		map.put("direction", 0);//2
		map.put("lon", lon);//4
		map.put("lat", lat);//4
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_LOCATION);
		ms.setMessageId(1);
		ms.setApplicationDataLength(11);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
	
	public static String createIpcDestination(String ipcId,int messageId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		Long lat = Math.round(31*Math.pow(10, 7));
		Long lon = Math.round(121*Math.pow(10, 7));
		map.put("isLocation", (short)1);
		map.put("direction", 0);
		map.put("lon", lon);
		map.put("lat", lat);
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_ARRIVE_DESTINATION);
		ms.setMessageId(messageId);
		ms.setApplicationDataLength(11);
		ms.setSecurityType(2);
		ms.setsKey(Constants.DEFAULT_SKEY);
		ms.setsIv(Constants.DEFAULT_SIV);
		byte[] bytes = DataParseUtil.parseData(ms);
		String hexString = TextUtils.toHexStringWithSpace(bytes);
		return hexString;
	}
	
	public static void main(String[] args) {
		String ipcId = "10000000000000001";
//		Long lat = 312883616L;
//		Long lon = 1211625266L;
		String hexString = createIpcLocationMessage(ipcId,null,null);
		System.out.println(hexString);
	}
}
