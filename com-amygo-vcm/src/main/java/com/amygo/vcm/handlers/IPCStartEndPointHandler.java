package com.amygo.vcm.handlers;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.common.BaseResult;
import com.amygo.common.constants.ApiResConstants;
import com.amygo.common.enums.CarStatusDetailEnum;
import com.amygo.common.enums.CarStatusEnum;
import com.amygo.common.enums.OrderStatusEnum;
import com.amygo.common.util.BitsHelper;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.Order;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.OrderJpaRepository;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.dto.IPCArriveDestinationDTO;
import com.amygo.vcm.interfaces.Handler;
import com.amygo.vcm.service.CarService;
@Service(ApplicationConstants.HANDLER+ApplicationConstants.IPC_ARRIVE_START_END_POINT)
public class IPCStartEndPointHandler implements Handler{
	private static Logger log = LoggerFactory.getLogger(IPCStartEndPointHandler.class);
	
	@Autowired
	private CarJpaRepository carJpaRepository;
	@Autowired
	private OrderJpaRepository orderJpaRepository;

	@Override
	public Object handle(Object obj) {
		MessageStructure ms = (MessageStructure)obj;
		byte[] applicationData = ms.getApplicationData();
		short isLocated = BitsHelper.getUInt8(applicationData,0);
		int direction = BitsHelper.getUInt16B(applicationData, 1);
		long lon = BitsHelper.getUInt32B(applicationData, 3);
		long lat = BitsHelper.getUInt32B(applicationData,7);
		log.info(" car arrive to :"+lon+","+lat);
		String ipcId = ms.getIpcId();
		IPCArriveDestinationDTO dto = new IPCArriveDestinationDTO(isLocated,direction,lon,lat);
		Car car = carJpaRepository.findByIpcId(ipcId).orElse(null);
		if(car == null) {
			return dto;
		}
		if(ms.getMessageId()==1) {
			//到达起始点，增加一点：车辆没有发送到站提醒，需要FMS自动判断
			log.info("car arrive to start point.....");
			car.setStatusDetail(CarStatusDetailEnum.CAR_ARRIVE_START_POINT.getStatus());
			//到达起始点，发送通知到乘客APP，调用消息通知服务，APP端通知乘客车辆最后停车位置，车辆此时停车
		
		}else if(ms.getMessageId()==3) {//到达终点 车辆恢复自由状态，订单变为待支付状态
			log.info("car arrive to end point.....");
			Order order = orderJpaRepository.findByOrderCode(car.getOrderCode()).orElse(null);
			if(order==null||OrderStatusEnum.ORDER_IN_PROCESS.getStatus()!=order.getOrderStatus()) {
				return dto;
			}
			order.setArriveTime(new Date());
			order.setOrderStatus(OrderStatusEnum.ORDER_FINISHED.getStatus());
			orderJpaRepository.save(order);
			car.setStatusDetail(CarStatusDetailEnum.CAR_ARRIVE_END_POINT.getStatus());
			car.setStatus(CarStatusEnum.CAR_TO_USE.getStatus());
			car.setOrderCode(null);
		}
		carJpaRepository.save(car);
		return dto;
	}
}
