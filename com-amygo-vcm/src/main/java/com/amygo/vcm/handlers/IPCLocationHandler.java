package com.amygo.vcm.handlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.common.CacheKeyConstants;
import com.amygo.common.Constants;
import com.amygo.common.entity.Coordinate;
import com.amygo.common.enums.OrderStatusEnum;
import com.amygo.common.util.BitsHelper;
import com.amygo.common.util.CacheUtil;
import com.amygo.common.util.CommonUtils;
import com.amygo.common.util.CoordinateConvertUtil;
import com.amygo.common.util.PointPolygonUtil;
import com.amygo.common.util.TextUtils;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.CarStatus;
import com.amygo.persis.domain.CarStatusRecord;
import com.amygo.persis.domain.Order;
import com.amygo.persis.domain.SystemVariable;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.CarStatusRecordJpaRepository;
import com.amygo.persis.repository.OrderJpaRepository;
import com.amygo.persis.repository.SystemVariableJpaRepository;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.dto.IPCArriveDestinationDTO;
import com.amygo.vcm.interfaces.Handler;
import com.amygo.vcm.service.CarStatusService;
import com.amygo.vcm.service.IPCChannelService;
import com.amygo.vcm.websocket.WebSocketServer;

@Service(ApplicationConstants.HANDLER + ApplicationConstants.IPC_LOCATION)
public class IPCLocationHandler implements Handler {
	private static Logger log = LoggerFactory.getLogger(IPCLocationHandler.class);
	@Autowired
	private CarJpaRepository carJpaRepository;
	@Autowired
	private CarStatusService carStatusService;
	@Autowired
	private CarStatusRecordJpaRepository carStatusRecordJpaRepository;
	@Autowired
	private OrderJpaRepository orderJpaRepository;
	@Autowired
	private CommonUtils commonUtils;
	@Autowired
	private SystemVariableJpaRepository systemVariableJpaRepository;
	@Autowired
	private IPCChannelService ipcChannelService;
	@Override
	public Object handle(Object obj) {
		MessageStructure ms = (MessageStructure) obj;
		byte[] applicationData = ms.getApplicationData();
		log.info("Ipc location message body:" + TextUtils.toHexString(applicationData));
		int pos = 0;
		short isLocated = BitsHelper.getUInt8(applicationData, pos);
		pos = pos + 1;
		int direction = BitsHelper.getUInt16B(applicationData, pos);
		pos = pos + 2;
		Double lon = BitsHelper.getUInt32B(applicationData, pos) / Math.pow(10, 7);
		pos = pos + 4;
		Double lat = BitsHelper.getUInt32B(applicationData, pos) / Math.pow(10, 7);
		pos = pos + 4;
		Double speed = null;
		if (applicationData.length - pos >= 2) {
			speed = BitsHelper.getUInt16B(applicationData, pos) / Math.pow(10, 2);
		}
		IPCArriveDestinationDTO dto = new IPCArriveDestinationDTO(isLocated, direction, lon.longValue(),lat.longValue());
		String ipcId = ms.getIpcId();
		CarStatus carStatus = carStatusService.getCarStatus(ipcId);

		carStatus.setLon(lon);
		carStatus.setLat(lat);
		carStatusService.updateCarStatus(carStatus);

		CarStatusRecord carStatusRecord = new CarStatusRecord();
		double[] gcjLatLon = CoordinateConvertUtil.wgs2GCJ(lat, lon);
		carStatusRecord.setCarId(carStatus.getId());
		carStatusRecord.setVinCode(carStatus.getVinCode());
		carStatusRecord.setDirection(String.valueOf(direction));
		carStatusRecord.setGcjLat(gcjLatLon[0]);
		carStatusRecord.setGcjLon(gcjLatLon[1]);
		carStatusRecord.setLatitude(lat);
		carStatusRecord.setLongitude(lon);
		carStatusRecord.setDeviceTime(ms.getDeviceTime());
		carStatusRecord.setServerTime(System.currentTimeMillis());
		carStatusRecord.setSpeed(speed);
		try {
			String message = commonUtils.toJson(new Coordinate(gcjLatLon[0], gcjLatLon[1], speed));
//			String message = commonUtils.toJson(new Coordinate(lat, lon, speed));
			WebSocketServer.sendInfo(message, carStatus.getIpcId());
		} catch (IOException e) {
			log.error("call web SocketServer error", e);
		}
		SystemVariable systemVariable = systemVariableJpaRepository.findByVariableCategoryAndVariableKey("ELECTRONIC_FENCE", "ELECTRONIC_FENCE").orElse(null);
		Car car = carJpaRepository.findOne(carStatus.getId());
		Coordinate lastCoordinate = (Coordinate) CacheUtil.get(CacheKeyConstants.VEHICLE_LOCATION, car.getVinCode());
		if(systemVariable!=null&&lastCoordinate!=null) {
			Map<String,Double> currentLocationMap = new HashMap<String,Double>();
			Map<String,Double> lastLocationMap = new HashMap<String,Double>();
			currentLocationMap.put("lon", gcjLatLon[1]);
			currentLocationMap.put("lat", gcjLatLon[0]);
			lastLocationMap.put("lon", lastCoordinate.getLon());
			lastLocationMap.put("lat", lastCoordinate.getLat());
			/*
			 * 出电子围栏：1 车辆上次位置在圈内；2 车辆当前位置在圈外；
			 */
			if (PointPolygonUtil.isInPolygon(lastLocationMap, systemVariable.getVariableValue())
			 &&!PointPolygonUtil.isInPolygon(currentLocationMap, systemVariable.getVariableValue())) {
				//发送熄火指令到车辆
				log.info("vehicle:"+car.getVinCode() + " overstep the electronic fence.");
				sendStopCommand(ms.getIpcId());
				try {
					String message = carStatus.getVinCode() + "已出电子围栏";
					WebSocketServer.sendInfo(message, carStatus.getIpcId());
				} catch (IOException e) {
					log.error("call web SocketServer error", e);
				}
			}
		}
		carStatusRecord.setOrderCode(car.getOrderCode());
		carStatusRecordJpaRepository.save(carStatusRecord);
		log.info("car location:" + carStatus);

		/**
		 * 计算订单里程和费用
		 */
		Order order = orderJpaRepository.findByOrderCode(car.getOrderCode()).orElse(null);
		if (order == null || OrderStatusEnum.ORDER_IN_PROCESS.getStatus() != order.getOrderStatus()) {
			log.info("order is not on process,no need to calculate order mileage and fee");
			return dto;
		}
		Integer orderMileage = order.getOrderMileage();
		if(lat>0&&lon>0&&lastCoordinate.getLat()>0&&lastCoordinate.getLon()>0) {
			Coordinate thisCoordinate = new Coordinate(lat, lon, speed);
			CacheUtil.put(CacheKeyConstants.VEHICLE_LOCATION, car.getVinCode(), thisCoordinate);
			Double distance = CoordinateConvertUtil.distance(lastCoordinate.getLat(), lastCoordinate.getLon(), lat, lon);
			orderMileage = orderMileage + distance.intValue();
			Integer originalFee = orderMileage / 10; // 1RMB/KM; 后期按梯度收费
			order.setOrderMileage(orderMileage);
			order.setOriginalFee(originalFee);
		}
		orderJpaRepository.save(order);
		return dto;
	}
	private void sendStopCommand(String ipcId) {
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("commandType", (short)0);
		MessageStructure ms = new MessageStructure();
		ms.setApplicationDataMap(map);
		ms.setIpcId(ipcId);
		ms.setApplicationId(ApplicationConstants.IPC_VEHICLE_START_STOP);
		ms.setMessageId(1);
		ms.setApplicationDataLength(1);
		String skey = (String) CacheUtil.get("IPC_SKEY", ms.getIpcId());
		String sIv = (String) CacheUtil.get("IPC_SIV", ms.getIpcId());
		if(StringUtils.isNotBlank(skey)) {
			ms.setsKey(skey.getBytes());
		}else {
			ms.setsKey(Constants.DEFAULT_SKEY);
		}
		if(StringUtils.isNotBlank(sIv)) {
			ms.setsIv(sIv.getBytes());
		}else {
			ms.setsIv(Constants.DEFAULT_SIV);
		}
		ipcChannelService.sendMessage(ms);
	}
}
