package com.amygo.vcm.handlers;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.common.util.BitsHelper;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.interfaces.Handler;
import com.amygo.vcm.service.IPCChannelService;
@Service(ApplicationConstants.HANDLER+ApplicationConstants.IPC_VEHICLE_STEERING_WHEEL)
public class IPCVehicleSteeringWheelHandler implements Handler{
	@Autowired
	private IPCChannelService ipcChannelService;

	@Override
	public Object handle(Object obj) {
		MessageStructure ms = (MessageStructure)obj;
		byte[] applicationData = ms.getApplicationData();
		int pos = 0;
		int result = BitsHelper.getUInt8(applicationData, pos);
		pos = pos + 1;
		int errorCode = BitsHelper.getUInt8(applicationData, pos);
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		map.put("result", result);
		map.put("errorCode", errorCode);
		LinkedBlockingQueue<Object> queue = ipcChannelService.acknowledgeQueueMap.get(ms.getIpcId());
		if(queue!=null) {
			try {
				queue.put(map);
			} catch (InterruptedException e) {
				queue.clear();
				e.printStackTrace();
			}
		}
		return map;
	}
}
