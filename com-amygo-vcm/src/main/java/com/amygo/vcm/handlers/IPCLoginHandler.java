package com.amygo.vcm.handlers;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amygo.common.Constants;
import com.amygo.common.util.CacheUtil;
import com.amygo.common.util.TextUtils;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.interfaces.Handler;
import com.amygo.vcm.service.IPCChannelService;
import com.amygo.vcm.util.DataParseUtil;
@Service(ApplicationConstants.HANDLER+ApplicationConstants.IPC_LOGIN)
public class IPCLoginHandler implements Handler{
	private static Logger log = LoggerFactory.getLogger(IPCLoginHandler.class);
	
	@Autowired
	private IPCChannelService ipcChannelService;

	@Override
	public Object handle(Object obj) {
		MessageStructure ms = (MessageStructure)obj;
		byte[] applicationData = ms.getApplicationData();
		log.info("udecrypt message body:"+TextUtils.toHexStringWithSpace(applicationData));
		String iccid = new String(applicationData);
		log.info("iccid:"+iccid);
		Map<String,Object> map = new LinkedHashMap<String,Object>();
		String skey = TextUtils.getRandomString(16);
		String sIv = TextUtils.getRandomString(16);
		log.info("sKey:"+skey);
		log.info("sIv:"+sIv);
		map.put("result", (short)1);
		map.put("errorCode", (short)0);
		map.put("sKey", skey.getBytes());
		map.put("sIv", sIv.getBytes());
		ms.setApplicationDataMap(map);
		ipcChannelService.add(ms.getIpcId(), ms.getChannel());
		ms.setMessageId(2);
		ms.setApplicationDataLength(34);
//		ms.setsKey(skey.getBytes());//登录的秘钥和向量已经存放了
//		ms.setsIv(sIv.getBytes());
		CacheUtil.put("IPC_SKEY", ms.getIpcId(), skey);
		CacheUtil.put("IPC_SIV", ms.getIpcId(), sIv);
		ms.getChannel().writeAndFlush(DataParseUtil.parseData(ms));
		return iccid;
	}
}
