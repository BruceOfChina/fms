package com.amygo.vcm.dto;

public class IPCArriveDestinationDTO {
	
	private short isLocated;
	
	private int direction;
	
	private long lon ;
	
	private long lat ;
	
	public short getIsLocated() {
		return isLocated;
	}
	public void setIsLocated(short isLocated) {
		this.isLocated = isLocated;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public long getLon() {
		return lon;
	}
	public void setLon(long lon) {
		this.lon = lon;
	}
	public long getLat() {
		return lat;
	}
	public void setLat(long lat) {
		this.lat = lat;
	}
	public IPCArriveDestinationDTO(short isLocated, int direction, long lon, long lat) {
		super();
		this.isLocated = isLocated;
		this.direction = direction;
		this.lon = lon;
		this.lat = lat;
	}
	
	
}
