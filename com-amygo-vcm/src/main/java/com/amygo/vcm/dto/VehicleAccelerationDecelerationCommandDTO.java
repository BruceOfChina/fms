package com.amygo.vcm.dto;

public class VehicleAccelerationDecelerationCommandDTO {
	private String ipcId;
	
	private Short accelerationOrDeceleration;
	
	private Short commandValue;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public Short getAccelerationOrDeceleration() {
		return accelerationOrDeceleration;
	}

	public void setAccelerationOrDeceleration(Short accelerationOrDeceleration) {
		this.accelerationOrDeceleration = accelerationOrDeceleration;
	}

	public Short getCommandValue() {
		return commandValue;
	}

	public void setCommandValue(Short commandValue) {
		this.commandValue = commandValue;
	}
}
