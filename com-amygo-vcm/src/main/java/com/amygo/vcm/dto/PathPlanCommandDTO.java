package com.amygo.vcm.dto;

public class PathPlanCommandDTO {
	private String ipcId;
	
	private Long startLon;
	
	private Long startLat;
	
	private Long endLat;
	
	private Long endLon;

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public Long getStartLon() {
		return startLon;
	}

	public void setStartLon(Long startLon) {
		this.startLon = startLon;
	}

	public Long getStartLat() {
		return startLat;
	}

	public void setStartLat(Long startLat) {
		this.startLat = startLat;
	}

	public Long getEndLat() {
		return endLat;
	}

	public void setEndLat(Long endLat) {
		this.endLat = endLat;
	}

	public Long getEndLon() {
		return endLon;
	}

	public void setEndLon(Long endLon) {
		this.endLon = endLon;
	}
}
