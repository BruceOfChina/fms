package com.amygo.vcm.interfaces;

public interface Handler {
	
	public Object handle(Object obj);

}
