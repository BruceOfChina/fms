package com.amygo.vcm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amygo.common.util.TextUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class TcpClientHandler extends SimpleChannelInboundHandler<Object> {
	public final static int startValue = (byte)0x7E;
	
    private static final Logger log = LoggerFactory.getLogger(TcpClientHandler.class);
    
	public final static int maxPackageLength = 1024 * 1024;

	private final static int minSizeToProcess = 14;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object feezu)
            throws Exception {
    	byte[] bytes = getByte((ByteBuf) feezu);
    	String responseData = TextUtils.toHexString(bytes);
        log.info("client0接收到服务器返回的消息:" + responseData);
    }
    //simulate tbox 
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object feezu)
            throws Exception {
    	byte[] bytes = getByte((ByteBuf) feezu);
    	String responseData = TextUtils.toHexStringWithSpace(bytes);
        log.info("client接收到服务器返回的消息:" + responseData);
       
    }
    
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    	super.channelInactive(ctx);
    	ctx.close();
    }
    
	private byte[] getByte(ByteBuf in) {
		if (in.readableBytes() < minSizeToProcess) {
			log.warn("Message is too short." + in.readableBytes());
			in.clear();
			return null;
		}
		if (in.readableBytes() > maxPackageLength) {
			log.warn("Message is too long." + in.readableBytes());
			in.clear();
			return null;
		}

		byte[] originData = new byte[in.readableBytes()];
		in.readBytes(originData);

		return originData;
	}
}

