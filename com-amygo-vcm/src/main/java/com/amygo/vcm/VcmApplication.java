package com.amygo.vcm;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;

import com.amygo.common.base.BaseApplication;
import com.amygo.vcm.websocket.LoggerMessage;
import com.amygo.vcm.websocket.LoggerQueue;
@EnableDiscoveryClient
@ComponentScan("com.amygo")
@SpringBootApplication(exclude = {
		SecurityAutoConfiguration.class
})
@EnableScheduling
@EnableWebSocketMessageBroker
public class VcmApplication extends BaseApplication
{
	private static final Logger log = LoggerFactory.getLogger(VcmApplication.class);
    public static void main( String[] args ) throws UnknownHostException
    {
    	SpringApplication app = new SpringApplication(VcmApplication.class);

		SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);

		BaseApplication.start(app, source, log, args);
    }
    
    @Autowired
	private SimpMessagingTemplate messagingTemplate;
    int info=1;
	@Scheduled(fixedRate = 1000000)
	public void outputLogger(){
      log.info("测试日志输出"+info++);
	}
	/**
	 * 推送日志到/topic/pullLogger
	 */
	@PostConstruct
	public void pushLogger(){
		ExecutorService executorService=Executors.newFixedThreadPool(2);
		Runnable runnable=new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						LoggerMessage log = LoggerQueue.getInstance().poll();
						if(log!=null){
							if(messagingTemplate!=null)
							messagingTemplate.convertAndSend("/topic/pullLogger",log);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		executorService.submit(runnable);
	}
}
