package com.amygo.vcm;

import java.util.Map;

import io.netty.channel.Channel;

public class MessageStructure {

	private String ipcId;

	private Long deviceTime;

	private int applicationId;

	private Integer securityType;

	private static int messageCount = 0;

	private int result;

	private int messageId;

	private byte[] applicationData;
	
	private Map<String,Object> applicationDataMap;
	
	private Channel channel;
	
	private byte[] sKey;
	
	private byte[] sIv;
	
	private int applicationDataLength;
	
	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getSecurityType() {
		return securityType;
	}

	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}

	public int getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public Long getDeviceTime() {
		return deviceTime;
	}

	public void setDeviceTime(Long deviceTime) {
		this.deviceTime = deviceTime;
	}

	public String getIpcId() {
		return ipcId;
	}

	public void setIpcId(String ipcId) {
		this.ipcId = ipcId;
	}

	public byte[] getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(byte[] applicationData) {
		this.applicationData = applicationData;
	}

	// generate global message count
	// circulate count : from 0 to 255;
	public static int genMessageCount() {
		return (messageCount++) % 255;
	}

	public Map<String, Object> getApplicationDataMap() {
		return applicationDataMap;
	}

	public void setApplicationDataMap(Map<String, Object> applicationDataMap) {
		this.applicationDataMap = applicationDataMap;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public byte[] getsKey() {
		return sKey;
	}

	public void setsKey(byte[] sKey) {
		this.sKey = sKey;
	}

	public byte[] getsIv() {
		return sIv;
	}

	public void setsIv(byte[] sIv) {
		this.sIv = sIv;
	}

	public int getApplicationDataLength() {
		return applicationDataLength;
	}

	public void setApplicationDataLength(int applicationDataLength) {
		this.applicationDataLength = applicationDataLength;
	}
}
