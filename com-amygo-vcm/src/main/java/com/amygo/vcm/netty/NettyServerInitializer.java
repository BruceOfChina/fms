package com.amygo.vcm.netty;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.netty.handler.timeout.IdleStateHandler;

public abstract class NettyServerInitializer extends ChannelInitializer<SocketChannel> {

	private SslContext sslCtx;
	
	@Value("${longConnection.useSSL:false}")
	private boolean useSSL;
	
	protected abstract NettyServerDecodeHandler createNettyServerDecodeHandler();
	
	protected abstract NettyServerHandler createNettyServerHandler();
	
	protected abstract NettyServerEncodeHandler createNettyServerEncodeHandler();
	
	public NettyServerInitializer() {
		SelfSignedCertificate ssc;
		try {
			ssc = new SelfSignedCertificate();
			sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void initChannel(SocketChannel ch) {
		ChannelPipeline pipeline = ch.pipeline();

		if (sslCtx != null && useSSL) {
			pipeline.addLast(sslCtx.newHandler(ch.alloc()));
		}

		// Enable stream compression (you can remove these two if unnecessary)
		// pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
		// pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));

		// Add the number codec first,
		
		

		// and then business logic.
		// Please note we create a handler for every new channel
		// because it has stateful properties.
		
		pipeline.addLast(new DelimiterBasedFrameDecoder(NettyServerDecodeHandler.maxPackageLength, Unpooled.copiedBuffer(new byte[] {NettyServerDecodeHandler.startValue})));
		pipeline.addLast(createNettyServerDecodeHandler());
		pipeline.addLast(createNettyServerEncodeHandler());
		pipeline.addLast(createNettyServerHandler());
		
	}
}
