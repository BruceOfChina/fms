package com.amygo.vcm.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amygo.common.util.TextUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class NettyServerEncodeHandler extends MessageToByteEncoder<byte[]> {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void encode(ChannelHandlerContext ctx, byte[] lib, ByteBuf out) throws Exception {
		String responseData = TextUtils.toHexString(lib);
		log.info("Server send message to ipc :" + responseData);
		out.writeBytes(lib);
	}

}
