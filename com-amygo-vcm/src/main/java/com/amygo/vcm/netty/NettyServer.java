package com.amygo.vcm.netty;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

@Service
public class NettyServer {

	@Value("${netty.server.port:9001}")
	private int port = 9001;

	Channel serverChannel;

	@Resource
	private ServerBootstrap serverBoot;
	
	@PostConstruct
	public void startServer() throws Exception {
			serverChannel = serverBoot.bind(port).channel();
			serverChannel.closeFuture().addListener(new GenericFutureListener<Future<? super Void>>() {
				@Override
				public void operationComplete(Future<? super Void> future) throws Exception {
					serverBoot.group().shutdownGracefully();
					serverBoot.childGroup().shutdownGracefully();

				}
			});// .sync();]
	}
	

	@PreDestroy
	void shutdown() throws Exception {
		serverChannel.close();
	}

}
