package com.amygo.vcm.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NettyServerHandler extends SimpleChannelInboundHandler<Object> {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Override
	public void channelRead0(ChannelHandlerContext ctx, Object message) throws Exception {
		ctx.writeAndFlush(message);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		log.info("tcp connection start. " + ctx.toString());
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.info("tcp connection stop. " + ctx.toString());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		log.info(cause.toString());
	}
}
