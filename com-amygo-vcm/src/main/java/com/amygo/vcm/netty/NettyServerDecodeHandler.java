package com.amygo.vcm.netty;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.amygo.common.Constants;
import com.amygo.common.util.ApplicationContextUtil;
import com.amygo.common.util.BitsHelper;
import com.amygo.common.util.CacheUtil;
import com.amygo.common.util.CommonUtils;
import com.amygo.common.util.HexXOR;
import com.amygo.common.util.SecurityUtils;
import com.amygo.common.util.TextUtils;
import com.amygo.persis.domain.Car;
import com.amygo.persis.domain.IPCMessageLog;
import com.amygo.persis.repository.CarJpaRepository;
import com.amygo.persis.repository.IPCMessageLogJpaRepository;
import com.amygo.vcm.ApplicationConstants;
import com.amygo.vcm.MessageStructure;
import com.amygo.vcm.interfaces.Handler;
import com.amygo.vcm.util.DataParseUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class NettyServerDecodeHandler extends ByteToMessageDecoder {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public final static int startValue = (byte)0x7E;
	
	public final static int endValue = (byte)0x7E;

	public final static int maxPackageLength = 1024 * 1024;

	private final static int minSizeToProcess = 12;
	@Autowired
	private CarJpaRepository carJpaRepository;
	@Autowired
	private IPCMessageLogJpaRepository ipcMessageLogJpaRepository;
	@Autowired
	private CommonUtils commonUtils;
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf obj, List<Object> out) throws Exception {
						
		byte[] originData = getByte(obj);
		if (originData != null) {
			String hexString = TextUtils.toHexStringWithSpace(originData);
			log.info("get request data from IPC : "+ hexString);
			//转义还原
			byte[] decoded7EData = DataParseUtil.decode7E(originData);
			//验证校验码
			if(!checkCode(decoded7EData)){ 
				log.info("check code is wrong,data has been altered");
				return;
			}
			MessageStructure ms = (MessageStructure) parseData(decoded7EData);
			ms.setChannel(ctx.channel());
			Handler handler = (Handler) ApplicationContextUtil.getBean(ApplicationConstants.HANDLER + ms.getApplicationId());
			Object handleResultData = handler.handle(ms);
			String resultJson = handleResultData==null?null:commonUtils.toJson(handleResultData);
			IPCMessageLog ipcMessageLog = new IPCMessageLog(
					ms.getIpcId(),
					hexString,
					new Date(),
					new Date(ms.getDeviceTime()*1000),
					Integer.toHexString(ms.getApplicationId()),
					ms.getMessageId(),
					resultJson);
			ipcMessageLogJpaRepository.save(ipcMessageLog);
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		log.info("tcp connection  connected*******************,IP:"+getIPString(ctx));
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.info("tcp connection  stopped*********************,IP:"+getIPString(ctx));
		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		if(cause instanceof java.io.IOException){
		}else{
			log.error("tcp channel error, IP:"+getIPString(ctx)+"cause:{}", cause.getMessage());
			cause.printStackTrace();
		}
	}

	public String getIPString(ChannelHandlerContext ctx) {
		String socketString = ctx.channel().remoteAddress().toString();
		return socketString;
	}

	private byte[] getByte(ByteBuf in) {
		if (in.readableBytes() < minSizeToProcess) {
			log.warn("Message is too short." + in.readableBytes());
			in.clear();
			return null;
		}
		if (in.readableBytes() > maxPackageLength) {
			log.warn("Message is too long." + in.readableBytes());
			in.clear();
			return null;
		}

		byte[] originData = new byte[in.readableBytes()];
		in.readBytes(originData);

		return originData;
	}
	
	private boolean checkCode(byte[] bytes){
		int checkCodeFromDevice = BitsHelper.getUInt8(bytes, bytes.length - 1);   //校验码
		int checkCodeAutoGen = BitsHelper.getUInt8(HexXOR.xor(bytes,bytes.length-1));//最后一个字节是校验码
		return checkCodeFromDevice==checkCodeAutoGen;
	}
	
	public Object parseData(byte[] requestData) {
		MessageStructure commonMessage = new MessageStructure();
		try {
			int offset = 0;
			int messageLength = BitsHelper.getUInt16B(requestData, offset);// 此处需要做一个编码以后的消息编码后的消息长度(Message
			int currentMessageLength = requestData.length-3;
			if(messageLength!=currentMessageLength) {
				log.error("message length error,messageLengh:"+messageLength+"currentMessageLength:"+currentMessageLength);
			}
			offset = offset + 2;
			int securityType = BitsHelper.getUInt8(requestData, offset);// 0:Byte 实际传输过程中，都是加密的
			offset = offset + 1;
			int messageCount = BitsHelper.getUInt8(requestData, offset);// 消息循环计数 0-255
			offset = offset + 1;
			int applicationId = BitsHelper.getUInt16B(requestData, offset);
			offset = offset + 2;
			char[] ipcIdArray = BitsHelper.getCharArray(requestData, offset, 17);
			String ipcId = new String(ipcIdArray);
			log.info("ipcid:"+ipcId.trim());
			offset = offset + 17;
			byte[] source = new byte[requestData.length-offset-1];
			System.arraycopy(requestData, offset, source, 0, source.length);
			if(applicationId==ApplicationConstants.IPC_LOGIN) {
				Car car = carJpaRepository.findByIpcId(ipcId).orElse(null);
				if(car!=null&&StringUtils.isNotBlank(car.getRootKey())&&StringUtils.isNotBlank(car.getInitIv())) {
					source = SecurityUtils.AES_cbc_decrypt(source, car.getRootKey().getBytes(), car.getInitIv().getBytes());
					commonMessage.setsKey(car.getRootKey().getBytes());
					commonMessage.setsIv(car.getInitIv().getBytes());
				}else {
					return null;
				}
			}else if(securityType==2) {
				source = SecurityUtils.AES_cbc_decrypt(source, Constants.DEFAULT_SKEY, Constants.DEFAULT_SIV);
				commonMessage.setsKey(Constants.DEFAULT_SKEY);
				commonMessage.setsIv(Constants.DEFAULT_SIV);
			}else if(applicationId!=ApplicationConstants.IPC_LOGIN&&securityType==1){
				String sKey = (String) CacheUtil.get("IPC_SKEY", ipcId);
				String sIv = (String) CacheUtil.get("IPC_SIV", ipcId);
				sKey = sKey!=null?sKey:"idyuuexpwskjkkrk";
				sIv = sIv!=null?sIv:"guwexksnhxmybhkw";
				log.info("encrypt part:"+TextUtils.toHexString(source));
				source = SecurityUtils.AES_cbc_decrypt(source, sKey.getBytes(), sIv.getBytes());
				log.info("decrypt part:"+TextUtils.toHexString(source));
				commonMessage.setsKey(sKey.getBytes());
				commonMessage.setsIv(sIv.getBytes());
			}
			int offsetNew = 0;
			Long eventCreationTime = BitsHelper.getUInt32B(source, offsetNew);// 消息创建时间
			offsetNew = offsetNew + 4;
			int result = BitsHelper.getUInt16B(source, offsetNew);// 存储T平台、TBOX处理每次业务请求后返回的应答码信息
			offsetNew = offsetNew + 2;
			int messageId = BitsHelper.getUInt16B(source, offsetNew);
			offsetNew = offsetNew + 2;
			int applicationDataLength = BitsHelper.getUInt16B(source, offsetNew);// 此处需要添加一个check：判断application
			offsetNew = offsetNew + 2;
			int applicationDataLengthCurrent = source.length - offsetNew;
			if(applicationDataLength!=applicationDataLengthCurrent) {
				log.error("application data length is not match with the real length of application data");
			}
			int applicationDataRealLength = applicationDataLengthCurrent>applicationDataLength?applicationDataLengthCurrent:applicationDataLength;
			commonMessage.setApplicationId(applicationId);
			commonMessage.setMessageCount(messageCount);
			commonMessage.setResult(result);
			commonMessage.setSecurityType(securityType);
			commonMessage.setMessageId(messageId);
			commonMessage.setIpcId(ipcId);
			commonMessage.setDeviceTime(eventCreationTime);
			commonMessage.setSecurityType(securityType);
			commonMessage.setApplicationData(BitsHelper.subBytes(source, offsetNew, applicationDataRealLength));
			return commonMessage;
		} catch (Exception e) {
			log.info("Parse Data error: ", e);
		}		
		return null;
	}

}
