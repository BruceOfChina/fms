package com.amygo.common;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amygo.common.util.CommonUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
public class UtilConfig {
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	@Bean
	public ObjectMapper jsonMapper() {
		ObjectMapper jsonMapper = new ObjectMapper();

		jsonMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		jsonMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
		jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		jsonMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		jsonMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
		return jsonMapper;
	}
	
	@Bean
	public CommonUtils commonUtils() {
		return new CommonUtils();
	}

}
