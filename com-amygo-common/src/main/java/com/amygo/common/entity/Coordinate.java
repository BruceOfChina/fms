package com.amygo.common.entity;

public class Coordinate {
	private Double lat;
	
	private Double lon;
	
	private Double speed;
	
	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Coordinate(Double lat, Double lon,Double speed) {
		super();
		this.lat = lat;
		this.lon = lon;
		this.speed = speed;
	}

	public Coordinate(Double lat, Double lon) {
		super();
		this.lat = lat;
		this.lon = lon;
	}

	public Coordinate() {
		super();
	}
	
}
