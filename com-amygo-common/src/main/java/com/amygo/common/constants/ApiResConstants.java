package com.amygo.common.constants;

public class ApiResConstants {
	
	public static final String VERIFY_CODE_EXPIRE = "E0001";
	
	public static final String VERIFY_CODE_ERROR  = "E0002";
	
	public static final String USER_NOT_FOUND     = "E0003";
	
	public static final String USERNAME_PASSWORD_WRONG     = "U0001";
	
	public static final String CAR_IN_USE         = "C0001";
	
	public static final String ORDER_IN_PROCESS   = "O0001";
	
	public static final String TELEPHONE_REGISTERED   = "W0001";
	
	public static final String TELEPHONE_NOT_REGISTER   = "W0002";
	
	public static final String TOKEN_INVALID = "10000";
	
	public static final String NOT_IN_OPERATION_SCOPE = "10001";
	
	public static final String NOT_IN_OPERATION_TIME  = "10002";
	
	public static final String CARS_TOO_FAR  = "10003";
	
	public static final String ORDER_NOT_FINISH = "10004";
	
	public static final String ORDER_NOT_IN_PROCESS   = "O0002";
	
}
