package com.amygo.common.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseLogger
{
	protected Logger log = LoggerFactory.getLogger(this.getClass());
}
