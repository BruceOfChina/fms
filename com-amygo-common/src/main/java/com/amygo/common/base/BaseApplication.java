package com.amygo.common.base;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.amygo.common.Constants;

//@SpringBootApplication
public abstract class BaseApplication{
	// private static final Logger log =
	// LoggerFactory.getLogger(CServerApplication.class);

	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Environment env;

	/**
	 * Initializes WebApplication.
	 * <p/>
	 * Spring profiles can be configured with a program arguments
	 * --spring.profiles.active=your-active-profile
	 * <p/>
	 * <p>
	 * You can find more information on how profiles work with JHipster on
	 * <a href="http://jhipster.github.io/profiles.html">http://jhipster.github.
	 * io/profiles.html</a>.
	 * </p>
	 */
	@PostConstruct
	public void initApplication() throws IOException {
		if (env.getActiveProfiles().length == 0) {
			log.warn("No Spring profile configured, running with default configuration");
		} else {
			log.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
			Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
			if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT)
					&& activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)) {
				log.error("You have misconfigured your application! "
						+ "It should not run with both the 'dev' and 'prod' profiles at the same time.");
			}
			if (activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)
					&& activeProfiles.contains(Constants.SPRING_PROFILE_FAST)) {
				log.error("You have misconfigured your application! "
						+ "It should not run with both the 'prod' and 'fast' profiles at the same time.");
			}
			if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT)
					&& activeProfiles.contains(Constants.SPRING_PROFILE_CLOUD)) {
				log.error("You have misconfigured your application! "
						+ "It should not run with both the 'dev' and 'cloud' profiles at the same time.");
			}
		}
	}

	protected static void start(SpringApplication app, SimpleCommandLinePropertySource source, Logger log,
			String[] args) throws UnknownHostException {
		Environment env = app.run(args).getEnvironment();
		addDefaultProfile(app, source, env);
		log.info(
				"Access URLs:\n----------------------------------------------------------\n\t"
						+ "Local: \t\thttp://localhost:{}\n\t"
						+ "External: \thttp://{}:{}\n----------------------------------------------------------",
				env.getProperty("server.port"), InetAddress.getLocalHost().getHostAddress(),
				env.getProperty("server.port"));
	}

	/**
	 * If no profile has been configured, set by default the "dev" profile.
	 */
	private static void addDefaultProfile(SpringApplication app, SimpleCommandLinePropertySource source, Environment env) {
		if (!source.containsProperty("spring.profiles.active") && !env.containsProperty("spring.profiles.active")
				&& System.getProperty("spring.profiles.active") == null) {
			// System.getProperties().containsKey("spring.profiles.active")
			app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT);
		}
	}
}
