package com.amygo.common.enums;

public enum OrderStatusEnum {
	ORDER_CREATE("已预约订单", 1), 
	ORDER_IN_PROCESS("进行中订单", 2), // 订单行程开始后，订单更新为该状态
	ORDER_CANCEL("已取消订单", 3), 
	ORDER_TO_PAY("待支付订单", 4),
	ORDER_TO_EVALUATE("待评价订单", 5),
	ORDER_FINISHED("已完成订单", 6),
	ORDER_CANCEL_TO_PAY("取消后待支付订单", 7),
	ORDER_CANCEL_PAID("取消后已支付订单", 8),
	ORDER_WAIT_TO_START("到达起点等待启动的订单", 9);// 车辆到达起点后，更新为该状态
	private String name;
	
	private Integer status;

	private OrderStatusEnum(String name, Integer status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
