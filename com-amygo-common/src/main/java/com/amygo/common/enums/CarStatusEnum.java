package com.amygo.common.enums;

public enum CarStatusEnum {
	CAR_TO_USE("待使用", 1), 
	CAR_IN_USE("使用中", 2), 
	CAR_IN_MAINTAINANCE("维护中", 3);
	
	private String name;
	
	private Integer status;

	private CarStatusEnum(String name, Integer status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
