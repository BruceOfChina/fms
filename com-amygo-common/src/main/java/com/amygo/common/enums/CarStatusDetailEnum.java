package com.amygo.common.enums;

public enum CarStatusDetailEnum {
	CAR_WAIT_TO_ORDER("等待预约", 0), //新增的状态
	CAR_TO_START_POINT("预约去上车点", 1), 
	CAR_ARRIVE_START_POINT("到达上车点", 2), 
	CAR_START_TO_END("行驶中", 3),
	CAR_ARRIVE_END_POINT("到达目的地", 4),
	CAR_PASSENGER_GET_ON("乘客已上车", 5);
	
	private String name;
	
	private Integer status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	private CarStatusDetailEnum(String name, Integer status) {
		this.name = name;
		this.status = status;
	}
}
