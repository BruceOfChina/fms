package com.amygo.common.enums;

public enum OrderBehaviorEnum {
	ORDER_MODIFY_DESTINATION("更改目的地"),
	ORDER_ADD_STATION("增加途径站点"),
	ORDER_PULL_OVER("靠边停车"),
	ORDER_RESUME("重新开始"),
	ORDER_TRANSFER("用户换车");
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private OrderBehaviorEnum(String name) {
		this.name = name;
	}
	
}
