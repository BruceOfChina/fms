package com.amygo.common;

public class BaseResult {
	
	private Integer resultCode;
	
	private String messageCode;
	
	private String message;
	
	private Object data;
	
	public static int ERROR = -1;
	
	public static int SUCCESS = 0;
	
	public static int WARN = 1;
	
	public Integer getResultCode() {
		return resultCode;
	}

	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public BaseResult(Integer resultCode, String messageCode, String message, Object data) {
		super();
		this.resultCode = resultCode;
		this.messageCode = messageCode;
		this.message = message;
		this.data = data;
	}

	public BaseResult() {
		super();
		this.resultCode = BaseResult.SUCCESS;//默认返回值成功
	}

	public static BaseResult DEFAULT_SUCCESS = new BaseResult(0,"","",null);
	
	public static BaseResult DEFAULT_FAILURE = new BaseResult(-1,"","",null);
}
