package com.amygo.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amygo.common.Constants;

import sun.misc.BASE64Decoder;

/**
 * copy from app-server Created by on 15/11/25.
 */
public class SecurityUtils {

	private static final Logger log = LoggerFactory.getLogger(SecurityUtils.class);

	public static final String ALGORITHM_MD5 = "MD5";
	public static final String ALGORITHM_3DES = "DESede";
	private static final String ENCODING_UTF8 = "UTF-8";
	private static final String AES_CBC_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String AES_ECB_ALGOTITHM = "AES/ECB/PKCS5Padding";
	public static final String ALGORITHM_SHA512 = "SHA-512";

	private SecurityUtils() {
	}

	/**
	 * 对上送到服务端请求报文加密
	 * 
	 * @param key
	 * @param content
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws Exception
	 * @throws UnsupportedEncodingException
	 */
	public static String cryption(String key, String content)
			throws UnsupportedEncodingException, NoSuchAlgorithmException {
		return byteArray2HexString(encryptMode(hexString2ByteArray(Md5(key)), content.getBytes()));
	}

	/**
	 * MD5签名算法 返回 Hex string
	 * 
	 * @param plainText
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String Md5(String plainText) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if (plainText == null) {
			return null;
		}
		MessageDigest md = MessageDigest.getInstance(ALGORITHM_MD5);
		md.update(plainText.getBytes());
		byte[] b = md.digest();
		int i;
		StringBuilder buf = new StringBuilder("");
		for (int offset = 0; offset < b.length; offset++) {
			i = b[offset];
			if (i < 0)
				i += 256;
			if (i < 16)
				buf.append("0");
			buf.append(Integer.toHexString(i));
		}
		return buf.toString().toUpperCase();

	}

	public static String SHA512(String plainText) throws Exception {
		MessageDigest md = MessageDigest.getInstance(ALGORITHM_SHA512);
		byte[] byteArr = md.digest(plainText.getBytes(ENCODING_UTF8));
		return byteArray2HexString(byteArr);
	}

	// ---------------------------------------------------------- private
	// methods ------------------------------------------------------------
	private static byte[] encryptMode(byte[] keybyte, byte[] src) {
		try {
			// 生成密钥
			if (keybyte.length < 24) {
				keybyte = SecurityUtils.convertKey(keybyte);
			}
			SecretKey deskey = new SecretKeySpec(keybyte, ALGORITHM_3DES);
			// 加密
			Cipher c1 = Cipher.getInstance(ALGORITHM_3DES);
			c1.init(Cipher.ENCRYPT_MODE, deskey);
			return c1.doFinal(src);
		} catch (java.security.NoSuchAlgorithmException e1) {
			log.error("NoSuchAlgorithmException: ", e1);
		} catch (javax.crypto.NoSuchPaddingException e2) {
			log.error("NoSuchPaddingException: ", e2);
		} catch (java.lang.Exception e3) {
			log.error("Exception: ", e3);
		}
		return null;
	}

	// keybyte为加密密钥，长度为24字节
	// src为加密后的缓冲区

	/*
	 * private static byte[] decryptMode(byte[] keybyte, byte[] src) { try { //
	 * System.out.println("key length: " + keybyte.length); if (keybyte.length <
	 * 24) { keybyte = SecurityUtils.convertKey(keybyte); } SecretKey deskey =
	 * new SecretKeySpec(keybyte, ALGORITHM_3DES); Cipher c1 =
	 * Cipher.getInstance("DESede/ECB/NoPadding"); c1.init(Cipher.DECRYPT_MODE,
	 * deskey); return c1.doFinal(src); } catch
	 * (java.security.NoSuchAlgorithmException e1) { log.error(
	 * "NoSuchAlgorithmException: ", e1); } catch
	 * (javax.crypto.NoSuchPaddingException e2) { log.error(
	 * "NoSuchPaddingException: ", e2); } catch (java.lang.Exception e3) {
	 * log.error("Exception: ", e3); } return null; }
	 */

	// 将16字节的密钥转换成24字节
	private static byte[] convertKey(byte[] srcKey) {
		byte[] destKey = null;
		byte[] keyFirst = new byte[8];
		ByteBuffer buffer = ByteBuffer.wrap(srcKey);
		buffer.get(keyFirst);
		buffer.clear();
		buffer = ByteBuffer.allocate(24);
		buffer.put(srcKey);
		buffer.put(keyFirst);
		buffer.flip();
		destKey = buffer.array();
		buffer.clear();
		return destKey;
	}

	/*
	 * private static String byteArray2HexString(byte[] arr) { StringBuilder sbd
	 * = new StringBuilder(); for (byte b : arr) { String tmp =
	 * Integer.toHexString(0xFF & b); if (tmp.length() < 2) tmp = "0" + tmp;
	 * sbd.append(tmp); } return sbd.toString(); }
	 */

	private static String byteArray2HexString(byte[] byteArr) {
		if (byteArr == null || byteArr.length == 0) {
			return null;
		}
		StringBuilder sbd = new StringBuilder("");
		for (byte b : byteArr) {
			String tmp = Integer.toHexString(0xFF & b);
			if (tmp.length() < 2) {
				sbd.append(0);
			}
			sbd.append(tmp);
		}
		return sbd.toString();
	}

	private static byte[] hexString2ByteArray(String hexStr) {
		if (hexStr == null)
			return null;
		if (hexStr.length() % 2 != 0) {
			return null;
		}
		byte[] data = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			char hc = hexStr.charAt(2 * i);
			char lc = hexStr.charAt(2 * i + 1);
			byte hb = hexChar2Byte(hc);
			byte lb = hexChar2Byte(lc);
			if (hb < 0 || lb < 0) {
				return null;
			}
			int n = hb << 4;
			data[i] = (byte) (n + lb);
		}
		return data;
	}

	private static byte hexChar2Byte(char c) {
		if (c >= '0' && c <= '9')
			return (byte) (c - '0');
		if (c >= 'a' && c <= 'f')
			return (byte) (c - 'a' + 10);
		if (c >= 'A' && c <= 'F')
			return (byte) (c - 'A' + 10);
		return -1;
	}

	// 将 s 进行 BASE64 编码
	public static String getBASE64(String s) {
		if (s == null)
			return null;
		return (new sun.misc.BASE64Encoder()).encode(s.getBytes());
	}

	// 将 BASE64 编码的字符串 s 进行解码
	public static String getFromBASE64(String s) {
		if (s == null)
			return null;
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			byte[] b = decoder.decodeBuffer(s);
			return new String(b);
		} catch (Exception e) {
			return null;
		}
	}

	public static byte[] AES_encode(byte[] srcData) {
		byte[] iv = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
		return AES_cbc_encrypt(srcData, iv, iv);
	}

	// CBC模式加密
	public static byte[] AES_cbc_encrypt(byte[] srcData, byte[] key, byte[] iv) {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		byte[] encData = null;
		try {
			cipher = Cipher.getInstance(AES_CBC_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
			encData = cipher.doFinal(srcData);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return encData;
	}

	// CBC模式解密
	public static byte[] AES_cbc_decrypt(byte[] encData, byte[] key, byte[] iv) {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		byte[] decbbdt = null;
		try {
			cipher = Cipher.getInstance(AES_CBC_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
			decbbdt = cipher.doFinal(encData);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return decbbdt;
	}

	// ECB加密
	public static byte[] AES_ecb_encrypt(byte[] srcData, byte[] key) {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		byte[] encData = null;
		try {
			cipher = Cipher.getInstance(AES_ECB_ALGOTITHM);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			encData = cipher.doFinal(srcData);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return encData;
	}

	// ECB解密
	public static byte[] AES_ecb_decrypt(byte[] encData, byte[] key) {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		byte[] decbbdt = null;
		try {
			cipher = Cipher.getInstance(AES_ECB_ALGOTITHM);
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			decbbdt = cipher.doFinal(encData);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return decbbdt;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
//		byte[] rootKey = { 0x41, 0x4D, 0x73, 0x6C, 0x66, 0x71, 0x46, 0x34, 0x42, 0x4B, 0x56, 0x50, 0x78, 0x50, 0x38,
//				0x54 };
//		byte[] initVector = { 0x39, 0x48, 0x72, 0x30, 0x72, 0x7A, 0x52, 0x4C, 0x73, 0x71, 0x44, 0x58, 0x6B, 0x4E, 0x37,
//				0x44 };
//		String str = "47 2D D1 E8 36 4D 3C AE 83 0B 31 1F FD E6 41 C6 48 F8 D5 48 F3 B6 69 4C 78 78 63 6D FC 37 71 75";
//		byte[] encoderesult = AES_ecb_encrypt(TextUtils.hexStringToByte(str), rootKey);
//		byte[] initVectorResult = AES_ecb_decrypt(encoderesult, rootKey);
		String hexStr = "1F26676202FAE6C3F653C464584D7ECE";
		String skey = "61bd51571aafed30ae1a8279f752ba84";
		String sIv = "6E22088DC435D86E9F80EC838F6ED392";
		String imsi= "343630303930353030373734303834";
		byte[] bytes = TextUtils.hexStringToByte(hexStr);
		String str = "48 3B 05 88 1B 6C 86 44 0B 79 A2 31 DA 6D 41 32 61 9A 8F 10 3A DD BF B4 DB 6C B7 B3 A3 75 B3 D4 C5 26 10 5E 42 1A DE 26 D8 61 B2 B3 42 8F 47 76";
		byte[] length1ToEncrypt = AES_cbc_decrypt(TextUtils.hexStringToByte(str),"gajklgghjaklghjg".getBytes(),"gajklgghjaklghjg".getBytes());
		System.out.println("length:"+TextUtils.toHexStringWithSpace(length1ToEncrypt));
//		byte[] cbcDecode  = AES_cbc_decrypt(bytes,TextUtils.hexStringToByte(skey),TextUtils.hexStringToByte(sIv));
//		System.out.println(TextUtils.toHexString(ecbDecode));
//		System.out.println(TextUtils.toHexString(cbcDecode));
//		int x = 16;
//		System.out.println(Math.ceil(x/16.));
//		System.out.println(length16(str.length()));

	}
	/**
	 * 如果x是16的倍数，返回16*(x+1)
	 * 如果x不是16的倍数，先将x/16向上取整(1/16结果为1),然后再乘以16;
	 * @param x 加密前的长度
	 * @return 加密后的长度
	 */
	public static int length16(int x) {
		int result = 0;
		if(x%16==0) {
			result = 16*(x+1);
		}else {
			result = new Double((Math.ceil(x/16.))).intValue()*16;
		}
		return result;
	}
}
