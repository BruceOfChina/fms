package com.amygo.common.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;


public class BitsHelper {

	public static byte[] intToByteArray(int a) {
		return new byte[] { (byte) ((a >> 24) & 0xFF), (byte) ((a >> 16) & 0xFF), (byte) ((a >> 8) & 0xFF),
				(byte) (a & 0xFF) };
	}

	public static byte[] getByteFromBinaryStr(String bits) {
		return StringUtils.isBlank(bits) ? null : getByteL(bitToInt(bits));
	}
	public static byte[] BinaryStrToByte(String bits) {
		return StringUtils.isBlank(bits) ? null : ShortToByteArray(bitToInt(bits));
	}
	/**
	 * long转字节数组
	 * 
	 * @param num
	 * @return
	 */
	public static byte[] long2Bytes(long num) {
		byte[] byteNum = new byte[8];
		for (int ix = 0; ix < 8; ++ix) {
			int offset = 64 - (ix + 1) * 8;
			byteNum[ix] = (byte) ((num >> offset) & 0xff);
		}
		return byteNum;
	}

	private BitsHelper() {
	}

	public static long getUInt32L(byte[] b, int off) {
		return (long) ((b[off] & 0xFF)) + ((b[off + 1] & 0xFF) << 8) + ((b[off + 2] & 0xFF) << 16)
				+ ((b[off + 3]) << 24) & 0xffffffffL;
	}

	public static long getUInt32L(byte[] b) {
		return getUInt32L(b, 0);
	}

	public static long getUInt32B(byte[] b, int off) {
		return (long) ((b[off + 3] & 0xFF)) + ((b[off + 2] & 0xFF) << 8) + ((b[off + 1] & 0xFF) << 16)
				+ ((b[off]) << 24) & 0xffffffffL;
	}

	public static long getUInt32B(byte[] b) {
		return getUInt32B(b, 0);
	}

	/**
	 * Reads four bytes at the given index, composing them into a int value
	 * according to the current byte order.
	 *
	 * @param bytebuffer
	 * @param position
	 * @return long
	 */
	public static long getUInt32(ByteBuffer bb, int position) {
		return ((long) bb.getInt(position) & 0xffffffffL);
	}

	/**
	 * Reads the next four bytes at this buffer's current position, composing
	 * them into an int value according to the current byte order, and then
	 * increments the position by four.
	 *
	 * @param bytebuffer
	 * @return long
	 */
	public static long getUInt32(ByteBuffer bb) {
		return ((long) bb.getInt() & 0xffffffffL);
	}

	public static int getUInt16L(byte[] b, int off) {
		return (int) ((b[off] & 0xFF)) + ((b[off + 1]) << 8) & 0xffff;
	}

	public static int getUInt16L(byte[] b) {
		return getUInt16L(b, 0);
	}

	public static int getUInt16B(byte[] b, int off) {
		return (int) ((b[off + 1] & 0xFF)) + ((b[off]) << 8) & 0xffff;
	}

	public static int getUInt16B(byte[] b) {
		return getUInt16B(b, 0);
	}

	public static int getUInt16(ByteBuffer bb, int position) {
		return ((int) bb.getShort(position) & 0xffff);
	}

	public static int getUInt16(ByteBuffer bb) {
		return ((int) bb.getShort() & 0xffff);
	}

	public static short getUInt8(byte[] b, int off) {
		return (short) (b[off] & 0xFF);
	}

	public static short getUInt8(byte[] b) {
		return getUInt8(b, 0);
	}

	public static int varByteToUIntL(int count, byte[] data, int offset) {
		int val = 0;

		assert count <= 4;
		for (int i = 0; i < count; i++) {
			val |= (int) (data[offset + i] & 0xFF) << (8 * i);
		}
		return (int) val;
	}

	public static short getUInt8(byte b) {
		return (short) (b & 0xFF);
	}

	public static short getUInt8(ByteBuffer bb, int position) {
		return (short) (bb.get(position) & 0xff);
	}

	public static short getUInt8(ByteBuffer bb) {
		return (short) (bb.get() & 0xff);
	}

	public static byte[] getByteL(int i) {
		return new byte[] { (byte) (i & 0xFF), (byte) ((i >> 8) & 0xFF), (byte) ((i >> 16) & 0xFF),
				(byte) ((i >> 24) & 0xFF) };
	}
	
    /*将int转为低字节在后，高字节在前的byte数组 
    b[0] = 11111111(0xff) & 01100001 
    b[1] = 11111111(0xff) & 00000000 
    b[2] = 11111111(0xff) & 00000000 
    b[3] = 11111111(0xff) & 00000000 
    */  
    public static byte[] IntegerToByteArray(int value)     
    {     
        byte[] src = new byte[2];    
        src[0] = (byte) ((value>>8)&0xFF);      
        src[1] = (byte) (value & 0xFF);         
        return src;    
    }
    
    public static byte[] LongToByteArray(long value)     
    {     
    	byte[] src = new byte[4];    
        src[0] = (byte) ((value>>24) & 0xFF);    
        src[1] = (byte) ((value>>16)& 0xFF);    
        src[2] = (byte) ((value>>8)&0xFF);      
        src[3] = (byte) (value & 0xFF);         
        return src;    
    }
    
    public static byte[] ShortToByteArray(int value)     
    {     
    	 byte[] src = new byte[1];    
         src[0] = (byte) (value & 0xFF);         
         return src;       
    }
    
	public static byte[] getByteL(short i) {
		return new byte[] { (byte) (i & 0xFF), (byte) ((i >> 8) & 0xFF) };
	}

	public static char[] getCharArray(byte[] b) {
		if (b == null) {
			return null;
		}
		return getCharArray(b, 0, b.length);
	}

	public static char[] getCharArray(byte[] b, int offset, int length) {
		if (b == null) {
			return null;
		}
		/*
		 * if (b.length > offset + length) { return null; }
		 */

		char[] c = new char[length];
		for (int i = 0; i < length; i++) {
			c[i] = (char) b[offset + i];
		}

		return c;
	}

	public static float getFloat32B(byte[] b, int index) {
		int l;
		l = b[index + 3];
		l &= 0xff;
		l |= ((long) b[index + 2] << 8);
		l &= 0xffff;
		l |= ((long) b[index + 1] << 16);
		l &= 0xffffff;
		l |= ((long) b[index + 0] << 24);
		return Float.intBitsToFloat(l);
	}

	/**
	 * 把byte数组转化成2进制字符串
	 *
	 * @param bArr
	 * @return
	 */
	public static String getBinaryStrFromByteArr(byte[] bArr) {
		String result = "";
		for (byte b : bArr) {
			result += getBinaryStrFromByte(b);
		}
		return result;
	}

	/**
	 * 把byte转化成2进制字符串
	 *
	 * @param b
	 * @return
	 */
	public static String getBinaryStrFromByte(byte b) {
		String result = "";
		byte a = b;
		;
		for (int i = 0; i < 8; i++) {
			byte c = a;
			a = (byte) (a >> 1);// 每移一位如同将10进制数除以2并去掉余数。
			a = (byte) (a << 1);
			if (a == c) {
				result = "0" + result;
			} else {
				result = "1" + result;
			}
			a = (byte) (a >> 1);
		}
		return result;
	}

	/**
	 * 把byte转化成2进制字符串
	 *
	 * @param b
	 * @return
	 */
	public static String getBinaryStrFromByte2(byte b) {
		String result = "";
		byte a = b;
		;
		for (int i = 0; i < 8; i++) {
			result = (a % 2) + result;
			a = (byte) (a >> 1);
		}
		return result;
	}

	/**
	 * 把byte转化成2进制字符串
	 *
	 * @param b
	 * @return
	 */
	public static String getBinaryStrFromByte3(byte b) {
		String result = "";
		byte a = b;
		;
		for (int i = 0; i < 8; i++) {
			result = (a % 2) + result;
			a = (byte) (a / 2);
		}
		return result;
	}

	/**
	 * 把byte转为字符串的bit
	 */
	public static String byteToBit(byte b) {
		return "" + (byte) ((b >> 7) & 0x1) + (byte) ((b >> 6) & 0x1) + (byte) ((b >> 5) & 0x1)
				+ (byte) ((b >> 4) & 0x1) + (byte) ((b >> 3) & 0x1) + (byte) ((b >> 2) & 0x1) + (byte) ((b >> 1) & 0x1)
				+ (byte) ((b >> 0) & 0x1);
	}

	/**
	 *
	 * 把字节数组转为字符串的bit
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytesToBit(byte[] bytes) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			String s = byteToBit(b);
			stringBuilder.append(s);
		}
		return stringBuilder.toString();
	}

	/**
	 * 把字节数组转为字符串的bit
	 *
	 * @param src
	 * @param startIndex
	 * @param offset
	 * @return
	 */
	public static String bytesToBit(byte[] src, int startIndex, int offset) {
		byte[] bytes = new byte[offset];
		System.arraycopy(src, startIndex, bytes, 0, offset);
		return bytesToBit(bytes);
	}

	/**
	 * bite 转 int
	 *
	 * @param bit
	 * @return
	 */
	public static int bitToInt(String bit) {
		int re, len;
		if (null == bit) {
			return 0;
		}
		len = bit.length();
		if (len == 5 || len == 6) {
			bit = padLeft(bit, 8);
			len = bit.length();
		}
		if (len == 8) {// 8 bit处理
			if (bit.charAt(0) == '0') {// 正数
				re = Integer.parseInt(bit, 2);
			} else {// 负数
				re = Integer.parseInt(bit, 2) - 256;
			}
		} else {// 4 bit处理
			re = Integer.parseInt(bit, 2);
		}
		return re;
	}

	/**
	 * fill in String with certain length
	 *
	 * @param s
	 * @param length
	 * @return
	 */
	public static String padLeft(String s, int length) {
		byte[] bs = new byte[length];
		byte[] ss = s.getBytes();
		Arrays.fill(bs, (byte) (48 & 0xff));
		System.arraycopy(ss, 0, bs, length - ss.length, ss.length);
		return new String(bs);
	}

	/**
	 * @功能: BCD码转为10进制串(阿拉伯数据)
	 * @参数: BCD码
	 * @结果: 10进制串
	 */
	public static String bcd2Str(byte[] bytes) {
		StringBuffer temp = new StringBuffer(bytes.length * 2);
		for (int i = 0; i < bytes.length; i++) {
			temp.append((byte) ((bytes[i] & 0xf0) >>> 4));
			temp.append((byte) (bytes[i] & 0x0f));
		}
		return temp.toString().substring(0, 1).equalsIgnoreCase("0") ? temp.toString().substring(1) : temp.toString();
	}

	public static String bcd2Str(byte[] bytes, int offset, int length) {
		byte[] bs = new byte[length];
		System.arraycopy(bytes, offset, bs, 0, length);
		return bcd2Str(bs);
	}

	/**
	 * @功能: 10进制串转为BCD码
	 * @参数: 10进制串
	 * @结果: BCD码
	 */
	public static byte[] str2Bcd(String asc) {
		int len = asc.length();
		int mod = len % 2;
		if (mod != 0) {
			asc = "0" + asc;
			len = asc.length();
		}
		byte abt[] = new byte[len];
		if (len >= 2) {
			len = len / 2;
		}
		byte bbt[] = new byte[len];
		abt = asc.getBytes();
		int j, k;
		for (int p = 0; p < asc.length() / 2; p++) {
			if ((abt[2 * p] >= '0') && (abt[2 * p] <= '9')) {
				j = abt[2 * p] - '0';
			} else if ((abt[2 * p] >= 'a') && (abt[2 * p] <= 'z')) {
				j = abt[2 * p] - 'a' + 0x0a;
			} else {
				j = abt[2 * p] - 'A' + 0x0a;
			}
			if ((abt[2 * p + 1] >= '0') && (abt[2 * p + 1] <= '9')) {
				k = abt[2 * p + 1] - '0';
			} else if ((abt[2 * p + 1] >= 'a') && (abt[2 * p + 1] <= 'z')) {
				k = abt[2 * p + 1] - 'a' + 0x0a;
			} else {
				k = abt[2 * p + 1] - 'A' + 0x0a;
			}
			int a = (j << 4) + k;
			byte b = (byte) a;
			bbt[p] = b;
		}
		return bbt;
	}

	public static byte[] arrayBitCut(byte[] data, int offset, int len) {
		byte[] result = new byte[len % 8 == 0 ? len / 8 : len / 8 + 1];
		if (offset + len > data.length * 8)
			return null;
		byte[] temp = data;
		byte[] tempResult = new byte[(len + offset % 8) % 8 == 0 ? (len + offset % 8) / 8 : (len + offset % 8) / 8 + 1];
		System.arraycopy(temp, offset / 8, tempResult, 0,
				(len + offset % 8) % 8 == 0 ? (len + offset % 8) / 8 : (len + offset % 8) / 8 + 1);
		int j = offset % 8;
		tempResult[0] = (byte) (tempResult[0] << j);
		tempResult[0] = (byte) (tempResult[0] >> j);
		if ((len + offset % 8) % 8 != 0) {
			int k = (len + offset % 8) / 8 + 1, l = (len + offset % 8) % 8;
			for (int m = k; m > 1; m--)
				tempResult[m - 1] = (byte) ((tempResult[m - 1] & 0xff) >> (8 - l) | (tempResult[m - 2] & 0xff) << l);
			tempResult[0] = (byte) (tempResult[0] >> (8 - l));
		}
		System.arraycopy(tempResult, tempResult.length - result.length, result, 0, result.length);
		return result;
	}
	public static byte[] subBytes(byte[] src, int begin, int count) {  
	    byte[] bs = new byte[count];  
	    System.arraycopy(src, begin, bs, 0, count);  
	    return bs;  
	}  
 
	public static void main(String[] args) {
		String str = "10";
		System.out.println(str2Bcd(str));
		System.out.println(bcd2Str(str2Bcd(str)));
	}

}
