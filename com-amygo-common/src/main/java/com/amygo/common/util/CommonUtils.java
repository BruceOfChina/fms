package com.amygo.common.util;

import org.springframework.beans.factory.annotation.Autowired;

import com.amygo.common.base.BaseLogger;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtils extends BaseLogger {

	@Autowired
	private ObjectMapper jsonMapper;

	public String toJson(Object obj) {
		try {
			return jsonMapper.writeValueAsString(obj);
		} catch (Exception e) {
			log.error("Fail to convert to json", e);
			return null;
		}
	}

	public <T> T fromJson(String content, Class<T> valueType) {
		try {
			return jsonMapper.readValue(content, valueType);
		} catch (Exception e) {
			log.error("Fail to convert from json to object", e);
			return null;
		}
	}

	public <T> T fromJson(String content, Class<?> parametrized, Class<?> parametersFor, Class<?>... parameterClasses) {
		try {
			return jsonMapper.readValue(content, getCollectionType(parametrized, parametersFor, parameterClasses));
		} catch (Exception e) {
			log.error("Fail to convert from json to object", e);
			return null;
		}
	}

	public JavaType getCollectionType(Class<?> parametrized, Class<?> parametersFor, Class<?>... parameterClasses) {
		return jsonMapper.getTypeFactory().constructParametrizedType(parametrized, parametersFor, parameterClasses);
	}
}
