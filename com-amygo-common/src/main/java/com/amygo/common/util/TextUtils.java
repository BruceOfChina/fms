package com.amygo.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public final class TextUtils {
	private final static Logger log = LoggerFactory.getLogger(TextUtils.class);

	private static char[] base64EncodeChars = new char[] { 'p', 'B', 'C', '5', 'E', 'V', 'r', 'H', 'I', 'J', '+', 'L',
			'8', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'D', 'F', 'W', 'X', 'Y', 'm', 'a', 'b', 'c', 'M', 'e', 'f', 'G',
			'h', 'i', 'j', 'A', 'l', '0', 'n', 'o', 'k', 'q', 'g', 's', 'u', 't', 'v', 'w', '3', 'y', 'z', 'Z', '1',
			'2', '9', '4', 'U', '6', '7', 'd', 'x', 'K', '/' };

	private static final char[] hex = "0123456789ABCDEFG".toCharArray();

	private TextUtils() {
	}

	public static String toHexStringWithSpace(byte[] bytes) {
		if (null == bytes || bytes.length == 0) {
			return null;
		}

		StringBuilder sb = new StringBuilder(bytes.length << 1);

		for (int i = 0; i < bytes.length; ++i) {
			sb.append(hex[(bytes[i] & 0xf0) >> 4]).append(hex[(bytes[i] & 0x0f)]).append(' ');
			;
		}

		return sb.substring(0, sb.length() - 1);
	}

	public static String toHexString(byte[] bytes) {
		if (null == bytes) {
			return null;
		}

		StringBuilder sb = new StringBuilder(bytes.length << 1);

		for (int i = 0; i < bytes.length; ++i) {
			sb.append(hex[(bytes[i] & 0xf0) >> 4]).append(hex[(bytes[i] & 0x0f)]);
			;
		}

		return sb.toString();
	}

	public static byte[] fromHexString(String str) {
		if (StringUtils.isEmpty(str)) {
			return new byte[] {};
		}

		str = str.replaceAll(" ", "").replaceAll("0x", "");
		if (str.length() % 2 == 1) {
			str = "0" + str;
		}
		byte[] res = new byte[str.length() / 2];
		char[] chs = str.toCharArray();
		for (int i = 0, c = 0; i < chs.length; i += 2, c++) {
			res[c] = (byte) (Integer.parseInt(new String(chs, i, 2), 16));
		}

		return res;

	}

	public static byte[] fromDecimalString(String str) {
		if (StringUtils.isEmpty(str)) {
			return new byte[] {};
		}

		str = str.replaceAll(" ", "");
		if (str.length() % 2 == 1) {
			str = "0" + str;
		}
		byte[] res = new byte[str.length() / 2];
		char[] chs = str.toCharArray();
		for (int i = 0, c = 0; i < chs.length; i += 2, c++) {
			res[c] = (byte) (Integer.parseInt(new String(chs, i, 2)));
		}

		return res;

	}

	public static String removeFirstSlash(String str) {
		if (StringUtils.isEmpty(str)) {
			return str;
		}

		if (str.startsWith(File.separator)) {
			return str.substring(1);
		}

		return str;
	}

	public static String removePoint(String str) {
		if (StringUtils.isEmpty(str)) {
			return str;
		}
		str = str.replaceAll("\\.", "");
		return str;
	}

	public static boolean parseBoolean(String str) {
		boolean ret = true;
		try {
			ret = Boolean.parseBoolean(str);
		} catch (Exception e) {

		}
		return ret;
	}

	public static int parseInt(String str) {
		int ret = 0;
		if (str == null || str.isEmpty()) {
			log.warn("Cannot parse a empty value to int.");
			return ret;
		}
		try {
			if (str.startsWith("0x")) {
				return bytes2Int(TextUtils.fromHexString(str.replaceFirst("0x", "")));
			} else {
				return Integer.parseInt(str);
			}
			// ret = Integer.parseInt(str.replaceFirst("0x", ""),
			// getRedix(str));
		} catch (Exception e) {
			log.error("parse int failure: ", e);
		}
		return ret;
	}

	public static long parseLong(String str) {
		long ret = 0;
		try {
			if (str.startsWith("0x")) {
				return bytes2Long(TextUtils.fromHexString(str.replaceFirst("0x", "")));
			} else {
				return Long.parseLong(str);
			}
			// ret = Long.parseLong(str.replaceFirst("0x", ""), getRedix(str));
		} catch (Exception e) {
			log.error("parse long failure: ", e);
		}
		return ret;
	}

	public static float parseFloat(String str) {
		float ret = 0f;
		try {
			ret = Float.parseFloat(str);
		} catch (Exception e) {

		}
		return ret;
	}

	public static double parseDouble(String str) {
		double ret = 0d;
		try {
			ret = Double.parseDouble(str);
		} catch (Exception e) {

		}
		return ret;
	}

	public static int bytes2Int(byte[] byteNum) {
		byte[] byteNum4 = new byte[] { 0, 0, 0, 0 };
		System.arraycopy(byteNum, 0, byteNum4, byteNum4.length - byteNum.length, byteNum.length);
		int num = 0;
		for (int ix = 0; ix < 4; ++ix) {
			num <<= 8;
			num |= (byteNum4[ix] & 0xff);
		}
		return num;
	}

	public static long bytes2Long(byte[] byteNum) {
		byte[] byteNum8 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		System.arraycopy(byteNum, 0, byteNum8, byteNum8.length - byteNum.length, byteNum.length);
		long num = 0;
		for (int ix = 0; ix < 8; ++ix) {
			num <<= 8;
			num |= (byteNum8[ix] & 0xff);
		}
		return num;
	}

	public static String[] split(String str, String regex) {
		if (str == null) {
			return null;
		}
		return str.split(regex);
	}

	public static byte[] hmacSHA1Encrypt(byte[] encryptText, String encryptKey) throws Exception {
		byte[] data = encryptKey.getBytes("UTF-8");
		// generate the key
		SecretKey secretKey = new SecretKeySpec(data, "HmacSHA1");
		// generate a Mac algorithm and object
		Mac mac = Mac.getInstance("HmacSHA1");
		// init Mac object with key
		mac.init(secretKey);
		// finish MAC
		return mac.doFinal(encryptText);
	}

	public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
		byte[] byte_3 = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
		System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
		return byte_3;
	}

	// The Base64 mapping list is defined according to SPEC
	public static String encodeBase64(byte[] data) {
		StringBuffer sb = new StringBuffer();
		int len = data.length;
		int i = 0;
		int b1, b2, b3;
		while (i < len) {
			b1 = data[i++] & 0xff;
			if (i == len) {
				sb.append(base64EncodeChars[b1 >>> 2]);
				sb.append(base64EncodeChars[(b1 & 0x3) << 4]);
				sb.append("==");
				break;
			}
			b2 = data[i++] & 0xff;
			if (i == len) {
				sb.append(base64EncodeChars[b1 >>> 2]);
				sb.append(base64EncodeChars[((b1 & 0x03) << 4) | ((b2 & 0xf0) >>> 4)]);
				sb.append(base64EncodeChars[(b2 & 0x0f) << 2]);
				sb.append("=");
				break;
			}
			b3 = data[i++] & 0xff;
			sb.append(base64EncodeChars[b1 >>> 2]);
			sb.append(base64EncodeChars[((b1 & 0x03) << 4) | ((b2 & 0xf0) >>> 4)]);
			sb.append(base64EncodeChars[((b2 & 0x0f) << 2) | ((b3 & 0xc0) >>> 6)]);
			sb.append(base64EncodeChars[b3 & 0x3f]);
		}
		return sb.toString();
	}

	// -------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 用于建立十六进制字符的输出的小写字符数组
	 */
	private static final char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
			'e', 'f' };
	/**
	 * 用于建立十六进制字符的输出的大写字符数组
	 */
	private static final char[] DIGITS_UPPER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	/**
	 * 将字节数组转换为十六进制字符数组
	 *
	 * @param data
	 *            byte[]
	 * @return 十六进制char[]
	 */
	public static char[] encodeHex(byte[] data) {
		return encodeHex(data, true);
	}

	/**
	 * 将字节数组转换为十六进制字符数组
	 *
	 * @param data
	 *            byte[]
	 * @param toLowerCase
	 *            <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
	 * @return 十六进制char[]
	 */
	public static char[] encodeHex(byte[] data, boolean toLowerCase) {
		return encodeHex(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
	}

	/**
	 * 将字节数组转换为十六进制字符数组
	 *
	 * @param data
	 *            byte[]
	 * @param toDigits
	 *            用于控制输出的char[]
	 * @return 十六进制char[]
	 */
	protected static char[] encodeHex(byte[] data, char[] toDigits) {
		int l = data.length;
		char[] out = new char[l << 1];
		// two characters form the hex value.
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
			out[j++] = toDigits[0x0F & data[i]];
		}
		return out;
	}

	/**
	 * 将字节数组转换为十六进制字符串
	 *
	 * @param data
	 *            byte[]
	 * @return 十六进制String
	 */
	public static String encodeHexStr(byte[] data) {
		return encodeHexStr(data, true);
	}

	/**
	 * 将字节数组转换为十六进制字符串
	 *
	 * @param data
	 *            byte[]
	 * @param toLowerCase
	 *            <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
	 * @return 十六进制String
	 */
	public static String encodeHexStr(byte[] data, boolean toLowerCase) {
		return encodeHexStr(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
	}

	/**
	 * 将字节数组转换为十六进制字符串
	 *
	 * @param data
	 *            byte[]
	 * @param toDigits
	 *            用于控制输出的char[]
	 * @return 十六进制String
	 */
	protected static String encodeHexStr(byte[] data, char[] toDigits) {
		return new String(encodeHex(data, toDigits));
	}

	/**
	 * 将十六进制字符数组转换为字节数组
	 *
	 * @param data
	 *            十六进制char[]
	 * @return byte[]
	 * @throws RuntimeException
	 *             如果源十六进制字符数组是一个奇怪的长度，将抛出运行时异常
	 */
	public static byte[] decodeHex(char[] data) {
		int len = data.length;
		if ((len & 0x01) != 0) {
			throw new RuntimeException("Odd number of characters.");
		}
		byte[] out = new byte[len >> 1];
		// two characters form the hex value.
		for (int i = 0, j = 0; j < len; i++) {
			int f = toDigit(data[j], j) << 4;
			j++;
			f = f | toDigit(data[j], j);
			j++;
			out[i] = (byte) (f & 0xFF);
		}
		return out;
	}

	/**
	 * 将十六进制字符转换成一个整数
	 *
	 * @param ch
	 *            十六进制char
	 * @param index
	 *            十六进制字符在字符数组中的位置
	 * @return 一个整数
	 * @throws RuntimeException
	 *             当ch不是一个合法的十六进制字符时，抛出运行时异常
	 */
	protected static int toDigit(char ch, int index) {
		int digit = Character.digit(ch, 16);
		if (digit == -1) {
			throw new RuntimeException("Illegal hexadecimal character " + ch + " at index " + index);
		}
		return digit;
	}

	// 十六进制字符串转换成为数组方法1：

	/**
	 * 把16进制字符串转换成字节数组
	 * 
	 * @param hexString
	 * @return byte[]
	 */
	public static byte[] hexStringToByte(String hex) {
		if (hex == null || hex.equals("")) {
			return null;
		}
		hex = hex.toUpperCase().replaceAll(" ", "").replaceAll("0X", "");
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] achar = hex.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
		}
		return result;
	}

	private static int toByte(char c) {
		byte b = (byte) "0123456789ABCDEF".indexOf(c);
		return b;
	}

	private static String string = "abcdefghijklmnopqrstuvwxyz";

	public static String getRandomString(int length) {
		StringBuffer sb = new StringBuffer();
		int len = string.length();
		for (int i = 0; i < length; i++) {
			sb.append(string.charAt(getRandom(len - 1)));
		}
		return sb.toString();
	}

	public static int getRandom(int count) {
	    return (int) Math.round(Math.random() * (count));
	}

	/** * 获取16进制随机数 * @param len * @return * @throws CoderException */ 
	public static String randomHexString(int len) {
		try {
			StringBuffer result = new StringBuffer();
			for (int i = 0; i < len; i++) {
				result.append(Integer.toHexString(new Random().nextInt(16)));
			}
			return result.toString().toUpperCase();
		} catch (Exception e) { // TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 16进制直接转换成为字符串(无需Unicode解码)
	 * @param hexStr
	 * @return
	 */
	public static String hexStr2Str(String hexStr) {
	    String str = "0123456789ABCDEF";
	    char[] hexs = hexStr.toCharArray();
	    byte[] bytes = new byte[hexStr.length() / 2];
	    int n;
	    for (int i = 0; i < bytes.length; i++) {
	        n = str.indexOf(hexs[2 * i]) * 16;
	        n += str.indexOf(hexs[2 * i + 1]);
	        bytes[i] = (byte) (n & 0xff);
	    }
	    return new String(bytes);
	}
	public static void main(String[] args) throws Exception {
//		byte[] aKey = TextUtils.hexStringToByte("5A 0D 42 C1 00 01 00 01 00 0B 01 00 00 48 36 F0 9A 12 A6 AF 60");
//		byte[] result = SecurityUtils.AES_cbc_encrypt(aKey, "eiyzyjizevwhuwki".getBytes(), "ekhpvagzbdfyomhf".getBytes());
//		String hex = TextUtils.toHexStringWithSpace(result);
//		
//		byte[] source = SecurityUtils.AES_cbc_decrypt(result, "eiyzyjizevwhuwki".getBytes(), "ekhpvagzbdfyomhf".getBytes());
//		System.out.println(hex);
//		System.out.println(TextUtils.toHexString(source));
		Long lat = 310000000L;
		byte[] bytes = BitsHelper.long2Bytes(lat);
		String hexString = TextUtils.toHexString(bytes);
		System.out.println(hexString);
	}
}
