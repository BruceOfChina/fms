package com.amygo.common.util;

public class HexXOR {
	
	private HexXOR() {
	}

	public static String selforByte(String hexStr){
		String strHex_X = xor(hexStr.substring(0,2),hexStr.substring(2, 4));
		for (int i = 0; i < hexStr.length()/2; i++) {
			if(i>1){
				strHex_X = xor(strHex_X,hexStr.substring(i*2,i*2+2));
			}
		}
		return strHex_X;
	}
	
	public static String xor(String strHex_X,String strHex_Y){   
        //transfer x y to binary
        String anotherBinary=Integer.toBinaryString(Integer.valueOf(strHex_X,16));   
        String thisBinary=Integer.toBinaryString(Integer.valueOf(strHex_Y,16));   
        String result = "";   
        //check whether x y are 8 bits,or add 0 to left
        if(anotherBinary.length() != 8){   
        for (int i = anotherBinary.length(); i <8; i++) {   
                anotherBinary = "0"+anotherBinary;   
            }   
        }   
        if(thisBinary.length() != 8){   
        for (int i = thisBinary.length(); i <8; i++) {   
                thisBinary = "0"+thisBinary;   
            }   
        }   
        //exclusive or calculate
        for(int i=0;i<anotherBinary.length();i++){   
        //if the number is same in the same position,the result is 0 ,or the result is 1
                if(thisBinary.charAt(i)==anotherBinary.charAt(i))   
                    result+="0";   
                else{   
                    result+="1";   
                }   
            }  
        return Integer.toHexString(Integer.parseInt(result, 2));   
    }  
	
	public static byte xor(byte[] bytes,int length){
		if(bytes==null||bytes.length==0){
			return -1;
		}
		byte result = bytes[0];
		for (int i = 1; i < length; i++) {
			result = (byte) (result^bytes[i]);
		}
		return result;
	}
	
	public static byte xor(byte[] bytes){
		if(bytes==null||bytes.length==0){
			return -1;
		}
		byte result = bytes[0];
		for (int i = 1; i < bytes.length; i++) {
			result = (byte) (result^bytes[i]);
		}
		return result;
	}
	
	public static byte xor(byte[] bytes,int startPos,int length){
		if(bytes==null||bytes.length==0){
			return -1;
		}
		byte result = bytes[startPos];
		for (int i = startPos; i < length; i++) {
			result = (byte) (result^bytes[i]);
		}
		return result;
	}

}
