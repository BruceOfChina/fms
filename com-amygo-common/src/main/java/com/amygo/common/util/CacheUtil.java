package com.amygo.common.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheUtil {
	public static Map<String,Object> map = new ConcurrentHashMap<>();
	
	public static void put(String key,String field,Object value) {
		map.put(key+field, value);
	}
	
	public static Object get(String key,String field) {
		return map.get(key+field);
	}
	
	public static Object remove(String key,String field) {
		return map.remove(key+field);
	}

}
