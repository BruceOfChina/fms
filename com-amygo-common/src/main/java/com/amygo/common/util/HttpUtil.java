package com.amygo.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 使用 HttpClient 来发送 post 和 get 请求
 */
public class HttpUtil {
	
	public static final String ERROR_RETURN = "\"ok\":false"; //http 请求, 返回错误
	
	public static String clientPostUseUTF8(String url, Map<String, String> map) {
		return clientPost(url, map, "UTF-8");
	}
	
	public static String clientPostUseGBK(String url, Map<String, String> map) {
		return clientPost(url, map, "GBK");
	}
	
	/**
     * 发送  http post 请求. 请求的参数用的是 类似这种格式: ?id=258&name=suyi&age=23
     */
	public static String clientPost(String url, Map<String, String> map, String charset) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);

		List<NameValuePair> pairList = new ArrayList<NameValuePair>(map.size());
		for(Map.Entry<String, String> ent: map.entrySet()) { 
        	pairList.add(new BasicNameValuePair(ent.getKey(), ent.getValue()));
		}
		
		//设置: 连接请求和传输超时时间
		// java.net.SocketTimeoutException: Read timed out
		RequestConfig rc = RequestConfig.custom().setConnectTimeout(121000).setSocketTimeout(5555000).build();
		httpPost.setConfig(rc);
		
		CloseableHttpResponse httpResponse = null;

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(pairList, charset)); //"UTF-8", "GBK"
			
			httpResponse = httpClient.execute(httpPost);
			
			//StatusLine sl = httpResponse.getStatusLine(); //获取状态行
			//printStatusLine(sl); //打印状态行
			
			HttpEntity entity = httpResponse.getEntity();
			//printHttpEntity(entity);
			
			String result = EntityUtils.toString(entity);
			
			return result;
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(httpResponse, httpClient);
		}
		
		return ERROR_RETURN; //"error"
	}
	/**
     * 发送  http post 请求. 请求的参数用的是 类似这种格式: ?id=258&name=suyi&age=23
     */
	public static String clientPostJson(String url, String jsonParam, String charset) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);

		
		
		//设置: 连接请求和传输超时时间
		// java.net.SocketTimeoutException: Read timed out
		RequestConfig rc = RequestConfig.custom().setConnectTimeout(121000).setSocketTimeout(5555000).build();
		httpPost.setConfig(rc);
		httpPost.addHeader("Content-Type", "application/json");
		CloseableHttpResponse httpResponse = null;

		try {
			httpPost.setEntity(new ByteArrayEntity(jsonParam.getBytes("UTF-8"))); //"UTF-8", "GBK"
			
			httpResponse = httpClient.execute(httpPost);
			
			//StatusLine sl = httpResponse.getStatusLine(); //获取状态行
			//printStatusLine(sl); //打印状态行
			
			HttpEntity entity = httpResponse.getEntity();
			//printHttpEntity(entity);
			
			String result = EntityUtils.toString(entity);
			
			return result;
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeResource(httpResponse, httpClient);
		}
		
		return ERROR_RETURN; //"error"
	}
	public static String clientGet(String url, Map<String, String> map) {
		if(map == null || map.isEmpty()) {
			return clientGet(url);
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		int index = 0;
		
		for(Map.Entry<String, String> ent: map.entrySet()) {
			String val = ent.getValue();
			
			if(val != null) {
				sb.append(((index++) == 0) ? "?" : "&");
				sb.append(ent.getKey()).append("=").append(val);
			}
		}
		
		return clientGet(sb.toString());
	}

    /**
     * 发送  http get 请求
     */
	public static String clientGet(String url) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(url);
		//httpget.addHeader(new BasicHeader("", ""));
//		httpGet.addHeader("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNTgwMDYyMzc1MyIsImNyZWF0ZWQiOjE1NDI3MTg1NTYxMTEsImV4cCI6MTU0MzMyMzM1Nn0.ArDap7p_Jq-OMRJj2KBsl25EA0RZQe4lHAtQ59ltq8DT_FF06xsHA9bBDXkfFvWuWbWLi_XvEEEb8ops8TGhFw");
		
		//设置: 连接请求和传输超时时间
		// java.net.SocketTimeoutException: Read timed out
		RequestConfig rc = RequestConfig.custom().setConnectTimeout(11000).setSocketTimeout(55000).build();
		httpGet.setConfig(rc);
		
		CloseableHttpResponse httpResponse = null;
		
		try {
			httpResponse = httpClient.execute(httpGet);
			
			//StatusLine sl = httpResponse.getStatusLine(); //获取状态行
			//printStatusLine(sl); //打印状态行
			
			HttpEntity entity = httpResponse.getEntity();
			String result = EntityUtils.toString(entity);
			
			return result;
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			closeResource(httpResponse, httpClient);
		}
		
		return "error";
	}
	
	public static String clientGet(String url,String token) {
		if(StringUtils.isBlank(token)) {
			return clientGet(url);
		}
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(url);
		//httpget.addHeader(new BasicHeader("", ""));
		httpGet.addHeader("Authorization", token);//有的token需要拼接Bearer
		
		//设置: 连接请求和传输超时时间
		// java.net.SocketTimeoutException: Read timed out
		RequestConfig rc = RequestConfig.custom().setConnectTimeout(11000).setSocketTimeout(55000).build();
		httpGet.setConfig(rc);
		
		CloseableHttpResponse httpResponse = null;
		
		try {
			httpResponse = httpClient.execute(httpGet);
			
			//StatusLine sl = httpResponse.getStatusLine(); //获取状态行
			//printStatusLine(sl); //打印状态行
			
			HttpEntity entity = httpResponse.getEntity();
			String result = EntityUtils.toString(entity);
			
			return result;
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			closeResource(httpResponse, httpClient);
		}
		
		return "error";
	}
    
	/**
	 * 释放资源
	 */
	private static void closeResource(CloseableHttpResponse httpResponse, CloseableHttpClient httpClient) {
		try {
			httpResponse.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			httpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    
    
    public static void main(String[] args) throws JsonProcessingException {
//    	String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNTgwMDYyMzc1MyIsImNyZWF0ZWQiOjE1NDI3NjU0MDI3NjUsImV4cCI6MTU0MzM3MDIwMn0.d-oYhyZGcco1GNhJH1EMBXLpyrQrZMkpxHOrOPQQMZVhnEOhfYM26DrJaQ0BjDM7tCrbHRI3Y64A42JAhu_mfw";
//    	String url = "http://212.64.58.52:8080/api/getCarLocation?vinCode=123456";
    	
//    	String url = "http://localhost:8080/auth/getCarLocation";
//    	String url = "http://app.singulato.com/vehicle/center/v1/vehicle/detail/2c92838864da74fd0164ea0369ce0006";
//    	String result = clientGet(url);
//    	Map map = new HashMap();
//    	System.out.println(result);
//    	CommonUtils commonUtils = new CommonUtils();
//    	commonUtils.fromJson(result, VehicleStatusDTO)
//    	String url = "http://212.64.55.44:8080/api/toStartPoint?ipcId="+"100000000000";
//		String result = HttpUtil.clientGet(url);
    	
    	String url = "http://212.64.55.44:8080/api/sendPathPlanCommand";
		Map<String,String> paramMap = new HashMap<String,String>();
		String startLon = String.valueOf(121);
		String startLat = String.valueOf(31);
		String endLon = String.valueOf(121);
		String endLat = String.valueOf(31);
		
		paramMap.put("startLon", startLon);
		paramMap.put("startLat", startLat);
		paramMap.put("endLon", endLon);
		paramMap.put("endLat", endLat);
		CommonUtils commonUtils = new CommonUtils();
		ObjectMapper objectMapper = new ObjectMapper();
		String result = HttpUtil.clientPostJson(url, commonUtils.toJson(paramMap),"utf-8");
		System.out.println("call vcm api result:"+result);
    }

}
