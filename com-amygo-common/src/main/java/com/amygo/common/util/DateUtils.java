package com.amygo.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public final class DateUtils {
	
	public static String commonDateFormat = "yyyy-MM-dd HH:mm:ss";

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMdd", Locale.CHINESE);

	private DateUtils() {
	}

	public static Date getCurrentDate() {
		return new Date();
	}

	public static String getFormattedDate() {
		return simpleDateFormat.format(getCurrentDate());
	}

	public static String getFormattedDate(Date date) {
		return simpleDateFormat.format(date);
	}

	public static String getDateByFormat(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.CHINESE);
		return simpleDateFormat.format(date);
	}

	public static String transferLongToDate(String dateFormat, Long millSec) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date = new Date(millSec);
		return sdf.format(date);
	}

	public static Date getDateByStr(String str) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINESE);
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}

	public static Long getTimeByStr(String str) {
		return getDateByStr(str).getTime();
	}
	
	public static Long getTimeByStr(String str,String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE);
		try {
			return simpleDateFormat.parse(str).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static java.util.Date getDayEnd() {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	public static Long getDateLastDay(String year, String month) {

		// year="2018" month="2"
		Calendar calendar = Calendar.getInstance();
		// 设置时间,当前时间不用设置
		calendar.set(Calendar.YEAR, Integer.parseInt(year));
		calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);

		System.out.println(calendar.getTime());

		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		return calendar.getTime().getTime();
	}

	public static Date getPreviousDay() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		System.out.println(sdf.format(date));
		return date;
	}
}
